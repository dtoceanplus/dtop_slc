# Overview

A repository to demonstrate some of the many features of development tools and
introduce a typical structure for project directories.

SLC module is in the process of development.
SLC is distributed under the AGPL license see LICENSE file for more details.​

## Running using `docker-compose`

The System Lifetime Costs module can be run using `docker-compose` which runs in a single terminal. Run the following command to use `docker-compose`:
```bash
docker-compose build
docker-compose up -d
```

You can access the GUI/frontend by navigating to http://localhost

# Python Package

See [setup.py](setup.py) file and [src](src) folder.

Installing the package for development:
```bash
pip install -e .
```

Build package:
```bash
python setup.py sdist
```
Package will be in `dist` folder.

Install package from `dist`:
```bash
pip install dtop-slc --find-links dist
```

Documentation:
- [Python Packaging User Guide](https://packaging.python.org)
- [Installing Packages](https://packaging.python.org/tutorials/installing-packages/)
- [Packaging Python Projects](https://packaging.python.org/tutorials/packaging-projects/)

# Testing
## Unit Testing

See [test](test) folder.

The [pytest](https://docs.pytest.org/en/latest/) framework makes it easy to write tests.

Install pytest:
```bash
pip install pytest
```

Run tests:
```bash
python -m pytest
```

## API testing

You need to install and run [Dredd](https://dredd.org/) to perform automatic API testing. Before installation, make sure that [npm](https://www.npmjs.com/) is available in your environment (if you are in a Conda environment, just run `conda install nodejs`).

Install Dredd:
```bash
npm install -g dredd
pip install dredd-hooks
pip install -e .
```

Run tests:
```bash
flask initb-db  # this removes all data from existing database
dredd
```

## Coverage

Coverage reports about code which should be tested.

Python has tool [Coverage.py](https://coverage.readthedocs.io/)
and PyTest has plugin for it [pytest-cov](https://pytest-cov.readthedocs.io/).

Install pytest-cov plugin
```bash
pip install pytest-cov
```

Run pytest with coverage:
```bash
python -m pytest --cov=dtop_slc
```

# Languages

Python has tool [Babel](http://babel.pocoo.org/) that assist in internationalizing and localizing
and Flask has plugin for it [Flask-Babel](https://pythonhosted.org/Flask-Babel/).

See [babel.cfg](babel.cfg) file.

Extracting Text to Translate:
```bash
pybabel extract .
```
It will create `message.pot` file with translations.
This file should not be stored in repository.

Generating a Language Catalog:
```bash
pybabel init -i messages.pot -d src/dtop_slc/service/translations -l fr
```
It will create [src/dtop_slc/service/translations/fr/LC_MESSAGES/messages.po](src/service/gui/translations/fr/LC_MESSAGES/messages.po) text file.

Use tool [poEdit](https://www.poedit.net/) for catalog (.po file) editing.

Compile Catalog:
```bash
pybabel compile -d src/dtop_slc/service/translations
```
It will create `src/dtop_slc/service/translations/fr/LC_MESSAGES/messages.mo` binary file.
This file is used by [Flask-Babel](https://pythonhosted.org/Flask-Babel/).

Tools:
- [poEdit](https://www.poedit.net/)

Documentation:
- [I18n and L10n](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xiii-i18n-and-l10n)
- [Babel](http://babel.pocoo.org/)
- [Flask-Babel](https://pythonhosted.org/Flask-Babel/)


# Service API documentation

For service API description there is [OpenAPI Specification](https://openapis.org).
See [openapi.json](src/dtop_slc/service/static/openapi.json).

For API documentation page there is [ReDoc](https://github.com/Rebilly/ReDoc) tool.
It is OpenAPI/Swagger-generated API Reference Documentation.
See [templates/api/index.html](src/dtop_slc/service/templates/api/index.html) and
[api/foo/__init__.py](src/dtop_slc/service/api/foo/__init__.py).

For generating OpenAPI files from code comments there is tool [oas](https://openap.is).

Install oas with [Node.js npm](https://nodejs.org/):
```bash
npm install oas -g
```
Init template:
```bash
oas init
```
See [swagger.yaml](swagger.yaml).

Generate OpenAPI description:
```bash
oas generate > src/dtop_slc/service/static/openapi.json
```
See [openapi.json](src/dtop_slc/service/static/openapi.json).

Documentation:
- [ReDoc](https://github.com/Rebilly/ReDoc)
- [OpenAPI Specification by Swagger](https://swagger.io/specification/)
- [oas](https://openap.is)
- [OpenAPI Tools](https://openapi.tools)


# GitLab CI

The [.gitlab-ci.yml](.gitlab-ci.yml) file tells the GitLab runner what to do.

GitLab offers a CI (Continuous Integration) service.
CI is for build and check each modification in repository.

Documentation:
- [Getting started with GitLab CI/CD](https://gitlab.com/help/ci/quick_start/README)
- [GitLab CI/CD Pipeline Configuration Reference](https://gitlab.com/help/ci/yaml/README.md)


# Start Service

Install `python-dotenv` for taking service default configuration from `.flaskenv`:

```DOS .bat
pip install python-dotenv
```

Without `.flaskenv` you should set environment variables for `flask`:

```DOS .bat
set FLASK_APP=dtop_slc.service
set FLASK_ENV=development
set FLASK_RUN_PORT=5000
```

Initialize database:
```DOS .bat
flask init-db
```
Database will be created in `instance/dtop_main.sqlite`.

Start the service:
```DOS .bat
flask run
```

Now you can open in browser this URL: http://localhost:5000/.

## E2E Frontend Testing

End-to-End (E2E) testing is one of final valuable indicators of a module progress and a quality.
However, this can be quite time consuming and depending on a stable configuration.

For achievement of good balance between unit tests and E2E tests, it is desirable to provide a sufficient amount of unit tests.

First of all build a proper environment for E2E tests. This is usually done once before the start of testing.
```shell
docker-compose build
```

Then start all necessary services of the application
```shell
docker-compose up --detach
```

Use the next command for running tests. This step can be repeated unlimited required number of times already without executing the two first commands.
```shell
docker exec slc_e2e npx cypress run
```

To get more technical details, please consider reading the dedicated [wiki page](https://gitlab.com/opencascade/dtocean/wiki/-/blob/master/dtoceanplus/E2E-tests-via-Cypress.md)

## Steps for Industrial Partners

To run and navigate through the SLC module tool, there are a few simple steps to take:
- Install [**git**](https://git-scm.com/downloads)\
Git is a versioning tool but, in this case, it is only necessary to clone SLC module from [GitLab](https://gitlab.com/wavec/dtoceanplus/dtop_slc)

- Install **Docker Desktop**:
    - for [Windows](https://docs.docker.com/docker-for-windows/install/)
    - for [Mac](https://docs.docker.com/docker-for-mac/install/)
    
Make sure you have Docker Desktop running.

- Clone SLC module:
In a command line (for example, `cmd.exe` in windows), travel to a directory where you want clone the module. For example:
```
C:\Users\"USER"> cd Documents\DTOceanPlus\Tools
C:\Users\"USER"\Documents\DTOceanPlus\Tools>_
```
Then, clone SLC module with the following commands:
```bash
git clone git@gitlab.com:wavec/dtoceanplus/dtop_slc.git
cd dtop_slc
```
_Note_: You need access rigths to SLC GitLab repository to clone it

- Run the module:
```bash
docker-compose build
docker-compose up -d
```

You can access the GUI/frontend by navigating to [http://localhost](http://localhost) in your browser
