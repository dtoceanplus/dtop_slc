import json
import sys

import dredd_hooks as hooks
import requests

sys.stdout = sys.stderr = open("dredd-hooks-output.txt", "w")

def if_not_skipped(func):
  def wrapper(transaction):
    if not transaction['skip']:
      func(transaction)
  return wrapper

# List of hooks to be skipped.
# List all possible hooks here. Uncomment to check that DREDD is correctly ready for testing.
# @hooks.before('/api/projects > Add an SLC project > 201 > application/json')
# @hooks.before('/api/projects > Add an SLC project > 400 > application/json')
# @hooks.before('/api/projects > Return the list of SLC projects > 200 > application/json')
# @hooks.before('/api/projects/{slcid} > Return SLC project > 200 > application/json')
# @hooks.before('/api/projects/{slcid} > Return SLC project > 404 > application/json')
# @hooks.before('/api/projects/{slcid} > Delete the SLC project > 200 > application/json')
# @hooks.before('/api/projects/{slcid} > Delete the SLC project > 400 > application/json')
# @hooks.before('/api/projects/{slcid} > Update the the SLC project > 200 > application/json')
# @hooks.before('/api/projects/{slcid} > Update the the SLC project > 400 > application/json')
# @hooks.before('/api/projects/{slcid} > Update the the SLC project > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/general > Add general inputs > 201 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/general > Add general inputs > 400 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/general > Add general inputs > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/general > General inputs status > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/general > General inputs status > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/general > Update general inputs > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/general > Update general inputs > 400 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/general > Update general inputs > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/financial > Add financial inputs > 201 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/financial > Add financial inputs > 400 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/financial > Add financial inputs > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/financial > Financial inputs status > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/financial > Financial inputs status > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/financial > Update financial inputs > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/financial > Update financial inputs > 400 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/financial > Update financial inputs > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/ace > Add ACE inputs > 201 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/ace > Add ACE inputs > 400 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/ace > Add ACE inputs > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/ace > ACE inputs status > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/ace > ACE inputs status > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/ace > Update ACE inputs > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/ace > Update ACE inputs > 400 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/ace > Update ACE inputs > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/external > Add external inputs > 201 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/external > Add external inputs > 400 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/external > Add external inputs > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/external > External inputs status > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/inputs/external > External inputs status > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/results > Add results > 201 > application/json')
# @hooks.before('/api/projects/{slcid}/results > Add results > 400 > application/json')
# @hooks.before('/api/projects/{slcid}/results > Add results > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/results > Results status > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/results > Results status > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/results > Delete the results > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/results > Delete the results > 400 > application/json')
# @hooks.before('/api/projects/{slcid}/results > Delete the results > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/economical/capex > Returns the project CapEx > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/economical/capex > Returns the project CapEx > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/economical/opex > Returns the project OpEx > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/economical/opex > Returns the project OpEx > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/economical/lcoe > Returns the LCOE > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/economical/lcoe > Returns the LCOE > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/economical/ace > Returns the ACE > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/economical/ace > Returns the ACE > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/financial/pbp > Return Simple and Discount Payback period > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/financial/pbp > Return Simple and Discount Payback period > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/financial/irr > Return Internal Rate of Return > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/financial/irr > Return Internal Rate of Return > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/financial/npv > Return Net Present Value > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/financial/npv > Return Net Present Value > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/benchmark/metrics > Returns the CAPEX and the OPEX of the project per killoWatt > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/benchmark/metrics > Returns the CAPEX and the OPEX of the project per killoWatt > 404 > application/json')
# @hooks.before('/api/projects/{slcid}/benchmark/breakdown > Percentage of the investment in each parcel > 200 > application/json')
# @hooks.before('/api/projects/{slcid}/benchmark/breakdown > Percentage of the investment in each parcel > 404 > application/json')
def _skip(transaction):
  transaction['skip'] = True


@hooks.before_all
def clear_database(transaction):
  response = requests.get('http://localhost:5000/api/projects')
  if response.status_code == 200:
    existing_project = response.json()
    for project in existing_project:
      response = requests.delete(f'http://localhost:5000/api/projects/{project["id"]}')


@hooks.before('/api/projects > Return the list of SLC projects > 200 > application/json')
@hooks.before('/api/projects/{slcid} > Return SLC project > 200 > application/json')
@hooks.before('/api/projects/{slcid} > Delete the SLC project > 200 > application/json')
@hooks.before('/api/projects/{slcid} > Delete the SLC project > 400 > application/json')
@hooks.before('/api/projects/{slcid} > Update the the SLC project > 200 > application/json')
@hooks.before('/api/projects/{slcid} > Update the the SLC project > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/general > Add general inputs > 201 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/general > Add general inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/general > General inputs status > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/general > Update general inputs > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/general > Update general inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/financial > Add financial inputs > 201 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/financial > Add financial inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/financial > Financial inputs status > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/financial > Update financial inputs > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/financial > Update financial inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/ace > Add ACE inputs > 201 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/ace > Add ACE inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/ace > ACE inputs status > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/ace > Update ACE inputs > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/ace > Update ACE inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/external > Add external inputs > 201 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/external > Add external inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/external > External inputs status > 200 > application/json')
@hooks.before('/api/projects/{slcid}/results > Add results > 201 > application/json')
@hooks.before('/api/projects/{slcid}/results > Add results > 400 > application/json')
@hooks.before('/api/projects/{slcid}/results > Results status > 200 > application/json')
@hooks.before('/api/projects/{slcid}/results > Delete the results > 200 > application/json')
@hooks.before('/api/projects/{slcid}/results > Delete the results > 400 > application/json')
@if_not_skipped
def create_temp_project(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  response = requests.post(f'{protocol}//{host}:{port}/api/projects',
                           json={"title": "Temporary project",
                                 "complexity": "low"})
  transaction['_project_id'] = response.json()['created_project']['id']


@hooks.before('/api/projects/{slcid}/economical/capex > Returns the project CapEx > 200 > application/json')
@hooks.before('/api/projects/{slcid}/economical/opex > Returns the project OpEx > 200 > application/json')
@hooks.before('/api/projects/{slcid}/economical/lcoe > Returns the LCOE > 200 > application/json')
@hooks.before('/api/projects/{slcid}/economical/ace > Returns the ACE > 200 > application/json')
@hooks.before('/api/projects/{slcid}/financial/pbp > Return Simple and Discount Payback period > 200 > application/json')
@hooks.before('/api/projects/{slcid}/financial/irr > Return Internal Rate of Return > 200 > application/json')
@hooks.before('/api/projects/{slcid}/financial/npv > Return Net Present Value > 200 > application/json')
@hooks.before('/api/projects/{slcid}/benchmark/metrics > Returns the CAPEX and the OPEX of the project per killoWatt > 200 > application/json')
@hooks.before('/api/projects/{slcid}/benchmark/breakdown > Percentage of the investment in each parcel > 200 > application/json')
@if_not_skipped
def create_temp_project_cpx3(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  response = requests.post(f'{protocol}//{host}:{port}/api/projects',
                           json={"title": "Temporary project",
                                 "complexity": "high"})
  transaction['_project_id'] = response.json()['created_project']['id']


@hooks.after('/api/projects > Return the list of SLC projects > 200 > application/json')
@hooks.after('/api/projects/{slcid} > Return SLC project > 200 > application/json')
@hooks.after('/api/projects/{slcid} > Delete the SLC project > 400 > application/json')
@hooks.after('/api/projects/{slcid} > Update the the SLC project > 200 > application/json')
@hooks.after('/api/projects/{slcid} > Update the the SLC project > 400 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/general > Add general inputs > 201 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/general > Add general inputs > 400 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/general > General inputs status > 200 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/general > Update general inputs > 200 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/general > Update general inputs > 400 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/financial > Add financial inputs > 201 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/financial > Add financial inputs > 400 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/financial > Financial inputs status > 200 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/financial > Update financial inputs > 200 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/financial > Update financial inputs > 400 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/ace > Add ACE inputs > 201 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/ace > Add ACE inputs > 400 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/ace > ACE inputs status > 200 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/ace > Update ACE inputs > 200 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/ace > Update ACE inputs > 400 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/external > Add external inputs > 201 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/external > Add external inputs > 400 > application/json')
@hooks.after('/api/projects/{slcid}/inputs/external > External inputs status > 200 > application/json')
@hooks.after('/api/projects/{slcid}/results > Add results > 201 > application/json')
@hooks.after('/api/projects/{slcid}/results > Add results > 400 > application/json')
@hooks.after('/api/projects/{slcid}/results > Results status > 200 > application/json')
@hooks.after('/api/projects/{slcid}/results > Delete the results > 200 > application/json')
@hooks.after('/api/projects/{slcid}/results > Delete the results > 400 > application/json')
@hooks.after('/api/projects/{slcid}/economical/capex > Returns the project CapEx > 200 > application/json')
@hooks.after('/api/projects/{slcid}/economical/opex > Returns the project OpEx > 200 > application/json')
@hooks.after('/api/projects/{slcid}/economical/lcoe > Returns the LCOE > 200 > application/json')
@hooks.after('/api/projects/{slcid}/economical/ace > Returns the ACE > 200 > application/json')
@hooks.after('/api/projects/{slcid}/financial/pbp > Return Simple and Discount Payback period > 200 > application/json')
@hooks.after('/api/projects/{slcid}/financial/irr > Return Internal Rate of Return > 200 > application/json')
@hooks.after('/api/projects/{slcid}/financial/npv > Return Net Present Value > 200 > application/json')
@hooks.after('/api/projects/{slcid}/benchmark/metrics > Returns the CAPEX and the OPEX of the project per killoWatt > 200 > application/json')
@hooks.after('/api/projects/{slcid}/benchmark/breakdown > Percentage of the investment in each parcel > 200 > application/json')
@if_not_skipped
def delete_temp_project(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  project_id = transaction['_project_id']
  requests.delete(f'{protocol}//{host}:{port}/api/projects/{project_id}')


@hooks.after('/api/projects > Add an SLC project > 201 > application/json')
@if_not_skipped
def delete_project_created_by_test(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  project_id = json.loads(transaction['results']['fields']['body']['values']['actual'])['created_project']['id']
  requests.delete(f'{protocol}//{host}:{port}/api/projects/{project_id}')


@hooks.before('/api/projects/{slcid}/inputs/general > General inputs status > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/general > Update general inputs > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/general > Update general inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/financial > Financial inputs status > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/financial > Update financial inputs > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/financial > Update financial inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/ace > ACE inputs status > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/ace > Update ACE inputs > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/ace > Update ACE inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/external > Add external inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/external > External inputs status > 200 > application/json')
@hooks.before('/api/projects/{slcid}/results > Add results > 201 > application/json')
@hooks.before('/api/projects/{slcid}/results > Add results > 400 > application/json')
@hooks.before('/api/projects/{slcid}/results > Results status > 200 > application/json')
@hooks.before('/api/projects/{slcid}/results > Delete the results > 200 > application/json')
@hooks.before('/api/projects/{slcid}/results > Delete the results > 400 > application/json')
@if_not_skipped
def post_project_inputs(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  project_id = transaction['_project_id']

  uri = transaction['request']['uri']
  if 'general' in uri or 'results' in uri:
    response = requests.post(f'{protocol}//{host}:{port}/api/projects/{project_id}/inputs/general',
                             json={"device_type": "WEC",
                                   "project_life": 20,
                                   "discount_rate": 12,
                                   "device_power": 1000, 
                                   "device_costs": 1000000,
                                   "device_number": 20})
  if 'financial' in uri:
    response = requests.post(f'{protocol}//{host}:{port}/api/projects/{project_id}/inputs/financial',
                             json={"energy_price": 0.055,
                                   "financial_grant": 10000000.0,
                                   "fit_price": 0.2,
                                   "fit_years": 15})
  if 'ace' in uri:
    response = requests.post(f'{protocol}//{host}:{port}/api/projects/{project_id}/inputs/ace',
                             json={"accw": 10,
                                   "surface_area": 213,
                                   "surface_thick": 0.2,
                                   "density": 1212,
                                   "cost_manufacture": 120})
  if 'external' in uri or 'results' in uri:
    response = requests.post(f'{protocol}//{host}:{port}/api/projects/{project_id}/inputs/external')


@hooks.before('/api/projects/{slcid}/economical/capex > Returns the project CapEx > 200 > application/json')
@hooks.before('/api/projects/{slcid}/economical/opex > Returns the project OpEx > 200 > application/json')
@hooks.before('/api/projects/{slcid}/economical/lcoe > Returns the LCOE > 200 > application/json')
@hooks.before('/api/projects/{slcid}/economical/ace > Returns the ACE > 200 > application/json')
@hooks.before('/api/projects/{slcid}/financial/pbp > Return Simple and Discount Payback period > 200 > application/json')
@hooks.before('/api/projects/{slcid}/financial/irr > Return Internal Rate of Return > 200 > application/json')
@hooks.before('/api/projects/{slcid}/financial/npv > Return Net Present Value > 200 > application/json')
@hooks.before('/api/projects/{slcid}/benchmark/metrics > Returns the CAPEX and the OPEX of the project per killoWatt > 200 > application/json')
@hooks.before('/api/projects/{slcid}/benchmark/breakdown > Percentage of the investment in each parcel > 200 > application/json')
@if_not_skipped
def post_project_all_inputs(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  project_id = transaction['_project_id']

  response = requests.post(f'{protocol}//{host}:{port}/api/projects/{project_id}/inputs/general',
                           json={"device_type": "WEC",
                                 "project_life": 20,
                                 "discount_rate": 12,
                                 "device_power": 1000, 
                                 "device_costs": 1000000,
                                 "device_number": 20})
                    
  response = requests.post(f'{protocol}//{host}:{port}/api/projects/{project_id}/inputs/financial',
                           json={"energy_price": 0.055,
                                 "financial_grant": 10000000.0,
                                 "fit_price": 0.2,
                                 "fit_years": 15})

  response = requests.post(f'{protocol}//{host}:{port}/api/projects/{project_id}/inputs/ace',
                           json={"accw": 10,
                                 "surface_area": 213,
                                 "surface_thick": 0.2,
                                 "density": 1212,
                                 "cost_manufacture": 120})

  response = requests.post(f'{protocol}//{host}:{port}/api/projects/{project_id}/inputs/external')


@hooks.before('/api/projects/{slcid}/results > Add results > 400 > application/json')
@hooks.before('/api/projects/{slcid}/results > Results status > 200 > application/json')
@hooks.before('/api/projects/{slcid}/results > Delete the results > 200 > application/json')
@hooks.before('/api/projects/{slcid}/economical/capex > Returns the project CapEx > 200 > application/json')
@hooks.before('/api/projects/{slcid}/economical/opex > Returns the project OpEx > 200 > application/json')
@hooks.before('/api/projects/{slcid}/economical/lcoe > Returns the LCOE > 200 > application/json')
@hooks.before('/api/projects/{slcid}/economical/ace > Returns the ACE > 200 > application/json')
@hooks.before('/api/projects/{slcid}/financial/pbp > Return Simple and Discount Payback period > 200 > application/json')
@hooks.before('/api/projects/{slcid}/financial/irr > Return Internal Rate of Return > 200 > application/json')
@hooks.before('/api/projects/{slcid}/financial/npv > Return Net Present Value > 200 > application/json')
@hooks.before('/api/projects/{slcid}/benchmark/metrics > Returns the CAPEX and the OPEX of the project per killoWatt > 200 > application/json')
@hooks.before('/api/projects/{slcid}/benchmark/breakdown > Percentage of the investment in each parcel > 200 > application/json')
@if_not_skipped
def post_project_results(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  project_id = transaction['_project_id']

  response = requests.post(f'{protocol}//{host}:{port}/api/projects/{project_id}/results')


@hooks.before('/api/projects > Add an SLC project > 400 > application/json')
@if_not_skipped
def make_query_invalid(transaction):
  data = json.loads(transaction['request']['body'])
  del data['title']
  transaction['request']['body'] = json.dumps(data)


@hooks.before('/api/projects/{slcid} > Update the the SLC project > 200 > application/json')
@if_not_skipped
def modify_query(transaction):
  data = json.loads(transaction['request']['body'])
  data['description'] = 'something else'
  transaction['request']['body'] = json.dumps(data)


@hooks.before('/api/projects/{slcid}/inputs/general > Add general inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/financial > Add financial inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/ace > Add ACE inputs > 400 > application/json')
@if_not_skipped
def make_inputs_query_invalid(transaction):
  data = json.loads(transaction['request']['body'])
  uri = transaction['request']['uri']

  if 'inputs' in uri and 'general' in uri:
    del data['project_life']
  elif 'inputs' in uri and 'financial' in uri:
    del data['energy_price']
  elif 'inputs' in uri and 'ace' in uri:
    del data['accw']

  transaction['request']['body'] = json.dumps(data)


@hooks.before('/api/projects/{slcid}/inputs/general > Update general inputs > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/financial > Update financial inputs > 200 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/ace > Update ACE inputs > 200 > application/json')
@if_not_skipped
def modify_inputs_query(transaction):
  data = json.loads(transaction['request']['body'])
  uri = transaction['request']['uri']

  if 'inputs' in uri and 'general' in uri:
    data['project_life'] = 1
  elif 'inputs' in uri and 'financial' in uri:
    data['fit_years'] = 20
  elif 'inputs' in uri and 'ace' in uri:
    data['surface_thick'] = 0.23

  transaction['request']['body'] = json.dumps(data)


@hooks.before('/api/projects/{slcid} > Update the the SLC project > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/general > Update general inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/financial > Update financial inputs > 400 > application/json')
@hooks.before('/api/projects/{slcid}/inputs/ace > Update ACE inputs > 400 > application/json')
@if_not_skipped
def delete_request_body(transaction):
  transaction['request']['body'] = json.dumps({})


@hooks.before('/api/projects/{slcid} > Delete the SLC project > 400 > application/json')
@hooks.before('/api/projects/{slcid}/results > Add results > 404 > application/json')
@hooks.before('/api/projects/{slcid}/results > Results status > 404 > application/json')
@hooks.before('/api/projects/{slcid}/results > Delete the results > 404 > application/json')
@if_not_skipped
def replace_project_id(transaction):
  uri = transaction['request']['uri']
  full_path = transaction['fullPath']
  idx = uri.replace('/api/projects/', '')

  if 'results' in uri:
    idx = idx.replace('/results', '')

  transaction['fullPath'] = full_path.replace(idx, '1234')
