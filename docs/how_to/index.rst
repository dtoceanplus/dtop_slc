.. _slc-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the System Lifetime Costs module. 
These guides are intended for users who have previously completed all the :ref:`System Lifetime Costs tutorials <slc-tutorials>` and have a good knowledge of the features and workings of the System Lifetime Costs module. 
While the tutorials give an introduction to the basic usage of the module, these *how-to guides* tackle slightly more advanced topics.

- :ref:`slc-how-to-complexity`

- :ref:`slc-how-to-formatdata`

- :ref:`slc-how-to-low-complexity`

- :ref:`slc-how-to-high-complexity`

.. toctree::
   :maxdepth: 2
   :hidden:

   slc-how-to-chose-complexity
   slc-how-to-formatdata
   slc-how-to-low-complexity
   slc-how-to-high-complexity