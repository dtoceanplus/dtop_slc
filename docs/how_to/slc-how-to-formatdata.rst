.. _slc-how-to-formatdata:

************************************************************************
How to prepare data for using the System lifetime Costs module
************************************************************************

This guide summarises the data requirements and specifications for running
the System Lifetime Costs module in full complexity standalone mode (introduced
in the “External inputs” tab), but notes which parameters are not required at low 
complexity and which come from other modules in integrated mode.

Format the Bill of materials input files
------------------------------------------

The bill of materials produced by each design module is stored in a json format,
with the specific data structures described below. Although all the information
stored in the individual BOMs are compiled into the final Bill of Materials
produced by the System Lifetime Costs module, only the total costs values of each
subsystem are considered for the economic and financial computations. 

Regardless of the module source, the bill of materials files follows the same data structure, a json file comprised of six lists of the same size: 

#. *id* is the component/system identifier, 

#. *name* corresponds to the name of the component/system, 

#. *qnt* refers to the quantities of each component/system, 

#. *uom* refers to the unity of measurement (e.g. m, kg, …), 

#. *unit_cost* corresponds to the unitary cost of each item, and,

#. *total_cost*, which corresponds to the total cost (in Euros) associated with each item. 

The SLC module is responsible for compiling the bill of materials of each module, although
it only uses the values in the total_cost variable, attributable to items with a ``Tot_`` id,
to carry out internal calculations. 

Energy Transformation BOM
^^^^^^^^^^^^^^^^^^^^^^^^^^

A dummy bill of materials for the components of the Energy Transformation module was generated, as presented below.
Even though the entire list of attributes will be presented in the final bill of materials compiled by SLC,
only the total cost values (stored in the ´´total_cost´´ list) are used for
internal calculations.

.. code:: 

    {
    "id":["CAT_turbine", "CAT_gen", "CAT_b2b", "Tot_ET"],
    "name":["Air turbine", "Generator", "Back to back converter", "Total ET system"],
    "qnt":["1", "1", "1", "-"],
    "uom":["-", "-", "-", "-"],
    "unit_cost":["100000", "100000", "100000", "-"],
    "total_cost":[100000, 100000, 100000, 300000]
    }

Example Bill of Materials for the Energy Transformation sub-systems

Energy Delivery BOM
^^^^^^^^^^^^^^^^^^^^

A dummy bill of materials for the components of the Energy Delivery module was generated,
as presented below. 
Even though the entire list of attributes will be presented in the final bill of
materials compiled by SLC, only the total cost values (stored in the ´´total_cost´´ list)
are used for internal calculations.


- Total onshore infrastructure costs (``id = tot_onshoreinf``, ``total_cost = 45,000€``)

- Total transmission network costs (``id = "Tot_transm"``, ``total_cost = 2,000,000€``)

- Total array network costs (``id = Tot_network``, ``total_cost = 1,000,000€``)

- Total collection point costs (``id = Tot_colpoint``, ``total_cost = 500,000€``)

.. code:: 

    {
    "id": ["CAT_Cable001", "CAT_Cable062", "CAT_colpoint", "CAT_con001", "Tot_onshoreinf",
                "Tot_transm", "Tot_network", "Tot_colpoint"],
    "name": ["Cable xyz", "Cable xyz239", "Subsea hub", "Connector wet-mate", "Total onshore
                        infrastructure", "Total Transmission network", "Total Array network", "Total 
                        Collection point"],
    "qnt":   ["3000", "9000", "2", "3", "-", "-", "-", "-"],
    "uom": ["m", "m", "-", "-", "-", "-", "-", "-"],
    "unit_cost":   ["2300", "1100", "1000000", "1000000", "-", "-", "-", "-"],
    "total_cost": [100, 200, 300, 400, 45000, 2000000, 1000000, 500000]
    }

Example Bill of Materials for the Energy Delivery sub-systems


Station Keeping BOM
^^^^^^^^^^^^^^^^^^^^

For the Station Keeping components, a dummy bill of materials was compiled in the table below. 
Even though the entire list of attributes will be presented in the final bill of materials
compiled by SLC, only the total cost values (stored in the ´´total_cost´´ list) are used for
internal calculations.

.. code:: 

    {
    "id": ["CAT_Anchor001", "CAT_ML001", "Tot_SK"],
    "name": ["Anchor ", "Mooring line", "Total costs of SK system"],
    "qnt": ["3", "1500", "-"],
    "uom": ["-", "m", "-"],
    "unit_cost": ["20000", "3000", "-"],
    "total_cost": [90000, 4500000, 4590000]
    }

Example Bill of Materials for the Station Keeping sub-systems



Logistics and Marine Operations BOM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The costs of the installation operations to be considered are compiled in the bill
of materials typically produced by LMO, as presented in the table below.
The SLC module will include every installation operation featured in the LMO BOM file, as long as its id starts with ´´Tot_´´.
In case a given operation is not to be considered, it can be ignored and not included in the file.
This is the case of the mooring and collection point installation costs, which did not take place in this example.
In this case, only total costs are compiled and considered, even though the operations are not in order:

- The total cost of installing devices (id = “Tot_Inst_Dev”, total_cost = 872,215€)

- The total cost of installing anchors and foundations  (id = “Tot_Inst_Anc”, total_cost = 6,128,696€)

- The total mooring installation costs  (id = “Tot_Inst_Moor”, total_cost = 1628674€)

- The total cable installation costs  (id = “Tot_Inst_Cable”, total_cost = xxxx€ - not included)

- The collection point installation costs  (id = “Tot_Inst_Col”, total_cost = xxxx€ - not included)

.. code:: 

    {
    "id": ["Tot_Inst_Dev", "Tot_Inst_Anc", "Tot_Inst_Cable"],
    "name":  ["Total cost of installation of devices", "Total cost of installation of Anchors", "Total cost 
                        of installation of cables"],
    "qnt": ["-", "-", "-"],
    "uom": ["-", "-", "-"],
    "unit_cost": ["-", "-", "-"],
    "total_cost": [872215, 6128696, 1628674]
    }

Example LMO Bill of materials
    

Format the Annual Energy Production (AEP) input file
-----------------------------------------------------

The AEP file contains the net annual energy production of the array, in kW, 
for each year of the project (in the example file below, 20 years are considered).
In the integrated mode, this input is produced by SPEY and already takes into consideration
the downtime due to component failure and O&M, as calculated by the LMO module.
It must be noted that changing the number of years (e.g. from 20 to 30 years) or
the number of devices (e.g. from 5 to 1), without changing the AEP file will create
an inconsistency and no effect on the calculations will take place. The AEP file must
be also modified to reflect these changes.


.. code::
    {
    "array_annual_net_energy_pd": {
        "Aggregation-Level": "array",
        "value": {
        "1": 2727000,
        "2": 2727000,
        "3": 2727000,
        "4": 2727000,
        "5": 2727000,
        "6": 2727000,
        "7": 2727000,
        "8": 2727000,
        "9": 2727000,
        "10": 2727000,
        "11": 2727000,
        "12": 2727000,
        "13": 2727000,
        "14": 2727000,
        "15": 2727000,
        "16": 2727000,
        "17": 2727000,
        "18": 2727000,
        "19": 2727000,
        "20": 2727000
        }
    }
    }

Example AEP input json file


Format the Maintenance solution
-------------------------------

In the integrated mode, the maintenance solution is produced by the Logistics and
Marine Operations planning tools. In this example, one maintenance operation per
year is assumed (with an average OPEX cost per year on the “operation_cost” parameter),
to simplify the data inputting process. An example of the maintenance solution input
is shown below.

.. code::

    {
    "operation_id": [
        "OP13_0",
        "OP13_0",
        "OP13_0",
        "OP13_0",
        "OP13_0",
        "OP13_0",
        "OP13_0",
        "OP12_0",
        "OP13_1",
        "OP12_1",
        "OP13_2",
        "OP12_2",
        "OP13_3",
        "OP12_3",
        "OP12_4",
        "OP13_4",
        "OP12_5",
        "OP12_6",
        "OP13_5",
        "OP12_7"
    ],
    "proj_year": [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20
        ],
    "operation_cost":
     [
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527
        ],
    }

Example Maintenance solution input json file, as produced by LMO

