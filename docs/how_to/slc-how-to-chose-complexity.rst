.. _slc-how-to-complexity:

How to decide between complexity levels in System and Lifetime Cost module
==========================================================================

.. warning::
    This content is mostly from D6.4 System Lifetime Costs alpha version,
    and still to be reviewed and updated

The System Lifetime Costs module was designed to support the assessment of ocean energy technologies
and projects at different stages of technology development. However, at different stages
of the technology development process, the amount of available information and data changes.
In DTOceanPlus, the method used by each module will change to align with this detail. Given that
for low TRLs, data availability is limited and uncertainty is high, the SLC module was
implemented with three different levels of complexity and slightly different modes of operation.

At early stages, little data is available, so the methods were designed to be simple and
require minimum inputs. At later stages, more detail is available so the methods used can become more complex.
This allowed to deliver meaningful metrics at each stage of development, while avoiding metrics
which would require an excessive amount of information from the user too early in the project.

In the table below, the different functionalities of the SLC module are described
for the different levels of complexity. While for complexity levels 2 and 3 (Cpx2 and Cpx3),
the user can run all the functionalities of the SLC module, for Cpx1 level,
some functionalities are limited (Economic and Benchmark) or simply not available (Benchmark).
However, in case of limited functionality, proxy metrics such as ACE (Economic assessment)
can be calculated instead.  


Example Differences between complexity levels for the SLC module
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

In the table below, the main differences in respect to the inputs and outputs of the
SLC module for the different complexity levels are presented.

.. figure:: fig/slc_complexity.png
   :alt: Functionalities of SLC at different complexity levels
   
   Functionalities of SLC at different complexity levels