.. _slc-how-to-low-complexity:

How to use System Lifetime Costs at low complexity
====================================================

The low complexity (level 1) of the System Lifetime Costs tool
can be used to get a quick assessment with minimum input effort of the economics and 
financial performance of a given project. 

.. note::
  At low complexity, financial parameters (e.g. internal rate of return, net present value) are not calculated. 

1. Enter inputs
---------------------

At low complexity, the input parameters for the SLC tool are split
into Project inputs, External inputs, and Alternative metrics. 

In integrated mode, all of the external input parameters come 
from other tools, as shown in the middle column in the tables below. 
In standalone mode the user must enter all parameters, as shown in the right column.
In both cases, the user must specify which are the project lifecycle stages
to be analysed (i.e. installation, maintenance, and/or decommissioning). It must be
noted that the decommissioning phase may only be computed if the installation phase
is selected.

**Project inputs:**

+------------------------------------+-----------------------------------+
|                                    |            Data source            |
|           Project inputs           +-----------------+-----------------+
|                                    | Integrated mode | Standalone mode |
+------------------------------------+-----------------+-----------------+
| Project lifetime                   | User            | User            |
+------------------------------------+-----------------+-----------------+
| Device type (WEC/Tidal)            | MC              | User            |
+------------------------------------+-----------------+-----------------+
| Device topology (floating/fixed)   | MC              | User            |
+------------------------------------+-----------------+-----------------+
| Device rated power                 | MC              | User            |
+------------------------------------+-----------------+-----------------+
| Device structural costs            | User            | User            |
+------------------------------------+-----------------+-----------------+
| Number of devices                  | EC              | User            |
+------------------------------------+-----------------+-----------------+
| Other CAPEX costs                  | User            | User            |
+------------------------------------+-----------------+-----------------+
| Other OPEX costs                   | User            | User            |
+------------------------------------+-----------------+-----------------+

**External inputs:**

+---------------------------------------------------+-----------------------------------+
|                                                   |            Data source            |
|               External module inputs              +-----------------+-----------------+
|                                                   | Integrated mode | Standalone mode |
+---------------------------------------------------+-----------------+-----------------+
| Energy Transformation Bill of Materials           | ET              | User            |
+---------------------------------------------------+-----------------+-----------------+
| Energy Delivery Bill of Materials                 | ED              | User            |
+---------------------------------------------------+-----------------+-----------------+
| Station Keeping Bill of Materials                 | SK              | User            |
+---------------------------------------------------+-----------------+-----------------+
| Logistics and Marine Operations Bill of Materials | LMO             | User            |
+---------------------------------------------------+-----------------+-----------------+
| Annual Energy Production                          | SPEY            | User            |
+---------------------------------------------------+-----------------+-----------------+
| Maintenance Solution                              | LMO             | User            |
+---------------------------------------------------+-----------------+-----------------+

**Alternative metric inputs:** 

+----------------------------------------+-----------------------------------+
|                                        |            Data source            |
|        Alternative metric inputs       +-----------------+-----------------+
|                                        | Integrated mode | Standalone mode |
+----------------------------------------+-----------------+-----------------+
| Average   Climate Capture Width (ACCW) | User            | User            |
+----------------------------------------+-----------------+-----------------+
| Total   surface area                   | User            | User            |
+----------------------------------------+-----------------+-----------------+
| Structural   thickness                 | User            | User            |
+----------------------------------------+-----------------+-----------------+
| Material   density                     | User            | User            |
+----------------------------------------+-----------------+-----------------+
| Cost   of manufacture per unit mass    | User            | User            |
+----------------------------------------+-----------------+-----------------+


To **view, update or delete** the Project inputs, click ``View/Update/Delete`` 
for the desired input type to be modified.

- To update values, only the parameters that need to be modified should be entered. 
  Click ``Update`` after making the updates to save these.

- To delete the inputs click ``Delete``, which will produce a pop-up window asking for confirmation. 
  Click ``Delete`` again to permenantly delete these inputs from the database.

2. Run computations 
---------------------------

Once the inputs have been filled, the user may chose to compute the results. 

Once the calculations have been completed, an alert window will inform the user.
The user may then choose to press the ´´View Results´´ button, which will redirect to the Results page.
Otherwise, in case an error occured, a message will be shown to the user.

.. note::
  Computing the results should only take a few seconds. 
  
3. View results 
-----------------

For complexity level 1, the user may select to view three different outputs, by pressing the respective button:

- **Compiled Bill of Materials**

- **Economic metrics**

- **Benchmark metrics**
