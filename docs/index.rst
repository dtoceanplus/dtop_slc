.. _slc-home:

System Lifetime Costs
=====================

Introduction 
------------
The System Lifetime Costs (SLC) module is responsible for assessing the economic
performance and financial attractiveness of a given ocean energy project, benchmarking
against reference projects. 


Structure
---------

This module's documentation is divided into four main sections:

- :ref:`slc-tutorials` to give step-by-step instructions on using SLC for new users. 

- :ref:`slc-how-to` that show how to achieve specific outcomes using SLC. 

- A section on :ref:`background, theory and calculation methods <slc-explanation>` that describes how SLC works and aims to give confidence in the tools. 

- The :ref:`API reference <slc-reference>` section documents the code of modules, classes, API, and GUI. 

.. toctree::
   :hidden:
   :maxdepth: 1

   tutorials/index
   how_to/index
   explanation/index
   reference/index

Functionalities
---------------

The main purpose of the System Lifetime Costs (SLC) module is to generate metrics in order to evaluate the economic performance (e.g CAPEX, OPEX, LCOE) and
financial attractiveness (IRR, NPV) of a given ocean energy project, benchmarking against reference projects. SLC’s functionalities include:

#.	**Compile Bill of Materials (BOM)**: it compiles an inventory of materials, assemblies, and components, including the quantities of each, as well as the installation operations required to construct a given ocean energy farm.

#.	**Financial assessment**: it evaluates the financial attractiveness of the project from the perspective of the investor, assessing project profitability.

#.	**Economic assessment**: it performs a techno-economic assessment, estimating the LCOE of the farm, or using other alternative metrics for early-stage technologies.

#.	**Benchmark analysis**: it compares the economic and financial results of the project against values from reference wave and tidal projects.

All assessments produced by the System Lifetime Costs module are carried out based
on the design outputs of the Deployment design tools but also project characteristics
introduced by the user and a catalogue of reference cost-breakdowns of ocean energy projects
at different development stages.

.. figure:: fig/slc_functionalities_arrow.svg
   :alt: Functionalities of the SLC module

   Functionalities of the SLC module

.. The module can be run in three different complexity modes (complexity 1, 2 and 3),
.. depending on data availability and accuracy requirements.

.. In standalone mode, the user first sets up a study, before entering inputs of the project.
.. Once these inputs are complete, the user can run the assessment process, 
.. and then view the results. The main outputs of the analysis are economic and
.. financial parameters, the compiled bill of materials, and benchmark metrics that
.. allow comparing different renewable energy projects.

Workflow for using the SLC module
---------------------------------
The workflow for using the System Lifetime Costs module can be summarised as
1) provide inputs, 2) perform a design, and 3) view the results, 
as shown in the figure below.

.. figure:: fig/slc_workflow.svg
   :alt: Workflow of the SLC module
   
   Workflow of the SLC module

Overview of SLC data requirements
---------------------------------
This section summarises the types of input data required to run the System Lifetime Costs module. 
Full details and data specifications are given in the how to guide on preparing data.
The required and optional inputs to run the module are summarised in the tables below. 
Note that in integrated mode, the required inputs will come from three different sources

#. :ref:`Inputs from the GUI <slc-main-gui-inputs>`

#. :ref:`Inputs from External modules (MC, EC, ET, ED, SK) <slc-main-external-inputs>`

#. :ref:`Inputs from the Catalogues<slc-main-db-inputs>`

Summary of mandatory inputs:

+------------------+-----------------------------+--------------+--------------+
|    Input Page    |         Complexity 1        | Complexity 2 | Complexity 3 |
+==================+=============================+==============+==============+
|  General inputs  |         Device type         |         Device type         |
|                  +-----------------------------+-----------------------------+
|                  |       Device topology       |       Device topology       |
|                  +-----------------------------+-----------------------------+
|                  |      Device rated power     |      Device rated power     |
|                  +-----------------------------+-----------------------------+
|                  | Device unit structural cost | Device unit structural cost |
|                  +-----------------------------+-----------------------------+
|                  |       Project lifetime      |       Project lifetime      |
|                  +-----------------------------+-----------------------------+
|                  |      Number of devices      |      Number of devices      |
|                  +-----------------------------+-----------------------------+
|                  |                             |        Discount rate        |
+------------------+-----------------------------+-----------------------------+
| Financial inputs |              N.D.           |   Electricity market price  |
+------------------+-----------------------------+-----------------------------+
|    ACE inputs    |              N.D.                                         |
+------------------+-----------------------------+-----------------------------+
|  External inputs |                 Bill of Materials from ET                 |
|                  +-----------------------------------------------------------+
|                  |                 Bill of Materials from ED                 |
|                  +-----------------------------------------------------------+
|                  |                 Bill of Materials from SK                 |
|                  +-----------------------------------------------------------+
|                  |                 Bill of Materials from LMO                |
|                  +-----------------------------------------------------------+
|                  |                 Annual Energy Production from SPEY        |
|                  +-----------------------------------------------------------+
|                  |                 Maintenance solution from LMO             |
+------------------+-----------------------------------------------------------+

Summary of optional inputs:

+---------------------+--------------------------------------+--------------------------------------+--------------------------------------+
|                     |                                      |                                      |                                      |
| Input  page         | Complexity 1                         | Complexity 2                         | Complexity 3                         |
+=====================+======================================+======================================+======================================+
|                     |                                      |                                      |                                      |
| General inputs      | Development and other CAPEX costs    | Development and other CAPEX costs    | Development and other CAPEX costs    |
+---------------------+--------------------------------------+--------------------------------------+--------------------------------------+
| Financial inputs    |                                      | Grant value                          | Grant value                          |
|                     |                                      +--------------------------------------+--------------------------------------+
|                     | N.D.                                 | Feed-in tariff price                 | Feed-in tariff price                 |
|                     |                                      +--------------------------------------+--------------------------------------+
|                     |                                      | Years of feed-in tariff              | Years of feed-in tariff              |
+---------------------+--------------------------------------+--------------------------------------+--------------------------------------+
| ACE inputs          | Average Climate Capture Width                                                                                      |
|                     +--------------------------------------------------------------------------------------------------------------------+
|                     | Surface area                                                                                                       |
|                     +--------------------------------------------------------------------------------------------------------------------+
|                     | Surface thickness                                                                                                  |
|                     +--------------------------------------------------------------------------------------------------------------------+
|                     | Material density                                                                                                   |
|                     +--------------------------------------------------------------------------------------------------------------------+
|                     | Cost of manufacture                                                                                                |
+---------------------+--------------------------------------------------------------------------------------------------------------------+
|                     |                                                                                                                    |
| External inputs     | N.D.                                                                                                               |
+---------------------+--------------------------------------------------------------------------------------------------------------------+

User inputs from the GUI
^^^^^^^^^^^^^^^^^^^^^^^^

.. _slc-main-gui-inputs:

The user will set basic information about the SLC study and provide the main project inputs, device and subsystem characteristics, as well as financing inputs and preferences, depending on the complexity level and technology.

- **Study**: Name, description and standalone mode (yes/no).

- **General inputs**: Device type (Wave/Tidal), device rated power, device structural costs, project lifetime, discount rate, number of devices, other development costs.

- **ACE inputs**: Optional metrics to calculate the ACE metric, a proxy metric for LCOE in low maturity technologies.

- **Financial metrics**: Electricity market price, awarded grant values, feed-in tariff schemas, duration of feed-in tariffs. 

Inputs from External modules
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. _slc-main-external-inputs: 

In order to run the System Lifetime Costs module, different inputs will be needed.

- The Bill of Materials from the Energy Transformation module

- The Bill of Materials from the Energy Delivery module

- The Bill of Materials from the Station Keeping module

- The Bill of Materials from the Logistics and Marine Operations module

- The Annual Energy Production from the SPEY module

- The Maintenance solution by the Logistics and Marine Operations module.

While the Bills of Materials from each module are not strictly required for the module to be able to run, the respective cost figures will not be considered in the economic and financial assessments. 
In standalone mode, these inputs will be uploaded to the SLC study through six independent json files. All external modules input studies must have the same complexity level.

Catalogue inputs
^^^^^^^^^^^^^^^^

.. _slc-main-db-inputs:

Apart from external inputs, and user inputs, the System Lifetime Costs module
uses default data stored in a catalogue, in order to benchmark the project outputs
against reference projects. These parameters may be modified by the user by directly edditing
the catalogue. More information about project stages in :ref:`Stage Gate: Background and theory <sg-explanation>`.

+-----------------------------+-----------+-----------+------------+-----------+-----------+------------+
|             Project   stage |  0,1,2,3  |     4     |      5     |  0,1,2,3  |     4     |      5     |
+=============================+===========+===========+============+===========+===========+============+
|                  Technology |   Tidal   |   Tidal   |    Tidal   |    Wave   |    Wave   |    Wave    |
+-----------------------------+-----------+-----------+------------+-----------+-----------+------------+
|                 Description | 1st array | 2nd array | Commercial | 1st array | 2nd array | Commercial |
+-----------------------------+-----------+-----------+------------+-----------+-----------+------------+
|              CAPEX   (€/kW) |    9500   |    7000   |    4500    |   10500   |    9800   |    4500    |
+-----------------------------+-----------+-----------+------------+-----------+-----------+------------+
|            OPEX   (€/kW/yr) |    600    |    370    |     270    |    700    |    350    |     300    |
+-----------------------------+-----------+-----------+------------+-----------+-----------+------------+
|              LCOE   (€/kWh) |    0.57   |    0.35   |    0.22    |    0.7    |    0.5    |    0.41    |
+-----------------------------+-----------+-----------+------------+-----------+-----------+------------+
|   Station   Keeping (%LCOE) |     8     |     8     |      8     |     10    |     10    |     10     |
+-----------------------------+-----------+-----------+------------+-----------+-----------+------------+
|  Electrical   costs (%LCOE) |     8     |     8     |      8     |     5     |     5     |      5     |
+-----------------------------+-----------+-----------+------------+-----------+-----------+------------+
|      Device   costs (%LCOE) |     28    |     28    |     28     |     43    |     43    |     43     |
+-----------------------------+-----------+-----------+------------+-----------+-----------+------------+
| Installation   costs(%LCOE) |     12    |     12    |     12     |     9     |     9     |      9     |
+-----------------------------+-----------+-----------+------------+-----------+-----------+------------+
|       Other   Costs (%LCOE) |     5     |     5     |      5     |     3     |     3     |      3     |
+-----------------------------+-----------+-----------+------------+-----------+-----------+------------+
|              OPEX   (%LCOE) |     39    |     39    |     39     |     30    |     30    |     30     |
+-----------------------------+-----------+-----------+------------+-----------+-----------+------------+