.. _slc_tut_02: 

Using the System Lifetime Costs module at low complexity in standalone mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. warning::
    This content is mostly from D6.6 Deliverable,
    and still to be reviewed and updated

Complexity CPX1 was designed to provide simplified assessments,
requiring minimum inputs from the user and other design modules. Inputs
are grouped into three input categories: i) “General inputs”, which
includes fundamental project parameters, ii) “ACE inputs”, which
consists of optional inputs that are only required in order to calculate
the ACE metric (as a proxy to the LCOE), and iii) “External inputs”,
which groups all the inputs from other upstream modules that are
required to run SLC. In this complexity, financial assessments are not
available due to limited data availability.

1) If required, create a new complexity level 1 study, as described in
   the tutorial of Section 7.3.4.1.

2) From the list of studies, click ‘Open’ to start working on the
   complexity level 1 study.

3) Click on the General inputs tab and:

   a. Select from the dropdown the device type [*required*].

   b. Select from the dropdown the device topology [*required*].

   c. Introduce the device rated power (kW) [*required*].

   d. Introduce the device unitary structural costs (€), which does not
      include the costs of the PTO [*required*].

   e. Introduce the project lifetime [1]_ (in years) [*required*]

   f. Introduce the number of devices [2]_ [*required*].

   g. Introduce Development and other CAPEX costs (€) [*optional*].

   h. Click “Validate”.

   i. If successful, you will get a message “General inputs added” and
      redirect the user to the inputs page. Otherwise, an error message
      will pop-up.

4) In case the ACE metric is to be calculated, the inputs in the ACE
   input tab must be *all* filled. However, since calculating the ACE
   metric is optional, the following inputs are described as optional as
   well:

   a. Specify the Average Climate Capture Width [3]_ (m) *[optional]*

   b. Surface Area (m2) *[optional]*

   c. Surface Thickness (m) *[optional]*

   d. Density  [4]_(kg/m3) *[optional]*

   e. Cost of manufacture (€/kg) *[optional]*

   f. Click “Validate”.

   g. If successful, you will get a message “ACE inputs added” and
      redirect the user to the inputs page. Otherwise, an error message
      will pop-up.

5) In order to provide meaningful assessments, the components of the
   farm, featured in the bill of materials produced by each design
   module, must be introduced in order to be considered in the economic
   and financial assessments. However, the SLC module does not strictly
   require all of the BOMs to be able to run, being therefore optional.

   a. Upload the Bill of Materials of the Energy Transformation module
      (json file) [*optional*]

   b. Upload the Bill of Materials of the Energy Delivery module (json
      file) [*optional*]

   c. Upload the Bill of Materials of the Station Keeping module (json
      file) [*optional*]

   d. Upload the Bill of Materials of the Logistics and Marine
      Operations module (json file) [*optional*]

   e. Upload the Bill of Materials of the Station Keeping module (json
      file) [*optional*]

   f. Upload the Annual Energy Production of the farm, for each year
      (json file) [*required*]

   g. Upload the maintenance solution, outputted by the Logistics and
      Marine Operations module, including the maintenance activities and
      costs for different years (json file) [*required*].

   h. In order to remove any file, press the “x” button close to the
      filename.

   i. Click “Validate”.

   j. If successful, you will get a message “External inputs added” and
      redirect the user to the inputs page. Otherwise, an error message
      will pop-up.

6) In order to view, modify or delete inputs, the input pages may be
   revisited.

7) Once all the desired inputs have been filled, click “Compute SLC
   Assessment” to run the tool

   a. If successful, a message will be shown to the user: “System
      Lifetime Costs assessment was successfully computed”. Otherwise,
      an error message will be presented to the user, describing what
      went wrong.

8) After pressing the button to run the SLC assessment, the user is
   directed to the results page, where the three main result categories
   are presented.

.. [1]
   The project lifetime must be consistent with the annual energy
   production (AEP) input file introduced in the external outputs
   section.

.. [2]
   The number of devices must be consistent with the annual energy
   production (AEP) input file introduced in the external outputs
   section. Otherwise, changing the number of devices will increase the
   total costs of the devices but not the total energy production.

.. [3]
   The Average Climate Capture Width (ACCW) is a measure of the
   effectiveness of a WEC at absorbing power from the incident wave
   energy field, expressed in m.

   ACCW [m] = P\ \ :sub:`avg` [kW] / P\ \ :sub:`resource` [Kw/m]

.. [4]
   The surface area, thickness and density are used to estimate the
   total material mass that is required, in order to calculate the
   Characteristic Capital Expenditure (CCE).
