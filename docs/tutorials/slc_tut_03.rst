.. _slc_tut_03: 

Using the System Lifetime Costs module at medium/high complexity in standalone mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. warning::
    This content is mostly from D6.6 Deliverable,
    and still to be reviewed and updated

In case of higher data availability, the System Lifetime Costs module
can be run at a higher complexity level (CPX2 or CPX3), to provide more
detailed assessments. In these complexities, the financial assessment
functionality is available. In this case, inputs are grouped into *four*
input categories: i) “General inputs”, which includes fundamental
project parameters, ii) Financial inputs, which include financial
parameters that are required to evaluate cashflows and financial
performance, iii) “ACE inputs”, which consists of optional inputs that
are only required in order to calculate the ACE metric (as a proxy to
the LCOE), iv) “External inputs”, which groups all the inputs from other
upstream modules that are required to run SLC.

1) If required, create a new complexity level 3 study, as described in
   the tutorial of Section 7.3.4.1.

2) From the list of studies, click ‘Open’ to start working on the
   complexity level 3 study

3) Click on the General inputs tab and:

   a. Select from the dropdown the device type [*required*].

   b. Select from the dropdown the device topology [*required*].

   c. Introduce the device rated power (kW) [*required*].

   d. Introduce the device unitary structural costs (€), which does not
      include the costs of the PTO [*required*].

   e. Introduce the project lifetime [1]_ (in years) [*required*]

   f. Introduce the number of devices [2]_ [*required*].

   g. Introduce Development and other CAPEX costs (€) [*optional*].

   h. Click “Validate”.

   i. If successful, you will get a message “General inputs added” and
      redirect the user to the inputs page. Otherwise an error message
      will pop-up.

4) Click on the Financial inputs tab and:

   a. Introduce market price (€/kWh) [*required*].

.. note::
   The market price specified in a) consists of the electricity selling
   price. However, in case a feed-in tariff is in place, especially if
   it does not cover the entire project lifetime, the user is allowed to
   specify a feed-in-tariff and the number of years, after which, the
   energy price reverts to the market priced specified in a).

b. Grant value (€) [*optional*], featuring possible grants that were
   awarded to the project.

c. Feed-in Tariff (FIT) price (€/kWh) [*optional*].

d. Years of FIT (years) [*optional*].

5) Click on the ACE inputs tab and:

   a. Specify the Average Climate Capture Width [3]_ (m) [optional]

   b. Surface Area (m2)

   c. Surface Thickness (m)

   d. Density [4]_(kg/m3)

   e. Cost of manufacture (€/kg)

   f. Click “Validate”.

   g. If successful, you will get a message “ACE inputs added” and
      redirect the user to the inputs page. Otherwise, an error message
      will pop-up.

6) In order to provide meaningful assessments, the components of the
   farm, featured in the bill of materials produced by each design
   module, must be introduced in order to be considered in the economic
   and financial assessments. However, the SLC module does not strictly
   require all of the BOMs to be able to run, being therefore optional.

   a. Upload the Bill of Materials of the Energy Transformation module
      (json file) [*optional*]

   b. Upload the Bill of Materials of the Energy Delivery module (json
      file) [*optional*]

   c. Upload the Bill of Materials of the Station Keeping module (json
      file) [*optional*]

   d. Upload the Bill of Materials of the Logistics and Marine
      Operations module (json file) [*optional*]

   e. Upload the Bill of Materials of the Station Keeping module (json
      file) [*optional*]

   f. Upload the Annual Energy Production of the farm, for each year
      (json file) [*required*]

   g. Upload the maintenance solution, outputted by the Logistics and
      Marine Operations module, including the maintenance activities and
      costs for different years (json file) [*required*].

   h. In order to remove any file, press the “x” button close to the
      filename.

   i. Click “Validate”.

   j. If successful, you will get a message “External inputs added” and
      redirect the user to the inputs page. Otherwise an error message
      will pop-up.

7) In order to view, modify or delete inputs, the input pages may be
   revisited.

8) Click “Compute SLC Assessment” to run the tool

   a. If successful, a message will be shown to the user: “System
      Lifetime Costs assessment was successfully computed”. Otherwise,
      an error message will be presented to the user, describing what
      went wrong.

9) After pressing the button to run the SLC assessment, the user is
   directed to the results page, where the four main result categories
   are presented.

.. [1]
   The project lifetime must be consistent with the annual energy
   production (AEP) files introduced in the external outputs section.

.. [2]
   Changing the number of devices will increase the total costs of the
   devices but not the AEP.

.. [3]
   The Average Climate Capture Width (ACCW) is a measure of the
   effectiveness of a WEC at absorbing power from the incident wave
   energy field, expressed in m.

   ACCW [m] = Pavg [kW] / Presource [Kw/m]

.. [4]
   The surface area, thickness and density are used to estimate the
   total material mass that is required, in order to calculate the
   Characteristic Capital Expenditure (CCE).
