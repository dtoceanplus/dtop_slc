.. _slc-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through each of the main functionalities of the System Lifetime Costs tool.
They are intended for those who are new to the System Lifetime Costs tool.
It is recommended to follow the tutorials in the suggested order listed below, as certain tutorials are dependent on others. 

**Using the System Lifetime Costs tool in integrated mode**

- :ref:`Using SLC at low complexity standalone for RM3 wave example <slc_tut_02>`

- :ref:`Using SLC at high complexity standalone for RM1 tidal example <slc_tut_03>`

**Using the System Lifetime Costs tool in standalone mode**

These show how to use System Lifetime Costs in standalone mode where the user needs to enter
all input parameters for the design process. 

- :ref:`Creating a new standalone SLC study <slc_tut_01>`

.. toctree::
   :maxdepth: 1
   :hidden:

   slc_tut_01
   slc_tut_02
   slc_tut_03
