.. _slc-explanation:

*********************
Background and theory
*********************

The System Lifetime Costs module is one of the four assessment modules
of the DTOceanPlus design suite of tools. It aims to perform economic
and financial assessments of wave and tidal renewable energy projects.
Some theoretical definitions are provided below, as well as the base
assumptions used in the SLC module.

.. contents::

Theory
========

Capital Expenditures (CAPEX)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^         
                            
Capital expenditures, commonly known as CAPEX, indicates the total
investment cost (in Euros) of a given project. CAPEX is a major driver
of the total costs of an ocean renewable energy project. In the context
of renewable energy production projects, the CAPEX is frequently
expressed in unit costs per installed unit power (i.e. €/kW) [Chozas2014], which
makes possible comparing different technologies in a benchmark analysis.

In the context of DTOceanPlus, it is assumed that the capital
expenditures occur at the beginning of the project. Reference values of
the CAPEX per kW were obtained from the most recent OES-IEA report [OES2015],
for wave energy and tidal stream projects, and presented in the figures below.

.. figure:: fig/slc_fig1.svg
   :alt: Unitary CAPEX variation with project capacity (Wave)
   
   Unitary CAPEX variation with project capacity (Wave) [OES2015]

.. figure:: fig/slc_fig2.png
   :alt: Unitary CAPEX variation with project capacity (Tidal)
   
   Unitary CAPEX variation with project capacity (Tidal) [OES2015]


Operational Expenditures (OPEX)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^         

The operational expenditures (OPEX) of a given project represent the
ongoing costs of running the project, e.g. maintenance costs, which are
distributed throughout the project lifetime. The OPEX of a renewable
energy project can be expressed as the total project OPEX (in Euros),
the average annual OPEX (€/year) or in costs per installed unit power
per year (i.e. €/(kW year) ). Calculating the OPEX in costs per
installed power per year, allows comparing projects of different
technologies and different sizes in the benchmark analysis.

In the figures below, reference values of the OPEX per kW per
year is shown for wave energy and tidal stream projects, respectively.

.. figure:: fig/slc_fig3.svg
   :alt: OPEX cost ranges for Wave Energy projects at different stages of Deployment.
   
   OPEX cost ranges for Wave Energy projects at different stages of Deployment. [OES2015]

.. figure:: fig/slc_fig4.png
   :alt: OPEX cost ranges for Tidal Energy projects at different stages of Deployment.
   
   OPEX cost ranges for Tidal Energy projects at different stages of Deployment [OES2015].

.. note::
  The dotted lines represent the maximum/minimum OPEX values provided during the Stakeholder engagement.
  The shaded area represent the industry averaged costs with an uncertainty bound of ±30%.

Levelised Cost of Energy (LCOE)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^         

The levelised cost of energy (LCOE) is a useful parameter to assess the
economic feasibility of a technology. It is defined as the sum of all
capital costs and lifetime operation and maintenance (O&M) costs
(discounted to present value) divided by the value of electricity
generation to grid accumulated throughout the technology’s lifetime
(also discounted to present value). Details of the calculation are given
in section 3.2.2 equation (13). The present value of decommissioning
costs of tidal and wave energy projects are assumed to have reduced
impact on the LCOE of the project (0.5-1% of the CAPEX [Astariz2015]) and were
therefore neglected in the present module [Neary2014], [Topper2019].

A large share of the LCOE may be attributed to the device CAPEX (both
structural and PTO). Estimates of device CAPEX are based on developers’
responses as well as on the historical costs of wave energy prototypes
published in the OES report. The CAPEX of these prototypes ranged from
7500 €/kW to 40 000 €/kW installed, depending on the technology type and
scale (larger scales lead to lower costs per kW). Moreover, the CAPEX of
wave and tidal energy projects typically represents 70% and 61% of the
overall LCOE, for wave and tidal energy projects, respectively [OES2015], [CarbonTrust2011].
Finally, the energy production (which can be expressed by capacity
factor and availability) is the most critical factor for which there are
more differences and uncertainties among developers.

.. figure:: fig/slc_fig5.svg
   :alt: Wave LCOE percentage breakdown by cost centre.
   
   Wave LCOE percentage breakdown by cost centre [OES2015].

.. figure:: fig/slc_fig6.svg
   :alt: Tidal LCOE percentage breakdown by cost Centre.
   
   Tidal LCOE percentage breakdown by cost centre [OES2015].

On the left, the current stage of Deployment is depicted, while on the
right the commercial target is represented. 

ACE metric
^^^^^^^^^^          

For early stages of technology development, it is not always possible to
calculate the LCOE. In these cases, cost proxies may be used instead.
ACE, short for the ratio of the **A**\ verage climate capture width
(ACCW) to the **C**\ haracteristic **C**\ apital **E**\ xpenditure (CCE), is a
benefit-to-cost ratio which can be used to assess the economics of wave
and tidal energy systems.

ACE, expressed in meters per Million Euros (m/M€), has been selected as
an appropriate metric for comparing low TRL WEC concepts, when there
isn’t sufficient (reliable) data for calculating the levelised cost of
energy for a given device [Driscoll2017]. Under this approach it has been
determined that the volume of material, density and material costs track
closely to capital costs, which is a major LCOE driver in WEC and TEC
technologies today [Weber2012]. The relationship between the ACE and the LCOE
metric is presented in the figure below.

.. figure:: fig/slc_fig7.png
   :alt: Comparison between ACE and LCOE metrics for Wave Energy Converters deployed in different locations.
   
   Comparison between ACE and LCOE metrics for Wave Energy Converters deployed in different locations [Andres2012].


Discount Rate
^^^^^^^^^^^^^
         
The discount rate refers to the interest rate used when performing a
discounted cash flow (DCF) analysis to determine the present value of a
future cash flows [Short1995]. A constant discount rate is assumed along the
project lifetime. It is used to calculate the LCOE and NPV.

Recent studies related to the discount rates used in marine energy
projects range from 7-15%, where the higher rates are applied to less
developed technologies, representing higher uncertainty and project
risks [Chozas2014], [Neary2014], [Andres2012].

Project revenues
^^^^^^^^^^^^^^^^           
               
Within the framework of DTOceanPlus, a marine renewable energy project
can generate revenues by selling the produced energy. However,
governmental renewable energy grants may also contribute to the
financing of strategic projects (in year 0).

Energy delivered to the grid can be either i) subsidized by
pre-established Feed in Tariff (FIT) programmes (€/kwh) for a
pre-defined number of years, ii) sold through auctions at a fixed price
agreed on bilateral contracts, iii) or simply sold in the spot market at
market price.

Both FITs and Auctions generally have long-term contracts, usually 15 to
20 years. Within DTOceanPlus, it is assumed that the FIT/Auction value
is constant throughout the project lifetime. It is used to calculate the
annual revenue, NPV and payback period of the ocean renewable energy
project at the selected location [Chozas2014], [Mendonça2012].

Net Present Value (NPV)
^^^^^^^^^^^^^^^^^^^^^^           
                       

For any project which strives towards creating value for the investors
or shareholders of a company, the returns must exceed the total costs of
the project undertaken by the company. The value of a project is the
difference between the revenues generated by the project and the
expenses consumed by the project. The Net Present Value (NPV) consists
of summing all the expected cash flows throughout the project lifetime
discounted to the present using the time value of money [Crundwell2008].

Payback Period
^^^^^^^^^^^^^^           
              
The payback period (PBP) is defined as the point in the project at which
the investor gets their investment back (breakeven). It can be
calculated by determining when the cumulative cash flow (CFC) reaches
zero.

In the figure below, a schematic representation of the cumulative cash flows
and project payback period is given. In the figure, A is the last year
with negative cumulative cash flow, and C is the first year with
positive cumulative cash flow, and B the point where the CFC reaches
zero.

.. figure:: fig/slc_fig8.svg
   :alt: Schematic representation of the Project Payback Period
   
   Schematic representation of the Project Payback Period.


Discounted Payback Period
^^^^^^^^^^^^^^^^^^^^^^^^^                         

The discounted payback period (DPBP) is the same as the payback period
but taking into consideration the time value of money. The main
difference is that the discounted cumulative cash flows are employed
instead [Crundwell2008].

Assumptions
===========

Currency
^^^^^^^^^         

The currency used in DTOceanPlus is Euros (€).

Project Lifetime
^^^^^^^^^^^^^^^^           
                
The project lifetime is the project life expectancy, or the target
lifetime for the deployed project. Although pilot projects may be
designed for shorter lifetimes, offshore renewable energy projects are
typically designed for a service lifetime of 20-25 years (there might be
differences from project to project, and in different stages of
development). For this reason, 20 years is the default project lifetime
in SLC, which can be edited by the user.

Device structural costs
^^^^^^^^^^^^^^^^^^^^^^^         
                       
Within DTOceanPlus project, device structural costs refer to the cost of
materials and fabrication of the structure and prime mover, not accounting for the
costs of the Power Take-Off (PTO) unit(s), which are grouped in PTO costs.

Other Costs
^^^^^^^^^^^           

Within the SLC module, the user is offered the option of introducing
other costs that are not modelled by DTOceanPlus deployment design
modules. These include Project Development costs (which typically
represent 5-6% of the project LCOE) and Monitoring and Miscellaneous
equipment (e.g. sensors, SCADA) which typically also represent about 6%
of the project LCOE.

References
===========
---------

.. [EC2018] European Commission, ‘Advanced Design Tools for Ocean Energy Systems Innovation, Development and Deployment | Projects | H2020 | CORDIS’, 17-Jan-2018. [Online]. Available: https://cordis.europa.eu/project/rcn/214811_en.html. [Accessed: 11-Dec-2019].

.. [Chozas2014]	J. F. Chozas, J. P. Kofoed, and N. E. H. Jensen, User guide - COE Calculation Tool for Wave Energy Converters: ver. 1.6 - April 2014, 1st ed. Denmark: Department of Civil Engineering, Aalborg University, 2014.

.. [OES2015]	OES - IEA, ‘International Levelised Cost of Energy for Ocean Energy Technologies’, 2015.

.. [Astariz2015]	S. Astariz, A. Vazquez, and G. Iglesias, ‘Evaluation and comparison of the levelized cost of tidal, wave, and offshore wind energy’, Journal of Renewable and Sustainable Energy, vol. 7, no. 5, p. 053112, Sep. 2015.

.. [Neary2014]	V. S. Neary et al., ‘Methodology for Design and Economic Analysis of Marine Energy Conversion (MEC) Technologies’, Sandia Report, 2014.

.. [Topper2019]	M. B. R. Topper et al., ‘Reducing variability in the cost of energy of ocean energy arrays’, Renewable and Sustainable Energy Reviews, vol. 112, pp. 263–279, Sep. 2019.

.. [CarbonTrust2011]	The Carbon Trust, ‘Accelerating Marine Energy - The potential for cost reduction - insights from the Carbon Trust Marine Energy Accelerator’, 2011.

.. [Driscoll2017]	F. Driscoll et al., ‘Methodologies of Determining the ACE Wave Energy Prize Metric’, in Proceedings of the Twelfth European Wave and Tidal Energy Conference, 2017.

.. [Weber2012]	J. Weber, ‘WEC Technology Readiness and Performance Matrix – finding the best research technology development trajectory’, 2012.

.. [Andres2012]	A. D. de Andres, J. Maillet, J. Todalshaug, P. Möller, D. Bould, and H. Jeffrey, ‘Techno-Economic Related Metrics for a Wave Energy Converters Feasibility Assessment’, Sustainability, vol. 8, p. 1109, 2016.

.. [Short1995]	W. Short, D. Packey, and T. Holt, ‘A Manual for the Economic Evaluation of Energy Efficiency and Renewable Energy Technologies’, NASA STI/Recon Technical Report N, vol. 95, 1995.

.. [Mendonça2012]	M. Mendonça, Feed-in tariffs: Accelerating the deployment of renewable energy. 2012.

.. [Crundwell2008]	F. K. Crundwell, Finance for engineers: evaluation and funding of capital projects. London: Springer, 2008.

.. [Nava2019]	V. Nava et al., ‘Performance and Energy Yield Tools – alpha version’, DTOceanPlus, 2019.

.. [Nava2019b]	V. Nava et al., ‘Technical Requirements for the Assessment Design Tools’, DTOceanPlus, 2019

