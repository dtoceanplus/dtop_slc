# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
Summary:
  Import different business logics depending on the complexity selected by the
  user
"""

# Import logic for Early Stage  - low complexity    -> 1 (Non-experienced users)
from .cpx1 import Economical1, Financial1, Benchmark1
# Import logic for Mid Stage    - medium complexity -> 2 (Regular users)
from .cpx2 import Economical2, Financial2, Benchmark2
# Import logic for Late Stage   - high complexity   -> 3 (Advanced users)
from .cpx3 import Economical3, Financial3, Benchmark3

class Economical:
  """

  """
  @staticmethod
  def get_complexity(
    complexity,
    proj_life=None,
    disc_rate=None,
    bom_compiled=None,
    cost_of_equipment=None,
    cost_of_installation=None,
    cost_other=None,
    maintenance_solution=None,
    array_avr_energy_prod=None,
    compute_ace=None,
    accw=None,
    device_surface=None,
    device_thick=None,
    mat_dens=None,
    mat_cost=None
  ):
    try:
      if complexity == 'low':
        return Economical1(
          proj_life,
          bom_compiled,
          cost_of_equipment,
          cost_of_installation,
          cost_other,
          maintenance_solution,
          compute_ace,
          accw,
          device_surface,
          device_thick,
          mat_dens,
          mat_cost
        )
      elif complexity == 'med':
        return Economical2(
          proj_life,
          disc_rate,
          bom_compiled,
          cost_of_equipment,
          cost_of_installation,
          cost_other,
          maintenance_solution,
          array_avr_energy_prod,
          compute_ace,
          accw,
          device_surface,
          device_thick,
          mat_dens,
          mat_cost
        )
      elif complexity == 'high':
        return Economical3(
          proj_life,
          disc_rate,
          bom_compiled,
          cost_of_equipment,
          cost_of_installation,
          cost_other,
          maintenance_solution,
          array_avr_energy_prod,
          compute_ace,
          accw,
          device_surface,
          device_thick,
          mat_dens,
          mat_cost
        )
      raise AssertionError('Complexity not found')

    except AssertionError as _e:
      print(_e)

class Financial:
  """

  """
  @staticmethod
  def get_complexity(
    complexity,
    proj_life=None,
    disc_rate=None,
    array_avr_energy_prod=None,
    financial_grant=None,
    financial_fit=None,
    market_price=None,
    grant_value = None,
    feed_in_tariff_value=None,
    feed_in_tariff_years=None,
    expenses_overall=None
  ):
    try:
      if complexity == 'low':
        return Financial1()
      elif complexity == 'med':
        return Financial2(
          proj_life,
          disc_rate,
          array_avr_energy_prod,
          financial_grant,
          financial_fit,
          market_price,
          grant_value,
          feed_in_tariff_value,
          feed_in_tariff_years,
          expenses_overall
          )
      elif complexity == 'high':
        return Financial3(
          proj_life,
          disc_rate,
          array_avr_energy_prod,
          financial_grant,
          financial_fit,
          market_price,
          grant_value,
          feed_in_tariff_value,
          feed_in_tariff_years,
          expenses_overall
        )
      raise AssertionError('Complexity not found')

    except AssertionError as _e:
      print(_e)

class Benchmark:
  """

  """
  @staticmethod
  def get_complexity(
    complexity,
    proj_life=None,
    disc_rate=None,
    expenses_overall=None,
    capex_total=None,
    opex_total=None,
    disc_costs=None,
    device_power=None,
    number_of_devices=None
  ):
    try:
      if complexity == 'low':
        return Benchmark1(
          proj_life,
          capex_total,
          opex_total,
          device_power,
          number_of_devices
        )
      elif complexity == 'med':
        return Benchmark2(
          proj_life,
          disc_rate,
          expenses_overall,
          capex_total,
          opex_total,
          disc_costs,
          device_power,
          number_of_devices
        )
      elif complexity == 'high':
        return Benchmark3(
          proj_life,
          disc_rate,
          expenses_overall,
          capex_total,
          opex_total,
          disc_costs,
          device_power,
          number_of_devices
        )
      raise AssertionError('Complexity not found')

    except AssertionError as _e:
      print(_e)

# if __name__ == "__main__":
#   Economical()
#   Financial()
#   Benchmark()
