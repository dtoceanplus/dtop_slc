# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

class Benchmark3(object):
  """
  Benchmark3: The class assesses a set of

  Arguments:

  Optional Arguments:

  Return:

  """

  complexity = "high"

  def __init__(self,
               proj_life=None,
               disc_rate=None,
               expenses_overall=None,
               capex_total=None,
               opex_total=None,
               disc_costs=None,
               device_power=None,
               number_of_devices=None
               ):
    # Inputs
    self.lifetime = proj_life
    self.disc_rate = disc_rate
    self.expenses = expenses_overall
    self.capex_total = capex_total
    self.opex_total = opex_total
    self.disc_costs = disc_costs
    self.device_power = device_power
    self.device_num = number_of_devices

    # Outputs
    self.capex_kw = None
    self.opex_kw = None
    self.other_breakdown = None
    self.grid_breakdown = None
    self.device_breakdown = None
    self.moor_found_breakdown = None
    self.installation_breakdown = None
    self.opex_breakdown = None

    # Run initializing functions
    # self.get_inputs()
    self.convert_check_inputs()
    # self.print_inputs()

  def get_inputs(self):
    return {
      "Discout Rate": self.disc_rate,
      "Overall Expenses": self.expenses,
      "Total CAPEX": self.capex_total,
      "Total OPEX": self.opex_total,
      "Discounted Costs": self.disc_costs,
      "Device Power": self.device_power,
      "Number of Devices": self.device_num
    }

  def print_inputs(self):
    print('Discout Rate: %s' % self.disc_rate)
    print('Overall Expenses: %s' % self.expenses)
    print('Total CAPEX: %s' % self.capex_total)
    print('Total OPEX: %s' % self.opex_total)
    print('Discounted Costs: %s' % self.disc_costs)
    print('Device Power: %s' % self.device_power)
    print('Number of Devices: %s' % self.device_num)

  def convert_check_inputs(self):
    # Project lifetime
    if self.lifetime is not None:
      try:
        self.lifetime = int(self.lifetime)
      except ValueError as _e:
        return AssertionError('Project lifetime must be an integer.')
    # Discount rate
    if self.disc_rate is not None:
      try:
        self.disc_rate = float(self.disc_rate)
      except ValueError as _e:
        return AssertionError(_e)
    # CAPEX Total
    if self.capex_total is not None:
      try:
        self.capex_total = float(self.capex_total)
      except ValueError as _e:
        return AssertionError(_e)
    # OPEX Total
    if self.opex_total is not None:
      try:
        self.opex_total = float(self.opex_total)
      except ValueError as _e:
        return AssertionError(_e)
    # Discounted costs
    if self.disc_costs is not None:
      try:
        self.disc_costs = float(self.disc_costs)
      except ValueError as _e:
        return AssertionError(_e)
    # Device Power
    if self.device_power is not None:
      try:
        self.device_power = float(self.device_power)
      except ValueError as _e:
        return AssertionError(_e)
    # Number of devices
    if self.device_num is not None:
      try:
        self.device_num = int(self.device_num)
      except ValueError as _e:
        return AssertionError(_e)

  def metrics_per_kw(self):
    df_expenses = self.expenses
    if self.capex_total == None:
      return AssertionError('Total CAPEX not defined')
    elif self.device_power == None:
      return AssertionError('Total installed power not defined')
    elif self.device_num == None:
      return AssertionError('Number of devices not defined')
    self.capex_kw = self.capex_total / (self.device_power * self.device_num)

    if self.opex_total == None:
      return AssertionError('Total OPEX not defined')
    # elif self.device_power == None:
    #   return AssertionError('Device installed power not defined')
    # elif self.device_num == None:
    #   return AssertionError('Number of devices not defined')
    install_power = (self.device_power * self.device_num)
    self.opex_kw = self.opex_total / install_power / self.lifetime

    return {
      "CAPEX per kW": self.capex_kw,
      "OPEX per kW": self.opex_kw
    }

  def lcoe_breakdown(self):
    df_expenses = self.expenses
    # Other costs
    other_costs = df_expenses[df_expenses['category']=='Other']['cost'].sum()
    self.other_breakdown = other_costs / self.disc_costs
    # Grid costs
    grid_costs = df_expenses[df_expenses['category']=='Grid']['cost'].sum()
    self.grid_breakdown = grid_costs / self.disc_costs
    # Device costs
    device_costs = df_expenses[df_expenses['category']=='Device']['cost'].sum()
    self.device_breakdown = device_costs / self.disc_costs
    # Mooring and Foundation costs
    moor_found_costs = df_expenses[df_expenses['category']=='Moor_Found']['cost'].sum()
    self.moor_found_breakdown = moor_found_costs / self.disc_costs
    # Other costs
    installation_costs = df_expenses[df_expenses['category']=='Installation']['cost'].sum()
    self.installation_breakdown = installation_costs / self.disc_costs

    # OPEX costs - The OPEX costs must be discounted
    df_expenses_opex = df_expenses[df_expenses['category']=='OPEX']
    df_expenses_opex_group = df_expenses_opex.groupby(['proj_year']).sum()
    df_expenses_opex_group = df_expenses_opex_group.reset_index()

    r = self.disc_rate/100    # convert disc_rate from %
    list_opex_costs = [row['cost']/((1+r)**row['proj_year'])
                       for idx,row in df_expenses_opex_group.iterrows()]
    self.opex_breakdown = sum(list_opex_costs) / self.disc_costs

    # Convert to percentage
    self.other_breakdown = self.other_breakdown * 100
    self.grid_breakdown = self.grid_breakdown * 100
    self.device_breakdown = self.device_breakdown * 100
    self.moor_found_breakdown = self.moor_found_breakdown * 100
    self.installation_breakdown = self.installation_breakdown * 100
    self.opex_breakdown = self.opex_breakdown * 100

    return {
      "Other": self.other_breakdown,
      "Grid": self.grid_breakdown,
      "Device": self.device_breakdown,
      "Mooring and Foundation": self.moor_found_breakdown,
      "Installation": self.installation_breakdown,
      "OPEX": self.opex_breakdown
    }
