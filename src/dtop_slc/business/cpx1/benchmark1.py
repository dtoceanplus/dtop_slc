# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

class Benchmark1(object):
  """
  Benchmark1: The class assesses a set of

  Arguments:

  Optional Arguments:

  Return:

  """

  complexity = "low"

  def __init__(self,
               proj_life=None,
               capex_total=None,
               opex_total=None,
               device_power=None,
               number_of_devices=None
               ):
    # Inputs
    self.lifetime = proj_life
    self.capex_total = capex_total
    self.opex_total = opex_total
    self.device_power = device_power
    self.device_num = number_of_devices

    # Outputs
    self.capex_kw = None
    self.opex_kw = None

    # Run initializing functions
    # self.get_inputs()
    self.convert_check_inputs()
    # self.print_inputs()

  def get_inputs(self):
    return {
      "Total CAPEX": self.capex_total,
      "Total OPEX": self.opex_total,
      "Device Power": self.device_power,
      "Number of Devices": self.device_num
    }

  def print_inputs(self):
    print('Total CAPEX: %s' % self.capex_total)
    print('Total OPEX: %s' % self.opex_total)
    print('Device Power: %s' % self.device_power)
    print('Number of Devices: %s' % self.device_num)

  def convert_check_inputs(self):
    # Project lifetime
    if self.lifetime is not None:
      try:
        self.lifetime = int(self.lifetime)
      except ValueError as _e:
        return AssertionError('Project lifetime must be an integer.')
    # CAPEX Total
    if self.capex_total is not None:
      try:
        self.capex_total = float(self.capex_total)
      except ValueError as _e:
        return AssertionError(_e)
    # OPEX Total
    if self.opex_total is not None:
      try:
        self.opex_total = float(self.opex_total)
      except ValueError as _e:
        return AssertionError(_e)
    # Device Power
    if self.device_power is not None:
      try:
        self.device_power = float(self.device_power)
      except ValueError as _e:
        return AssertionError(_e)
    # Number of devices
    if self.device_num is not None:
      try:
        self.device_num = int(self.device_num)
      except ValueError as _e:
        return AssertionError(_e)

  def metrics_per_kw(self):
    if self.capex_total == None:
      return AssertionError('Total CAPEX not defined')
    elif self.device_power == None:
      return AssertionError('Total installed power not defined')
    elif self.device_num == None:
      return AssertionError('Number of devices not defined')
    self.capex_kw = self.capex_total / (self.device_power * self.device_num)

    if self.opex_total == None:
      return AssertionError('Total OPEX not defined')
    # elif self.device_power == None:
    #   return AssertionError('Device installed power not defined')
    # elif self.device_num == None:
    #   return AssertionError('Number of devices not defined')
    install_power = (self.device_power * self.device_num)
    self.opex_kw = self.opex_total / install_power / self.lifetime

    return {
      "CAPEX per kW": self.capex_kw,
      "OPEX per kW": self.opex_kw
    }
