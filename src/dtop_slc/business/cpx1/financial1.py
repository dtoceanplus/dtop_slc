# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import numpy as np

class Financial1(object):
  """
  Financial1: The class assesses a set of

  Arguments:

  """

  complexity = "low"

  def __init__(self):
    # Run initializing functions
    # self.get_inputs()
    pass

  def get_inputs(self):
    return {
      "Level of Complexity": self.complexity
    }

  def print_inputs(self):
    print('Level of Complexity: %s' % self.complexity)
