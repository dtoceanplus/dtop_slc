# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from dtop_slc import business

import os
import pandas as pd

PATH_ROOT = os.getcwd()

def bom_compiler(bom_et=None,
                 bom_ed=None,
                 bom_sk=None,
                 bom_lmo=None,
                 device_topology=None,
                 device_struct_cost=None,
                 number_of_devices=None,
                 other_costs=None):
    """
    bom_compiler: Compiles every module Bill of Materials into a single one

    Inputs:
      bom_et = Bill of Materials related to Energy Transformation - pandas DataFrame
      bom_ed = Bill of Materials related to Energy Delivery - pandas DataFrame
      bom_sk = Bill of Materials related to Station Keeping - pandas DataFrame
      bom_lmo = Bill of Materials related to Logistics and Marine Operations - pandas DataFrame
      device_topology = Topology of the devices - string
      device_struct_cost = Cost of each device structure - float
      number_of_devices = Total number of devices - integer
      other_costs = Other costs of the project, inclusing project development, etc. - float

    Outputs:
      bom_compiled = Aggregated Bill of Materials - pandas DataFrame
      cost_of_equipment = Cost of installed equipment - float
      cost_of_installation = Cost of installation - float
      other_costs = Other costs of the project - float
    """
    # Convert BoMs to pandas DataFrames
    bom_et = pd.DataFrame(bom_et)
    bom_ed = pd.DataFrame(bom_ed)
    bom_sk = pd.DataFrame(bom_sk)
    bom_lmo = pd.DataFrame(bom_lmo)

    # Add 'category' column to every BOM
    bom_et.insert(loc=bom_et.shape[1], column='category', value='Device')
    bom_ed.insert(loc=bom_ed.shape[1], column='category', value='Grid')
    bom_sk.insert(loc=bom_sk.shape[1], column='category', value='Moor_Found')
    bom_lmo.insert(loc=bom_lmo.shape[1], column='category', value='Installation')

    # Convert and test inputs
    ## Device topology
    if device_topology == None:
      topology = 'n.d.'
    else:
      topology = device_topology
    ## Device cost
    if device_struct_cost is not None:
      try:
        device_struct_cost = float(device_struct_cost)
      except ValueError:
        _e = 'Device cost must be a float'
        return AssertionError(_e)
    else:
      device_struct_cost = 0.0
    ## Number of devices
    if number_of_devices is not None:
      try:
        number_of_devices = int(number_of_devices)
      except ValueError:
        _e = 'The Number of devices must be an integer'
        return AssertionError(_e)
    else:
      number_of_devices = 0
    ## Number of devices
    if other_costs is not None:
      try:
        other_costs = float(other_costs)
      except ValueError:
        _e = 'Other costs value must be a float'
        return AssertionError(_e)
    else:
      other_costs = 0.0

    list_device_bom = [
        'Device',
        topology,
        number_of_devices,
        None,
        device_struct_cost,
        device_struct_cost * number_of_devices,
        'Device'
    ]
    list_device_total_bom = [
        'Tot_Device',
        'Device total cost',
        None,
        None,
        None,
        device_struct_cost * number_of_devices,
        'Device'
    ]
    list_other_bom = [
        'Tot_Other',
        'Other costs',
        None,
        None,
        None,
        other_costs,
        'Other'
    ]
    list_columns = [
        'id',
        'name',
        'qnt',
        'uom',
        'unit_cost',
        'total_cost','category'
    ]
    df_bom_device = pd.DataFrame([list_device_bom, list_device_total_bom],
                                 columns=list_columns)
    df_bom_other = pd.DataFrame([list_other_bom], columns=list_columns)

    # Check if all columns match
    # list_columns_et = bom_et.columns.tolist()
    # list_columns_ed = bom_ed.columns.tolist()
    # list_columns_sk = bom_sk.columns.tolist()
    # list_columns_lmo = bom_lmo.columns.tolist()
    # list_columns_device = df_bom_device.columns.tolist()
    # list_columns_other = df_bom_other.columns.tolist()

    # _e = 'The columns of BoMs are different between each other'
    # if list_columns_et != list_columns_ed:
    #   print(_e)
    #   return AssertionError(_e)
    # elif list_columns_ed != list_columns_sk:
    #   print(_e)
    #   return AssertionError(_e)
    # elif list_columns_sk != list_columns_lmo:
    #   print(_e)
    #   return AssertionError(_e)
    # elif list_columns_lmo != list_columns_device:
    #   print(_e)
    #   return AssertionError(_e)
    # elif list_columns_lmo != list_columns_other:
    #   print(_e)
    #   return AssertionError(_e)

    df_bom = pd.concat(
        [
            df_bom_device,
            bom_et,
            bom_ed,
            bom_sk,
            bom_lmo,
            df_bom_other
        ],
        ignore_index=True
    )

    # Filter compiled BoM. Delete if no 'Tot_' is found in 'id' column.
    df_bom_filter = df_bom[df_bom['id'].str.contains('Tot_')]

    if bom_lmo.shape[0] > 0:
      # Filter LMO BoM. Delete if no 'Tot_' is found in 'id' column.
      df_bom_lmo_filter = bom_lmo[bom_lmo['id'].str.contains('Tot_')]
    else:
      df_bom_lmo_filter = pd.DataFrame([0], columns=['total_cost'])


    try:
      df_bom_filter['total_cost'] = df_bom_filter['total_cost'].astype(float)
    except ValueError as _e:
      print('Could not convert total_cost to floats.')
      print('Check if all values can be converted.')
      return _e
    cost_total = df_bom_filter.loc[:,'total_cost'].sum()
    cost_total = float(cost_total)
    cost_of_installation = df_bom_lmo_filter.loc[:,'total_cost'].sum()
    cost_of_installation = float(cost_of_installation)
    cost_of_equipment = cost_total - cost_of_installation - other_costs

    bom_compiled = {
      "BoM": df_bom,
      "Cost_of_equipment": cost_of_equipment,
      "Cost_of_installation": cost_of_installation,
      "Other_costs": other_costs
    }
    return bom_compiled
