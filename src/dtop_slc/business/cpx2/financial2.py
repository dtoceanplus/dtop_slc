# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import numpy as np

class Financial2(object):
  """
  Financial2: The class assesses a set of

  Arguments:

  Optional Arguments:
  proj_life = Project lifetime - integer
  disc_rate = Discount rate - float
  array_avr_energy_prod = Array average Annual Energy production per year - list of floats
  financial_grant = A Grant is given for this project - boolean
  financial_fit = A Feed-In-Tariff is applied for this project - boolean
  market_price = Energy Market Price - float
  grant_value = Grant value - integer
  feed_in_tariff_value = Feed-in Tariff value (fixed value for all years) - float
  feed_in_tariff_years = Number of years of Feed-in Tariff - integer
  expenses_overall = Project overall expenses - pandas Dataframe

  Anciliary Outputs:
  cf =  Cash Flows - list
  dcf = Discounted Cash Flows - list

  Return:
  pbp = Payback Periode - float
  irr = Internal Rate of Return - float
  npv = Net Present Value - float
  """

  complexity = "med"

  def __init__(self,
               proj_life=None,
               disc_rate=None,
               array_avr_energy_prod=None,
               financial_grant=None,
               financial_fit=None,
               market_price=None,
               grant_value = None,
               feed_in_tariff_value=None,
               feed_in_tariff_years=None,
               expenses_overall=None
               ):
    # Inputs
    self.lifetime = proj_life
    self.disc_rate = disc_rate
    self.aep = array_avr_energy_prod
    self.grant = financial_grant
    self.fit = financial_fit
    self.elect_price = market_price
    self.grant_value = grant_value
    self.fit_value = feed_in_tariff_value
    self.fit_years = feed_in_tariff_years
    self.expenses = expenses_overall

    # Anciliary variables
    self.cf = None
    self.dcf = None

    # Outputs
    self.pbp = None
    self.irr = None
    self.npv = None

    # Run initializing functions
    #self.get_inputs()
    self.convert_check_inputs()
    #self.print_inputs()

  def get_inputs(self):
    return {
      "Level of Complexity": self.complexity,
      "Project Lifetime": self.lifetime,
      "Discount Rate": self.disc_rate,
      "Annual Energy Production per Year": self.aep,
      "Financial Grant": self.grant,
      "Financial FIT": self.fit,
      "Energy market price": self.elect_price,
      "Grant value": self.grant_value,
      "Feed-in Tariff value": self.fit_value,
      "Feed-in Tariff years": self.fit_years
    }

  def print_inputs(self):
    print('Level of Complexity: %s' % self.complexity)
    print('Project Lifetime: %s' % self.lifetime)
    print('Discount Rate: %s' % self.disc_rate)
    print('Annual Energy Production per Year: %s' % self.aep)
    print('Financial Grant: %s' % self.grant)
    print('Financial FIT: %s' % self.fit)
    print('Energy market price: %s' % self.elect_price)
    print('Grant value: %s' % self.grant_value)
    print('Feed-in Tariff value: %s' % self.fit_value)
    print('Feed-in Tariff years: %s' % self.fit_years)

  def convert_check_inputs(self):
    # Project lifetime
    if self.lifetime is not None:
      try:
        self.lifetime = int(self.lifetime)
      except ValueError as _e:
        return AssertionError('Project lifetime must be an integer.')
    # Discunt rate
    if self.disc_rate is not None:
      try:
        self.disc_rate = float(self.disc_rate)
      except ValueError as _w:
        return AssertionError('Discunt rate must be a float.')
    else:
      self.disc_rate = 10
      _w = 'A discount rate of 10% was considered'
      print(_w)
    # Annual Energy Production
    if self.aep is not None:
      aep_year = self.aep.values()
      aep_list = list(aep_year)
      try:
        self.aep = [float(aep_list[y]) for y in range(0, len(aep_list))]
      except ValueError as _e:
        return AssertionError('At least one value of the AEP list is not a float.')
    # Energy market price
    if self.elect_price is not None:
      if self.elect_price != '':
        try:
          self.elect_price = float(self.elect_price)
        except ValueError as _w:
          print('Energy market price must be a float.')
          return AssertionError(_w)
    if self.grant is True:
      # Grant strategy enabled
      if self.grant_value != '' and self.grant_value is not None:
        try:
          self.grant_value = float(self.grant_value)
        except ValueError:
          return AssertionError('Grant value must be a float.')
      else:
        return AssertionError('You selected "Grant" strategy, please define a \
          Grant value.')
    if self.fit is True:
      # FIT strategy enabled
      if self.fit_value != '' and self.fit_value is not None:
        try:
          self.fit_value = float(self.fit_value)
        except ValueError:
          return AssertionError('Feed-in-Tariff value must be a float.')
      else:
        return AssertionError('You selected "FIT" strategy, please define a \
          Feed-In-Tariff value.')
      # Feed-in-Tariff years
      if self.fit_years != '' and self.fit_years is not None:
        try:
          self.fit_years = int(self.fit_years)
        except ValueError:
          return AssertionError('Years of the Feed-in Tariff must be an integer.')
        if self.fit_years > self.lifetime:
          _e = 'The number of years that the FIT will be applied can not be \
              greater than the project lifetime'
          return AssertionError(_e)

  def payback_period(self, disc_pbp=False):
    if self.cf == None:
      self.cashflows_compiler()

    if disc_pbp == False:
      cashflows = self.cf
    else:
      # Discounted payback of investment required
      cashflows = self.dcf
    if sum(cashflows) < 0: # cashflows already include investments
      _w = 'Insufficient cashflows. Investment > Revenue.'
      if disc_pbp == False:
        _w = _w + '\tNo payback period in %d years.' % self.lifetime
      else:
        _w = _w + '\tNo discounted payback period in %d years.' % self.lifetime
      print(_w)
      return
    else:
      cumulative = [sum(cashflows[:y])
                    for y in range(0, len(cashflows))]
      for y in range(0,len(cumulative)):
        if cumulative[y] > 0:
          break
      years = y

      A = years - 1
      B = cumulative[years]
      C = cumulative[years-1] - cumulative[years]

      self.pbp = A + (B/C)
    return self.pbp

  def internal_rate_return(self):
    try:
      irr = np.irr(self.cf)
    except TypeError as _e:
      self.cashflows_compiler()
      irr = np.irr(self.cf)

    irr = np.nan
    if np.isnan(irr):
      _w = 'No return in %d years.' % self.lifetime
      print(_w)
      return

    self.irr = irr
    return self.irr

  def net_present_value(self):
    # Discount rate is given in % so it needs to be divided per 100
    rate = self.disc_rate / 100
    try:
      npv = np.npv(rate, self.cf)
    except TypeError as _e:
      self.cashflows_compiler()
      npv = np.npv(rate, self.cf)

    self.npv = npv
    return self.npv

  def cashflows_compiler(self):
    """
    cashflows_compiler:

    returns:
      cf = Cash Flows - list of floats
    """

    aep_year = self.aep
    expenses_overall = self.expenses
    expenses_unique = expenses_overall.groupby('proj_year',as_index=False).sum()

    # Price of energy for each year of the project
    if self.fit is True:
      # The user defined a fit strategy
      if self.fit_years == None or self.fit_years == '':
        # fit for the project lifetime
        price_elect_year = [self.fit_value]*self.lifetime
        price_elect_year = price_elect_year
      else:
        # The user defined the number years of the fit
        price_elect_year = [self.fit_value] * self.fit_years + \
                          [self.elect_price] * (self.lifetime - self.fit_years)
        price_elect_year = price_elect_year
    else:
      price_elect_year = [self.elect_price] * self.lifetime
      price_elect_year = price_elect_year

    expenses = []
    returns = []
    cf = []    # Free cash flow
    for y in range(0, self.lifetime + 1):
      # For year 0 it's different - there is no returns
      if y == 0:
        if self.grant is True:
          returns.append(self.grant_value)
        else:
          returns.append(0)
        year_y_costs = expenses_unique[expenses_unique['proj_year']==y]['cost'].item()
        expenses.append(year_y_costs)
      else:
        returns.append(price_elect_year[y-1]*aep_year[y-1])
        if y in expenses_unique['proj_year'].tolist():
          year_y_costs = expenses_unique[expenses_unique['proj_year']==y]['cost'].item()
          expenses.append(year_y_costs)
        else:
          expenses.append(0)
      cf.append(returns[y]-expenses[y])

    # Discount rate is given in % so it needs to be divided per 100
    rate = self.disc_rate / 100
    dcf = [cf[y] * 1/(1+rate)**y
            for y in range(0, self.lifetime+1)]

    self.cf = cf
    self.dcf = dcf

    cash_flows = {
      "cf": self.cf,
      "dcf": self.dcf
    }
    print('Cash Flows')
    print(cash_flows["cf"])
    print('Discounted Cash Flows')
    print(cash_flows["dcf"])
    print('\n')
    return cash_flows
