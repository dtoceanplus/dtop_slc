# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pandas as pd
import numpy as np

import os

from datetime import datetime

DAYS_YEAR = 365.25


class Economical2(object):
  """
  Economical2: The class assesses a set of economical metrics for an early stage MRE project

  Arguments:

  Optional Arguments:
  proj_life = Project lifetime - integer
  disc_rate = Discount rate - float
  bom_compiled = Compiled Bill of Materials of the project
  cost_of_equipment = Cost of equipment - float
  cost_of_installation = Cost of installation - float
  cost_other = Other costs of the project - float
  maintenance_solution = Maintenance solution from Logistics and Marine Operations - pandas DataFrame
  array_avr_energy_prod = Array average Annual Energy production per year - list of floats
  compute_ace = Will it compute ace? - ???
  accw = Average Climate Capture Width - float
  device_surface = Total surface area of the device - float
  device_thick = Representative structural thickness - float
  mat_dens = Density of material - float
  mat_cost = Costs of manufactured material - float

  Return:
  capex_total = Total CAPEX for the project lifetime - float
  capex_table = Table with all data related to CAPEX - pandas Dataframe
  opex_total = Total OPEX for the project lifetime - float
  opex_table = Table with all data related to OPEX - pandas Dataframe
  opex_year = Average OPEX per year - float
  expenses_overall = Project overall expenses - pandas Dataframe
  disc_costs = float
  lcoe = Levelized Cost of Energy - float
  ace = Approximate Cost of Energy - float
  """

  complexity = "med"
  def __init__(self,
               proj_life=None,
               disc_rate=None,
               bom_compiled=None,
               cost_of_equipment=None,
               cost_of_installation=None,
               cost_other=None,
               maintenance_solution=None,
               array_avr_energy_prod=None,
               compute_ace=None,
               accw=None,
               device_surface=None,
               device_thick=None,
               mat_dens=None,
               mat_cost=None
               ):
    # Inputs
    self.lifetime = proj_life
    self.disc_rate = disc_rate
    self.bom = bom_compiled
    self.cost_equip = cost_of_equipment
    self.cost_inst = cost_of_installation
    self.other_costs = cost_other
    self.main_solu = maintenance_solution
    self.aep = array_avr_energy_prod
    self.compute_ace = compute_ace
    self.accw = accw
    self.surface = device_surface
    self.thickness = device_thick
    self.dens = mat_dens
    self.cost_manuf = mat_cost

    # Outputs
    self.capex_total = None
    self.capex_table = None
    self.opex_total = None
    self.opex_table = None
    self.opex_year = None
    self.disc_costs = None
    self.expenses_overall = None
    self.lcoe = None
    self.ace = None

    # Run initializing functions
    # self.get_inputs()
    self.convert_check_inputs()
    # self.print_inputs()

  def get_inputs(self):
    return {
      "Level of Complexity": self.complexity,
      "Project Lifetime": self.lifetime,
      "Discount Rate": self.disc_rate,
      "Compiled Bill of Materials": self.bom,
      "Cost of equipment": self.cost_equip,
      "Cost of installation": self.cost_inst,
      "Other costs of the project": self.other_costs,
      "Maintenance solution from Logistics and Marine Operations": self.main_solu,
      "Compute ACE checkbox": self.compute_ace,
      "Annual Energy Production per Year": self.aep,
      "Average Climate Capture Width": self.accw,
      "Total surface area of the device": self.surface,
      "Representative structural thickness": self.thickness,
      "Density of material": self.dens,
      "Costs of manufactured material": self.cost_manuf
    }

  def print_inputs(self):
    print('Level of Complexity: %s' % self.complexity)
    print('Project Lifetime: %s' % self.lifetime)
    print('Discount Rate: %s' % self.disc_rate)
    print('Compiled Bill of Materials of the Energy Transformation: %s' % self.bom)
    print('Cost of equipment: %s' % self.cost_equip)
    print('Cost of installation: %s' % self.cost_inst)
    print('Other costs of the project: %s' % self.other_costs)
    print('Maintenance solution from Logistics and Marine Operations: %s' % self.main_solu)
    print('Annual Energy Production per Year: %s' % self.aep)
    print('Compute ACE checkbox: %s' % self.compute_ace)
    print('Average Climate Capture Width: %s' % self.accw)
    print('Total surface area of the device: %s' % self.surface)
    print('Representative structural thickness: %s' % self.thickness)
    print('Density of material: %s' % self.dens)
    print('Costs of manufactured material: %s' % self.cost_manuf)

  def convert_check_inputs(self):
    # Project lifetime
    if self.lifetime is not None:
      try:
        self.lifetime = int(self.lifetime)
      except ValueError as _e:
        return AssertionError('Project lifetime must be an integer.')
    # Discunt rate
    if self.disc_rate is not None:
      try:
        self.disc_rate = float(self.disc_rate)
      except ValueError as _e:
        return AssertionError('Discunt rate must be a float.')
    else:
      self.disc_rate = 10
      _w = 'A discount rate of 10% was considered'
      print(_w)
    # Annual Energy Production
    if self.aep is not None:
      aep_year = self.aep.values()
      aep_list = list(aep_year)
      try:
        self.aep = [float(aep_list[y]) for y in range(0, len(aep_list))]
      except ValueError as _e:
        return AssertionError('At least one value of the AEP list is not a float.')
    # Maintenance plan
    self.main_solu = pd.DataFrame(self.main_solu)
    # Compute ACE
    if self.compute_ace:
      ## Average Climate Capture Width
      if self.accw is not None:
        try:
          self.accw = float(self.accw)
        except ValueError:
          return AssertionError('Average Climate Capture Width must be a float.')
      else:
        return AssertionError('You must define a value for ACCW')
      ## Total surface area
      if self.surface is not None:
        try:
          self.surface = float(self.surface)
        except ValueError:
          return AssertionError('Total surface area must be a float.')
      else:
        return AssertionError('You must define a value for Total Surface Area')
      ## Representative structural thickness
      if self.thickness is not None:
        try:
          self.thickness = float(self.thickness)
        except ValueError:
          return AssertionError('Representative structural thickness must be a float.')
      else:
        return AssertionError('You must define a value for Device Structure Thickness')
      ## Density of material
      if self.dens is not None:
        try:
          self.dens = float(self.dens)
        except ValueError:
          return AssertionError('Density of material must be a float.')
      else:
        return AssertionError('You must define a value for Device density')
      ## Costs of manufactured material
      if self.cost_manuf is not None:
        try:
          self.cost_manuf = float(self.cost_manuf)
        except ValueError:
          return AssertionError('Costs of manufactured material must be a float.')
      else:
        return AssertionError('You must define a value for the Cost of Manufacture')

  def capex_compiler(self):
    """
    capex_compiler: Calculates CAPEX from Aggregated BoM and logistic costs (installation costs)
    """
    # Imports Bill of Materials
    try:
      self.bom.head()
    except:
      _e = 'No BOM compiled was inputted'
      return AssertionError(_e)
    df_bill_of_materials = self.bom

    # BOM costs table
    df_bom_cost_filter = df_bill_of_materials.loc[:,['id','total_cost']]

    # Convert costs column to floats
    try:
      sr_bom_as_float = df_bom_cost_filter.loc[:,'total_cost'].astype(float)
    except ValueError as _e:
      print('Could not convert Total_cost to floats.')
      print('Check if all values can be converted.')
      return _e

    capex_value = self.cost_equip + self.cost_inst + self.other_costs
    # CAPEX table - All installation costs discretized as well as the costs of
    # equipment
    capex_cols = ['id','total_cost','proj_year','cost_label','category']
    df_capex_table = pd.DataFrame(columns=capex_cols)
    idx_row = 0
    for idx, row in df_bill_of_materials.iterrows():
      if 'Tot_' in row['id']:
        df_capex_table.loc[idx_row,'id'] = row['id']
        df_capex_table.loc[idx_row,'total_cost'] = row['total_cost']
        df_capex_table.loc[idx_row,'category'] = row['category']
        df_capex_table.loc[idx_row,'cost_label'] = 'capex'
        idx_row += 1
    # Fill project year with 0
    df_capex_table = df_capex_table.fillna(0)
    df_capex_table.rename(columns={'total_cost':'cost'}, inplace=True)

    self.capex_total = capex_value
    self.capex_table = df_capex_table

    return {
      "Total CAPEX": self.capex_total,
      "CAPEX table": self.capex_table
    }

  def opex_compiler(self):
    """
    opex_compiler: Calculates the operational costs from the maintenance solution
    """
    # Correct Cost label from Installation Soultion: "OPEX"->"opex"
    self.main_solu['cost_label'] = self.main_solu['cost_label'].str.lower()

    # Identify which rows are related to OPEX
    df_opex_rows = self.main_solu[self.main_solu['cost_label']=='opex']

    df_opex_filter = df_opex_rows.loc[:, ['operation_id', 'proj_year', 'cost']]
    df_opex_filter = df_opex_filter.reset_index()

    # Convert columns to proper types
    try:
      df_opex_filter['cost'] = df_opex_filter['cost'].astype(float)
    except ValueError as _e:
      print('Could not convert costs to floats.')
      print('Check if all values can be converted.')
      return AssertionError(_e)

    # OPEX value - sum of all O&M costs
    cost_of_maintenance = df_opex_filter['cost'].sum()
    opex_value = cost_of_maintenance

    # OPEX table - All maintenance costs discretized
    opex_cols = ['operation_id','cost','proj_year']
    df_opex_table = df_opex_filter[opex_cols]
    df_opex_table.insert(df_opex_table.shape[1], 'cost_label', 'opex')
    df_opex_table.insert(df_opex_table.shape[1], 'category', 'OPEX')
    # Include Breakdown category -> Electrical, foundation, device, other, etc.

    df_opex_table.rename(columns={'operation_id': 'id'}, inplace=True)
    opex_year = opex_value / self.lifetime

    self.opex_total = opex_value
    self.opex_table = df_opex_table
    self.opex_year = opex_year

    return {
      "Total OPEX": self.opex_total,
      "OPEX table": self.opex_table,
      "AVR OPEX per year": self.opex_year
    }

  def cost_compiler(self):
    try:
      self.capex_table.head()
    except:
      self.capex_compiler()
    try:
      self.opex_table.head()
    except:
      self.opex_compiler()

    self.expenses_overall = pd.concat([self.capex_table, self.opex_table])
    self.expenses_overall = self.expenses_overall.reset_index(drop=True)
    # Create a new DF grouped by year, summing costs
    df_expenses_group_year = self.expenses_overall.groupby(['proj_year']).sum()
    df_expenses_group_year = df_expenses_group_year.reset_index()

    r = self.disc_rate/100    # convert disc_rate from %
    list_disc_costs = [row['cost']/((1+r)**row['proj_year'])
                       for idx,row in df_expenses_group_year.iterrows()]
    self.disc_costs = sum(list_disc_costs)
    return {
      "Overall Expenses": self.expenses_overall,
      "Discounted costs": self.disc_costs
    }

  def lcoe_compiler(self):
    # Check if CAPEX table and OPEX table were already calculated
    # If not, run functions that calculate them
    try:
      self.capex_table.head()
    except AttributeError:
      self.capex_compiler()
    try:
      self.opex_table.head()
    except AttributeError:
      self.opex_compiler()

    capex_total = self.capex_total
    df_opex_table = self.opex_table
    rate = self.disc_rate/100   # It's in % so it needs to be divided per 100

    list_cost_year = []     # List of cost per year
    list_cost_year.append(capex_total)
    for y in range(1,self.lifetime+1): #corrigido FCF 24.11.2020
      df_opex_table_y = df_opex_table[df_opex_table['proj_year']==y]
      opex_y = df_opex_table_y['cost'].sum()
      list_cost_year.append(opex_y)

    aep_list = self.aep
    aep_list.insert(0,0)
    npv_money = np.npv(rate, list_cost_year)
    npv_energ = np.npv(rate, aep_list) #FCF 24.11.2020
    #npv_energ = np.npv(rate, self.aep)

    print("\nNPV costs: %.2f €" %npv_money)
    print("\nNPV Energy: %.2f €" %npv_energ)
    print("\nAEP= ")
    print(aep_list)

    self.lcoe = npv_money/npv_energ
    return self.lcoe

  def ace_compiler(self):
    """
    """
    if self.compute_ace:
      cce = self.surface * self.thickness * self.dens * self.cost_manuf
      ace = self.accw / cce
      ace = ace * 1000000   # to convert from m/€ to m/M€
    else:
      ace = None

    self.ace = ace
    return self.ace
