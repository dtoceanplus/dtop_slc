export const getDomain = () => {
	return window.location.hostname.replace(`${process.env.VUE_APP_DTOP_MODULE_SHORT_NAME}.`, '');
}

export const getProtocol = () => {
	return window.location.protocol;
}

export const getAuth = () => {
	return window.location.protocol === 'https:' ? {
		headers: {
			"Authorization": process.env.VUE_APP_DTOP_BASIC_AUTH
		},
		withCredentials: true
	} : {};
}
