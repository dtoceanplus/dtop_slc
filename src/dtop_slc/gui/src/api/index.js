import wretch from 'wretch'

const slc = wretch(process.env.VUE_APP_API_URL)

/* projects */
const project_url = slc.url('/api/projects/')

export { project_url }
