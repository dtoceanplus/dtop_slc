// System Lifetime Costs (SLC) tool.
// SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
// energy projects.
// Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import ProjectCreate from '@/components/projects/ProjectCreate.vue'
import ProjectUpdate from '@/components/projects/ProjectUpdate.vue'
import ProjectOpen from '@/components/projects/ProjectOpen.vue'
import ProjectMain from '@/components/projects/ProjectMain.vue'
import ProjectList from '@/components/projects/ProjectList.vue'
import ProjectResults from '@/components/projects/ProjectResults.vue'
import InputsGeneralCreate from '@/components/projects/inputs/InputsGeneralCreate.vue'
import InputsFinancialCreate from '@/components/projects/inputs/InputsFinancialCreate.vue'
import InputsAceCreate from '@/components/projects/inputs/InputsAceCreate.vue'
import InputsExternalCreate from '@/components/projects/inputs/InputsExternalCreate.vue'
import BOMResults from '@/components/projects/results/BOMResults.vue'
import EconomicResults from '@/components/projects/results/EconomicResults.vue'
import FinancialResults from '@/components/projects/results/FinancialResults.vue'
import BenchmarkResults from '@/components/projects/results/BenchmarkResults.vue'
import ExportStudy from '@/components/projects/results/ExportStudy.vue'
import Catalogue from '@/components/projects/results/Catalogue.vue'
export const constantRoutes = [
  // {
  //   path: '/login',
  //   component: () => import('@/views/login/index'),
  //   hidden: true
  // },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: () => import('@/views/dashboard/index'),
        meta: { title: 'Home', icon: 'dashboard' }
      }
    ]
  },
  {
    path: '/projects',
    component: Layout,
    children: [{
      path: 'create',
      name: 'project-create',
      component: ProjectCreate,
      meta: { title: 'Create project', icon: 'form' }
    }]
  },
  {
    path: '/projects',
    component: Layout,
    children: [{
      path: 'list',
      name: 'list-projects',
      component: ProjectList,
      meta: { title: 'Projects List', icon: 'table' }
    }]
  },
  {
    path: '/',
    component: Layout,
    children: [{
      path: '/projects/:id',
      name: 'project',
      component: ProjectMain,
      meta: { title: 'Projects' },
      props: (route) => {
        const id = Number.parseInt(route.params.id, 10)
        if (Number.isNaN(id)) {
          return 0
        }
        return { id }
      },
      children: [
        {
          name: 'project-update',
          path: '',
          component: ProjectUpdate,
          props: true
        },
        {
          name: 'project-open',
          path: 'home',
          component: ProjectOpen,
          props: true
        },
        {
          name: 'inputs-general',
          path: 'inputs/general',
          component: InputsGeneralCreate,
          props: true
        },
        {
          name: 'inputs-financial',
          path: 'inputs/financial',
          component: InputsFinancialCreate,
          props: (route) => {
            const id = Number.parseInt(route.params.id, 10)
            if (Number.isNaN(id)) {
              return 0
            }
            return { id }
          }
        },
        {
          name: 'inputs-ace',
          path: 'inputs/ace',
          component: InputsAceCreate,
          props: true
        },
        {
          name: 'inputs-external',
          path: 'inputs/external',
          component: InputsExternalCreate,
          props: true
        },
        {
          name: 'project-results',
          path: 'results',
          component: ProjectResults,
          props: true
        },
        {
          name: 'bom-results',
          path: 'results/bom',
          component: BOMResults,
          props: true
        },
        {
          name: 'economic-results',
          path: 'results/economic',
          component: EconomicResults,
          props: true
        },
        {
          name: 'financial-results',
          path: 'results/financial',
          component: FinancialResults,
          props: true
        },
        {
          name: 'benchmark-results',
          path: 'results/benchmark',
          component: BenchmarkResults,
          props: true
        },
        {
          name: 'export-study',
          path: 'export',
          component: ExportStudy,
          props: true
        }
      ],
      hidden: true
    }]
  },
  {
    path: '/catalogue',
    component: Layout,
    children: [{
      path: '',
      name: 'slc-catalogue',
      component: Catalogue,
      meta: { title: 'Catalogue', icon: 'table' }
    }]
  },
  {
    path: 'external-link',
    name: 'Links',
    meta: { title: 'Links', icon: 'link' },
    children: [
      {
        path: 'https://www.dtoceanplus.eu/',
        meta: { title: 'DTOceanPlus' }
      },
      {
        path: 'http://127.0.0.1:5000/api/',
        meta: { title: 'ReDoc (API)' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
