# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import ast

from marshmallow import fields, validates, ValidationError, post_dump

from . import ma, db

from .models import (
    SLCProject,
    InputsGeneral, InputsFinancial, InputsACE, InputsExternal,
    Results
)


class SLCProjectSchema(ma.ModelSchema):
  """Marshmallow schema for SLC Project database model"""

  class Meta:
    model = SLCProject
    sqla_session = db.session

class LoadSLCProjectSchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for creation of a SLC Project.

  Schema validates the provided data against these criteria.
  """
  title = fields.String(
      required=True,
      error_messages={"required": "\"Title\" is required."}
  )
  description = fields.String()
  complexity = fields.String(
      required=True,
      error_messages={"required": "\"complexity\" needs to be specified."}
  )

  @validates("title")
  def validate_title(self, value):
    """Validate title field is not empty"""
    if len(value) == 0:
      raise ValidationError("Title cannot be empty.")

  @validates("complexity")
  def validate_complexity(self, value):
    """Validate complexity field is not empty"""
    if len(value) == 0:
      raise ValidationError("Complexity cannot be empty.")
    """Validate complexity field is \"low\", \"med\" or \"high\""""
    if value.lower() != 'low' and value.lower() != 'med' and value.lower() != 'high':
      raise ValidationError("Complexity must be \"low\", \"med\" or \"high\".")

class UpdateSLCProjectSchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for updating a SLC Project.

  No field is required. Schema validates the provided data against these criteria.
  """
  title = fields.String()
  description = fields.String()
  complexity = fields.String()

  @validates("complexity")
  def validate_complexity(self, value):
    """If complexity field is not empty, Validate if is \"low\", \"med\" or \"high\""""
    if value is not None and len(value) != 0:
      if value.lower() != 'low' and value.lower() != 'med' and value.lower() != 'high':
        raise ValidationError("Complexity must be \"low\", \"med\" or \"high\".")


class InputsGeneralSchema(ma.ModelSchema):
  """Marshmallow schema for General SLC inputs database model"""

  class Meta:
    include_fk = True
    model = InputsGeneral
    sqla_session = db.session

class LoadInputsGeneralSchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for creation of general SLC inputs.

  Schema validates the provided data against these criteria.
  """
  device_type = fields.String(
      required=True,
      error_messages={"required": "\"device_type\" is required."}
  )
  device_topology = fields.String()
  project_life = fields.Integer(
      required=True,
      error_messages={"required": "\"project_life\" is required."}
  )
  discount_rate = fields.Float()
  device_power = fields.Float(
      required=True,
      error_messages={"required": "\"device_power\" is required."}
  )
  device_costs = fields.Float(
      required=True,
      error_messages={"required": "\"device_costs\" is required."}
  )
  device_number = fields.Integer(
      required=True,
      error_messages={"required": "\"device_number\" is required."}
  )
  other_costs = fields.Float()

  @validates("device_type")
  def validate_device_type(self, value):
    """Validate if device_type field is wec or tec"""
    if value.lower() != 'wec' and value.lower() != 'tec':
      raise ValidationError("Device type must be WEC or TEC")

  @validates("device_topology")
  def validate_device_topology(self, value):
    """Validate if device_topology field is floating or fixed"""
    if value.lower() != 'floating' and value.lower() != 'fixed':
      raise ValidationError("Device type must be floating or fixed")

  @validates("project_life")
  def validate_project_life(self, value):
    """Validate project_life greather than zero"""
    if value < 1:
      raise ValidationError("Project life must be a positive number.")

  @validates("discount_rate")
  def validate_discount_rate(self, value):
    """Validate discount_rate greather or equal to zero"""
    if value < 0:
      raise ValidationError("Discount rate cannot be a negative number.")

  @validates("device_power")
  def validate_device_power(self, value):
    """Validate device_power greather than zero"""
    if value <= 0:
      raise ValidationError("Device power must be greather than zero.")

  @validates("device_costs")
  def validate_device_costs(self, value):
    """Validate device_costs greather or equal to zero"""
    if value < 0:
      raise ValidationError("Device costs cannot be a negative number.")

  @validates("device_number")
  def validate_device_number(self, value):
    """Validate device_number greather than zero"""
    if value < 1:
      raise ValidationError("Number of devices must be greather than zero.")

  @validates("other_costs")
  def validate_other_costs(self, value):
    """Validate other_costs greather or equal to zero"""
    if value < 0:
      raise ValidationError("Other costs cannot be a negative number.")

class UpdateInputsGeneralSchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for updating general SLC inputs.

  Schema validates the provided data against these criteria.
  """
  device_type = fields.String()
  device_topology = fields.String()
  project_life = fields.Integer()
  discount_rate = fields.Float()
  device_power = fields.Float()
  device_costs = fields.Float()
  device_number = fields.Integer()
  other_costs = fields.Float()

  @validates("device_type")
  def validate_device_type(self, value):
    """Validate if device_type field is wec or tec"""
    if value.lower() != 'wec' and value.lower() != 'tec':
      raise ValidationError("Device type must be WEC or TEC")

  @validates("device_topology")
  def validate_device_topology(self, value):
    """Validate if device_topology field is floating or fixed"""
    if value.lower() != 'floating' and value.lower() != 'fixed':
      raise ValidationError("Device type must be floating or fixed")

  @validates("project_life")
  def validate_project_life(self, value):
    """Validate project_life greather than zero"""
    if value < 1:
      raise ValidationError("Project life must be a positive number.")

  @validates("discount_rate")
  def validate_discount_rate(self, value):
    """Validate discount_rate greather than zero"""
    if value < 0:
      raise ValidationError("Discount rate cannot be a negative number.")

  @validates("device_power")
  def validate_device_power(self, value):
    """Validate device_power greather than zero"""
    if value <= 0:
      raise ValidationError("Device power must be greather than zero.")

  @validates("device_costs")
  def validate_device_costs(self, value):
    """Validate device_costs greather or equal to zero"""
    if value < 0:
      raise ValidationError("Device costs cannot be a negative number.")

  @validates("device_number")
  def validate_device_number(self, value):
    """Validate device_number greather than zero"""
    if value < 1:
      raise ValidationError("Number of devices must be greather than zero.")

  @validates("other_costs")
  def validate_other_costs(self, value):
    """Validate other_costs greather or equal to zero"""
    if value < 0:
      raise ValidationError("Other costs cannot be a negative number.")


class InputsFinancialSchema(ma.ModelSchema):
  """Marshmallow schema for SLC inputs related with financial calculations database model"""

  class Meta:
    include_fk = True
    model = InputsFinancial
    sqla_session = db.session

class LoadInputsFinancialSchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for creation of financial SLC inputs.

  Schema validates the provided data against these criteria.
  """
  energy_price = fields.Float(
      required=True,
      error_messages={"required": "\"accw\" is required."}
  )
  financial_grant = fields.Float()
  fit_price = fields.Float()
  fit_years = fields.Float()

  @validates("energy_price")
  def validate_energy_price(self, value):
    """Validate energy_price greather than zero"""
    if value < 0:
      raise ValidationError("Price of energy cannot be a negative number.")

  @validates("financial_grant")
  def validate_financial_grant(self, value):
    """Validate financial_grant greather than zero"""
    if value < 0:
      raise ValidationError("Grant value cannot be a negative number.")

  @validates("fit_price")
  def validate_fit_price(self, value):
    """Validate fit_price greather than zero"""
    if value < 0:
      raise ValidationError("FIT value cannot be a negative number.")

  @validates("fit_years")
  def validate_fit_years(self, value):
    """Validate fit_years greather than zero"""
    if value <= 0:
      raise ValidationError("Years of FIT must be a positive number.")

class UpdateInputsFinancialSchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for updating financial SLC inputs.

  Schema validates the provided data against these criteria.
  """
  energy_price = fields.Float()
  financial_grant = fields.Float()
  fit_price = fields.Float()
  fit_years = fields.Float()

  @validates("energy_price")
  def validate_energy_price(self, value):
    """Validate energy_price greather than zero"""
    if value < 0:
      raise ValidationError("Price of energy cannot be a negative number.")

  @validates("financial_grant")
  def validate_financial_grant(self, value):
    """Validate financial_grant greather than zero"""
    if value < 0:
      raise ValidationError("Grant value cannot be a negative number.")

  @validates("fit_price")
  def validate_fit_price(self, value):
    """Validate fit_price greather than zero"""
    if value < 0:
      raise ValidationError("FIT value cannot be a negative number.")

  @validates("fit_years")
  def validate_fit_years(self, value):
    """Validate fit_years greather than zero"""
    if value <= 0:
      raise ValidationError("Years of FIT must be a positive number.")


class InputsExternalSchema(ma.ModelSchema):
  """Marshmallow schema for External SLC inputs database model"""

  class Meta:
    include_fk = True
    model = InputsExternal
    sqla_session = db.session


class InputsACESchema(ma.ModelSchema):
  """Marshmallow schema for SLC inputs related with ACE calculations database model"""

  class Meta:
    include_fk = True
    model = InputsACE
    sqla_session = db.session

class LoadInputsACESchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for creation of ACE SLC inputs.

  Schema validates the provided data against these criteria.
  """
  accw = fields.Float(
      required=True,
      error_messages={"required": "\"accw\" is required."}
  )
  surface_area = fields.Float(
      required=True,
      error_messages={"required": "\"surface_area\" is required."}
  )
  surface_thick = fields.Float(
      required=True,
      error_messages={"required": "\"surface_thick\" is required."}
  )
  density = fields.Float(
      required=True,
      error_messages={"required": "\"density\" is required."}
  )
  cost_manufacture = fields.Float(
      required=True,
      error_messages={"required": "\"cost_manufacture\" is required."}
  )

  @validates("accw")
  def validate_accw(self, value):
    """Validate accw greather than zero"""
    if value < 0:
      raise ValidationError("ACCW cannot be a negative number.")

  @validates("surface_area")
  def validate_surface_area(self, value):
    """Validate surface_area greather than zero"""
    if value <= 0:
      raise ValidationError("Surface area must be greather than zero.")

  @validates("surface_thick")
  def validate_surface_thick(self, value):
    """Validate surface_thick greather or equal to zero"""
    if value < 0:
      raise ValidationError("Surface thickness cannot be a negative number.")

  @validates("density")
  def validate_density(self, value):
    """Validate density greather than zero"""
    if value < 0:
      raise ValidationError("Density must be greather than zero.")

  @validates("cost_manufacture")
  def validate_cost_manufacture(self, value):
    """Validate cost_manufacture greather or equal to zero"""
    if value < 0:
      raise ValidationError("Cost of manufacture cannot be a negative number.")

class UpdateInputsACESchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for updating ACE SLC inputs.

  Schema validates the provided data against these criteria.
  """
  accw = fields.Float()
  surface_area = fields.Float()
  surface_thick = fields.Float()
  density = fields.Float()
  cost_manufacture = fields.Float()

  @validates("accw")
  def validate_accw(self, value):
    """Validate accw greather than zero"""
    if value < 0:
      raise ValidationError("ACCW cannot be a negative number.")

  @validates("surface_area")
  def validate_surface_area(self, value):
    """Validate surface_area greather than zero"""
    if value <= 0:
      raise ValidationError("Surface area must be greather than zero.")

  @validates("surface_thick")
  def validate_surface_thick(self, value):
    """Validate surface_thick greather or equal to zero"""
    if value < 0:
      raise ValidationError("Surface thickness cannot be a negative number.")

  @validates("density")
  def validate_density(self, value):
    """Validate density greather than zero"""
    if value < 0:
      raise ValidationError("Density must be greather than zero.")

  @validates("cost_manufacture")
  def validate_cost_manufacture(self, value):
    """Validate cost_manufacture greather or equal to zero"""
    if value < 0:
      raise ValidationError("Cost of manufacture cannot be a negative number.")


class ResultsSchema(ma.ModelSchema):
  """Marshmallow schema for SLC results database model"""

  class Meta:
    include_fk = True
    model = Results
    sqla_session = db.session
