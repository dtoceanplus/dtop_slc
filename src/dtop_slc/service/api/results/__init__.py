# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

import json
import pandas as pd

from flask import Blueprint, request, jsonify
from marshmallow import ValidationError

from ..errors import bad_request, not_found
from ..utils import join_validation_error_messages

from ..projects.slc_projects import update_status

from .slc_results import calculate_results, delete_results

from dtop_slc.service.models import SLCProject
from dtop_slc.service.schemas import ResultsSchema

bp = Blueprint('api_results', __name__)
"""
  @oas [get] /api/projects/<slcid>/results
  description: Add inputs to a SLC projects
"""
@bp.route('/<slcid>/results', methods=['GET'], strict_slashes=False)
def get_slc_results(slcid):
  """
  Flask blueprint route for getting the results of a SLC project.

  If slcid provided does not exist in the database, returns a 404 error.

  Args:
      slcid (str): the ID of the SLC project

  Returns:
      dict: the serialised response containing the selected SLC project or a HTTP 404 error
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.results is None:
    return bad_request("SLC project does not have results yet.")

  slc_results_schema = ResultsSchema()
  slc_results = slc_results_schema.dump(slc_project.results)
  slc_results["bom"] = slc_project.results.bom

  return jsonify(slc_results)

@bp.route('/<slcid>/results', methods=['POST'], strict_slashes=False)
def post_slc_results(slcid):
  """
  Flask blueprint route for performing calculations and adding results to a given SLC project.

  Calls the :meth:`~dtop_slc.service.api.slc_results.calculate_results()` method.
  Returns 404 error if project ID does not exist.
  Returns 404 error if required inputs for this project ID do not exist.

  :return: the json response containing a successful 'Results created' message and the new results data, if successful
  :rtype: flask.wrappers.Response
  """
  # Look for a project with this slcid
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')

  slc_results = slc_project.results
  if slc_results:
    return bad_request('Results for this SLC project already present')

  slc_results = calculate_results(slcid)

  if type(slc_results) is str:
    return bad_request(slc_results)
  if not slc_results:
    return bad_request('Error')
  slc_results_schema = ResultsSchema()

  # Update SLC project status
  update_status(slcid, +50)

  result = {
      "created_results": slc_results_schema.dump(slc_results),
      "message": "Results created"
  }
  result["created_results"]["bom"] = slc_results.bom
  response = jsonify(result)
  response.status_code = 201

  return response

@bp.route('/<slcid>/results', methods=['DELETE'])
def delete_slc_results(slcid):
  """
  Flask blueprint route for deleting SLC results.

  Calls the :meth:`~dtop_slc.service.api.projects.delete_results()` method.
  Returns HTTP 400 error if deletion of SLC results is unsuccessful or
  if SLC project id was not found.

  :param str slcid: the ID of the SLC project to deletet results
  :return: serialised response containing 'Results deleted' message; bad request error otherwise
  :rtype: flask.wrappers.Response
  """
  # Look for a project with this slcid
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')

  if slc_project.results is None:
    return bad_request('Results for this SLC project not present yet')

  data = delete_results(slcid)
  if data == "Results deleted":
    # Update SLC project status
    update_status(slcid, -50)

    response = {
        'project_id': slcid,
        'message': 'Results deleted.'
    }
    return jsonify(response)
  else:
    return bad_request(data)
