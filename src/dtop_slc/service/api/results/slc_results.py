# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from sqlalchemy.exc import IntegrityError
import json
import pandas as pd

from ....business import core, Economical, Financial, Benchmark

from ....service import db

from ...models import SLCProject
from ...schemas import ResultsSchema


def calculate_results(slcid):
  """
  Calculate SLC results based on inputs previously provided

  Args:
    slcid (int): SLC project ID 

  Returns:
    response (dict): Results in a dictionary format
  """
  slc_project = SLCProject.query.get(slcid)
  # Bill of Materials
  bom_compiled = core.bom_compiler(
      bom_et=slc_project.inputs_external.bom_et,
      bom_ed=slc_project.inputs_external.bom_ed,
      bom_sk=slc_project.inputs_external.bom_sk,
      bom_lmo=slc_project.inputs_external.bom_lmo,
      device_topology=slc_project.inputs_general.device_topology,
      device_struct_cost=slc_project.inputs_general.device_costs,
      number_of_devices=slc_project.inputs_general.device_number,
      other_costs=slc_project.inputs_general.other_costs
  )
  bom = bom_compiled['BoM']
  cost_equip = bom_compiled['Cost_of_equipment']
  cost_inst = bom_compiled['Cost_of_installation']
  cost_other = bom_compiled['Other_costs']

  # Economical metrics
  if slc_project.inputs_ace is not None:
    # There are inputs related with ACE, then, compute_ace = True
    compute_ace_bool = True
    accw = slc_project.inputs_ace.accw
    device_surface = slc_project.inputs_ace.surface_area
    device_thick = slc_project.inputs_ace.surface_thick
    mat_dens = slc_project.inputs_ace.density
    mat_cost = slc_project.inputs_ace.cost_manufacture
  else:
    compute_ace_bool = False
    accw = None
    device_surface = None
    device_thick = None
    mat_dens = None
    mat_cost = None

  Economical_metrics =  Economical.get_complexity(
      complexity=slc_project.complexity,
      proj_life=slc_project.inputs_general.project_life,
      disc_rate=slc_project.inputs_general.discount_rate,
      bom_compiled=bom,
      cost_of_installation=cost_inst,
      cost_of_equipment=cost_equip,
      cost_other=cost_other,
      maintenance_solution=slc_project.inputs_external.main_solution,
      array_avr_energy_prod=slc_project.inputs_external.aep,
      compute_ace=compute_ace_bool,
      accw=accw,
      device_surface=device_surface,
      device_thick=device_thick,
      mat_dens=mat_dens,
      mat_cost=mat_cost
  )

  capex = Economical_metrics.capex_compiler()
  opex = Economical_metrics.opex_compiler()

  try:
    costs = Economical_metrics.cost_compiler()
    expenses = costs['Overall Expenses']
    discounted_costs = costs['Discounted costs']
  except AttributeError:
    # Complexity 1
    costs = None
    expenses = None
    discounted_costs = None

  try:
    lcoe = Economical_metrics.lcoe_compiler()
  except AttributeError:
    # Complexity 1
    lcoe = None

  ace = Economical_metrics.ace_compiler()

  # Financial metrics
  market_price = None
  grant_bool = None
  grant_value = None
  fit_bool = None
  fit_price = None
  fit_years = None
  if slc_project.complexity != 'low':
    market_price = slc_project.inputs_financial.energy_price
    if (slc_project.inputs_financial.financial_grant is None or
        slc_project.inputs_financial.financial_grant == 0):
      grant_bool = False
      grant_value = None
    else:
      grant_bool = True
      grant_value = slc_project.inputs_financial.financial_grant
    if (slc_project.inputs_financial.fit_price is None or
        slc_project.inputs_financial.fit_price == 0):
      fit_bool = False
      fit_price = None
      fit_years = None
    else:
      fit_bool = True
      fit_price = slc_project.inputs_financial.fit_price
      fit_years = slc_project.inputs_financial.fit_years

  Financial_metrics = Financial.get_complexity(
      complexity=slc_project.complexity,
      proj_life=slc_project.inputs_general.project_life,
      disc_rate=slc_project.inputs_general.discount_rate,
      array_avr_energy_prod=slc_project.inputs_external.aep,
      financial_grant=grant_bool,
      financial_fit=fit_bool,
      market_price=market_price,
      grant_value=grant_value,
      feed_in_tariff_value=fit_price,
      feed_in_tariff_years=fit_years,
      expenses_overall=expenses
  )

  try:
    pbp = Financial_metrics.payback_period()
    dpbp = Financial_metrics.payback_period(disc_pbp=True)
  except AttributeError:
    pbp = None
    dpbp = None
  try:
    irr = Financial_metrics.internal_rate_return()
  except AttributeError:
    irr = None
  try:
    npv = Financial_metrics.net_present_value()
  except AttributeError:
    npv = None

  # Benchmark metrics
  Benchmark_metrics = Benchmark.get_complexity(
      complexity=slc_project.complexity,
      proj_life=slc_project.inputs_general.project_life,
      disc_rate=slc_project.inputs_general.discount_rate,
      expenses_overall=expenses,
      capex_total=capex['Total CAPEX'],
      opex_total=opex['Total OPEX'],
      disc_costs=discounted_costs,
      device_power=slc_project.inputs_general.device_power,
      number_of_devices=slc_project.inputs_general.device_number
  )
  metrics_kw = Benchmark_metrics.metrics_per_kw()
  capex_kw = metrics_kw['CAPEX per kW']
  opex_kw = metrics_kw['OPEX per kW']

  try:
    lcoe_breakdown = Benchmark_metrics.lcoe_breakdown()
    other_lcoe = lcoe_breakdown['Other']
    grid_lcoe = lcoe_breakdown['Grid']
    device_lcoe = lcoe_breakdown['Device']
    mooring_lcoe = lcoe_breakdown['Mooring and Foundation']
    inst_lcoe = lcoe_breakdown['Installation']
    opex_lcoe = lcoe_breakdown['OPEX']
  except AttributeError:
    other_lcoe = None
    grid_lcoe = None
    device_lcoe = None
    mooring_lcoe = None
    inst_lcoe = None
    opex_lcoe = None

  response = create_results(
      slc_project.id,
      bom,
      lcoe, ace, capex, opex, cost_equip, cost_inst,
      pbp, dpbp, irr, npv,
      capex_kw, opex_kw,
      other_lcoe, grid_lcoe, device_lcoe, mooring_lcoe, inst_lcoe, opex_lcoe
  )

  return response

def create_results(
    slc_project_id,
    bom,
    lcoe, ace, capex, opex, cost_equip, cost_inst,
    pbp, dpbp, irr, npv,
    capex_kw, opex_kw,
    other_lcoe, grid_lcoe, device_lcoe, mooring_lcoe, inst_lcoe, opex_lcoe
):
  # project_id = slc_project.id

  # Replace NaN's with None's
  bom =  bom.where(pd.notnull(bom), 'N/A')
  bom_dict = bom.to_dict('list')

  data = {
      "project_id": slc_project_id,
      "lcoe": lcoe,
      "ace": ace,
      "capex_total": capex['Total CAPEX'],
      "cost_equip": cost_equip,
      "cost_inst": cost_inst,
      "opex_total": opex['Total OPEX'],
      "opex_year": opex['AVR OPEX per year'],
      "pay_back_period": pbp,
      "disc_pay_back_period": dpbp,
      "internal_rate_return": irr,
      "net_present_value": npv,
      "capex_kw": capex_kw,
      "opex_kw": opex_kw,
      "bench_other_lcoe": other_lcoe,
      "bench_grid_lcoe": grid_lcoe,
      "bench_device_lcoe": device_lcoe,
      "bench_mooring_lcoe": mooring_lcoe,
      "bench_inst_lcoe": inst_lcoe,
      "bench_opex_lcoe": opex_lcoe
  }

  slc_results_schema = ResultsSchema()
  slc_results = slc_results_schema.load(data)
  slc_results.bom = bom_dict

  project =  SLCProject.query.get(slc_project_id)
  if project is None:
    db.session.rollback()
    return "SLC project with that ID does not exist."
  db.session.add(slc_results)
  db.session.commit()

  return slc_results

def delete_results(slcid):
  """
  Delete a SLC project results from the local storage database.

  If project with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  :param str slcid: the ID of the SLC project in local storage database to delete results
  :return: error message if ID does not exist, dictionary with deleted status set to True if deleted successfully
  :rtype: dict
  """
  slc_project =  SLCProject.query.get(slcid)
  db.session.delete(slc_project.results)
  db.session.commit()
  return "Results deleted"
