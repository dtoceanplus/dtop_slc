# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

import json

from flask import Blueprint, request, jsonify

bp = Blueprint('api_catalog', __name__)
"""
  @oas [get] /api/catalog
  description: Get benchmark catalog
"""
@bp.route('/', methods=['GET'])
def get_catalog():
  """
  Flask blueprint route for getting the benchmark catalog.

  Returns:
      dict: the serialised response containing the catalog
  """
  # import os
  # file_catalog = open(os.getcwd() + '\\src\\dtop_slc\\service\\catalog\\dtop-catalog.json')
  # data_catalog = json.load(file_catalog)

  data_catalog = [
      {
        "id": 1,
        "technology": "Tidal",
        "stage": "1st array",
        "capex": 9500,
        "opex_year": 600,
        "lcoe": 0.57,
        "break_station_keeping": 8,
        "break_electrical": 8,
        "break_device": 28,
        "break_installation": 12,
        "break_other": 5,
        "break_opex": 39
      },
      {
        "id": 2,
        "technology": "Tidal",
        "stage": "2nd array",
        "capex": 7000,
        "opex_year": 370,
        "lcoe": 0.35,
        "break_station_keeping": 8,
        "break_electrical": 8,
        "break_device": 28,
        "break_installation": 12,
        "break_other": 5,
        "break_opex": 39
      },
      {
        "id": 3,
        "technology": "Tidal",
        "stage": "Commercial",
        "capex": 4500,
        "opex_year": 270,
        "lcoe": 0.22,
        "break_station_keeping": 8,
        "break_electrical": 8,
        "break_device": 28,
        "break_installation": 12,
        "break_other": 5,
        "break_opex": 39
      },
      {
        "id": 4,
        "technology": "Wave",
        "stage": "1st array",
        "capex": 10500,
        "opex_year": 700,
        "lcoe": 0.7,
        "break_station_keeping": 10,
        "break_electrical": 5,
        "break_device": 43,
        "break_installation": 9,
        "break_other": 3,
        "break_opex": 30
      },
      {
        "id": 5,
        "technology": "Wave",
        "stage": "2nd array",
        "capex": 9800,
        "opex_year": 350,
        "lcoe": 0.5,
        "break_station_keeping": 10,
        "break_electrical": 5,
        "break_device": 43,
        "break_installation": 9,
        "break_other": 3,
        "break_opex": 30
      },
      {
        "id": 6,
        "technology": "Wave",
        "stage": "Commercial",
        "capex": 4500,
        "opex_year": 300,
        "lcoe": 0.41,
        "break_station_keeping": 10,
        "break_electrical": 5,
        "break_device": 43,
        "break_installation": 9,
        "break_other": 3,
        "break_opex": 30
      }
  ]

  return jsonify(data_catalog)
