# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import json
import pandas as pd
from sqlalchemy.exc import IntegrityError

from ....service import db

from ...models import (
    SLCProject, InputsGeneral, InputsFinancial, InputsACE, InputsExternal
)
from ...schemas import (
    InputsGeneralSchema, InputsFinancialSchema, InputsACESchema, InputsExternalSchema
)


def create_inputs_general(data):
  """
  Create new SLC project inputs and add it to the local storage database.

  Returns an error message and rolls back the session if the new SLC project has the same name as an existing project in the database.

  :param dict data: dictionary required to create the SLC project object.
  :return: the newly created SLC project instance
  :rtype: dtop_slc.service.models.InputsGeneral
  """
  slc_inputs_schema = InputsGeneralSchema()
  slc_inputs = slc_inputs_schema.load(data)

  try:
    db.session.add(slc_inputs)
    db.session.commit()
    return slc_inputs
  except IntegrityError:
    db.session.rollback()
    return 'SLC general inputs ID already exists.'

def create_inputs_financial(data):
  """
  Create new SLC project inputs and add it to the local storage database.

  Returns an error message and rolls back the session if the new SLC project has the same name as an existing project in the database.

  :param dict data: dictionary required to create the SLC project object.
  :return: the newly created SLC project instance
  :rtype: dtop_slc.service.models.InputsFinancial
  """
  slc_inputs_schema = InputsFinancialSchema()
  slc_inputs = slc_inputs_schema.load(data)

  try:
    db.session.add(slc_inputs)
    db.session.commit()
    return slc_inputs
  except IntegrityError:
    db.session.rollback()
    return 'SLC Financial inputs ID already exists.'

def create_inputs_ace(data):
  """
  Create new SLC project inputs and add it to the local storage database.

  Returns an error message and rolls back the session if the new SLC project has the same name as an existing project in the database.

  :param dict data: dictionary required to create the SLC project object.
  :return: the newly created SLC project instance
  :rtype: dtop_slc.service.models.InputsACE
  """
  slc_inputs_schema = InputsACESchema()
  slc_inputs = slc_inputs_schema.load(data)

  try:
    db.session.add(slc_inputs)
    db.session.commit()
    return slc_inputs
  except IntegrityError:
    db.session.rollback()
    return 'SLC ACE inputs ID already exists.'

def create_inputs_external(data):
  """
  Create new SLC project external inputs and add it to the local storage database.

  Returns an error message and rolls back the session if the new SLC project has the same name as an existing project in the database.

  :param dict data: dictionary required to create the SLC project object.
  :return: the newly created SLC external inputs instance
  :rtype: dtop_slc.service.models.InputsExternal
  """
  slc_inputs_external_schema = InputsExternalSchema()
  slc_inputs_external = slc_inputs_external_schema.load(
      {"project_id": data["project_id"]}
  )

  mc_general_request = data.get('mc_general')
  if mc_general_request is not None:
    slc_inputs_external.mc_general = mc_general_request

  ec_devices_request = data.get('ec_devices')
  if ec_devices_request is not None:
    slc_inputs_external.number_devices = ec_devices_request["number_devices"]

  bom_et_request = data.get('bom_et')
  if bom_et_request is not None:
    bom_et = bom_et_request["BoM"]["value"]
    slc_inputs_external.bom_et = bom_et

  et_power_request = data.get('et_power')
  if et_power_request is not None:
    slc_inputs_external.et_power = et_power_request

  bom_ed_request = data.get('bom_ed')
  if bom_ed_request is not None:
    bom_ed_unformatted = bom_ed_request["b_o_m_new"]
    bom_ed_df = pd.DataFrame.from_dict(bom_ed_unformatted)
    bom_ed_df.reset_index(inplace=True)
    bom_ed_df = bom_ed_df.rename(columns={'index': 'id'})
    bom_ed_dict = bom_ed_df.to_dict('list')
    slc_inputs_external.bom_ed = bom_ed_dict

  bom_sk_request = data.get('bom_sk')
  if bom_sk_request is not None:
    # Addapt SK BoM keys
    sk_bom = {}
    sk_bom["id"] = bom_sk_request["catalogue_ID"]
    sk_bom["name"] = bom_sk_request["product_name"]
    sk_bom["qnt"] = bom_sk_request["quantity"]
    sk_bom["uom"] = bom_sk_request["unit"]
    sk_bom["unit_cost"] = bom_sk_request["unit_cost"]
    sk_bom["total_cost"] = bom_sk_request["total_cost"]
    slc_inputs_external.bom_sk = sk_bom

  bom_lmo_request = data.get('bom_lmo')
  if bom_lmo_request is not None:
    slc_inputs_external.bom_lmo = bom_lmo_request

  main_solution_request = data.get('main_solution')
  if main_solution_request is not None:
    slc_inputs_external.main_solution = main_solution_request

  aep_request = data.get('aep')
  if aep_request is not None:
    aep = aep_request["Outputs_EP"]["array_annual_net_energy_pd"]["value"]
    aep_dict = json.loads(aep)
    slc_inputs_external.aep = aep_dict

  try:
    db.session.add(slc_inputs_external)
    db.session.commit()
    return slc_inputs_external
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error'


def update_inputs_general(data, slcid):
  """
  Update a SLC project inputs in the local storage database.

  Returns an error message if:
  - the SLC project ID that is trying to be updated does not exist or
  - the SLC project that is trying to be updated does not have inputs yet.

  :param dict data: the SLC project inputs data required to update a project in the local database.
  :param str slcid: the ID of the SLC project in the local storage database
  :return: the updated SLC project inputs instance
  :rtype: dtop_slc.service.models.Inputs
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return 'SLC project with that ID does not exist.'
  if slc_project.inputs_general is None:
    return "SLC project does not have general inputs."

  # Update existing project general inputs with existing data from "data")
  device_type = data.get('device_type', None)
  device_topology = data.get('device_topology', None)
  project_life = data.get('project_life', None)
  discount_rate = data.get('discount_rate', None)
  device_power = data.get('device_power', None)
  device_costs = data.get('device_costs', None)
  device_number = data.get('device_number', None)
  other_costs = data.get('other_costs', None)
  if device_type is not None:
    slc_project.inputs_general.device_type = device_type
  if device_topology is not None:
    slc_project.inputs_general.device_topology = device_topology
  if project_life is not None:
    slc_project.inputs_general.project_life = project_life
  if discount_rate is not None:
    slc_project.inputs_general.discount_rate = discount_rate
  if device_power is not None:
    slc_project.inputs_general.device_power = device_power
  if device_costs is not None:
    slc_project.inputs_general.device_costs = device_costs
  if device_number is not None:
    slc_project.inputs_general.device_number = device_number
  if other_costs is not None:
    slc_project.inputs_general.other_costs = other_costs

  # Update database
  try:
    db.session.commit()
    return slc_project.inputs_general
  except IntegrityError:
    db.session.rollback()
    return "Unknown error."

def update_inputs_financial(data, slcid):
  """
  Update a SLC project inputs in the local storage database.

  Returns an error message if:
  - the SLC project ID that is trying to be updated does not exist or
  - the SLC project that is trying to be updated does not have inputs yet.

  :param dict data: the SLC project inputs data required to update a project in the local database.
  :param str slcid: the ID of the SLC project in the local storage database
  :return: the updated SLC project inputs instance
  :rtype: dtop_slc.service.models.Inputs
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return 'SLC project with that ID does not exist.'
  if slc_project.inputs_financial is None:
    return "SLC project does not have Financial inputs."

  # Update existing project financial inputs with existing data from "data"
  energy_price = data.get('energy_price', None)
  financial_grant = data.get('financial_grant', None)
  fit_price = data.get('fit_price', None)
  fit_years = data.get('fit_years', None)
  if energy_price is not None:
    slc_project.inputs_financial.energy_price = energy_price
  if financial_grant is not None:
    slc_project.inputs_financial.financial_grant = financial_grant
  if fit_price is not None:
    slc_project.inputs_financial.fit_price = fit_price
  if fit_years is not None:
    slc_project.inputs_financial.fit_years = fit_years

  # Update database
  try:
    db.session.commit()
    return slc_project.inputs_financial
  except IntegrityError:
    db.session.rollback()
    return "Unknown error."

def update_inputs_ace(data, slcid):
  """
  Update a SLC project inputs in the local storage database.

  Returns an error message if:
  - the SLC project ID that is trying to be updated does not exist or
  - the SLC project that is trying to be updated does not have inputs yet.

  :param dict data: the SLC project inputs data required to update a project in the local database.
  :param str slcid: the ID of the SLC project in the local storage database
  :return: the updated SLC project inputs instance
  :rtype: dtop_slc.service.models.Inputs
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return 'SLC project with that ID does not exist.'
  if slc_project.inputs_ace is None:
    return "SLC project does not have ACE inputs."

  # Update existing project ACE inputs with existing data from "data"
  accw = data.get('accw', None)
  surface_area = data.get('surface_area', None)
  surface_thick = data.get('surface_thick', None)
  density = data.get('density', None)
  cost_manufacture = data.get('cost_manufacture', None)
  if accw is not None:
    slc_project.inputs_ace.accw = accw
  if surface_area is not None:
    slc_project.inputs_ace.surface_area = surface_area
  if surface_thick is not None:
    slc_project.inputs_ace.surface_thick = surface_thick
  if density is not None:
    slc_project.inputs_ace.density = density
  if cost_manufacture is not None:
    slc_project.inputs_ace.cost_manufacture = cost_manufacture

  # Update database
  try:
    db.session.commit()
    return slc_project.inputs_ace
  except IntegrityError:
    db.session.rollback()
    return "Unknown error."


def delete_inputs_general(slcid):
  """
  Delete SLC general inputs from the local storage database.

  If project with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  :param str slcid: the ID of the SLC project in local storage database to delete results
  :return: error message if ID does not exist, success message if inputs are deleted
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return "SLC project with that ID does not exist."
  if slc_project.inputs_general is None:
    return "SLC project with that ID does not have general inputs yet."
  else:
    db.session.delete(slc_project.inputs_general)
    db.session.commit()
    return "Inputs deleted"

def delete_inputs_financial(slcid):
  """
  Delete SLC financial inputs from the local storage database.

  If project with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  :param str slcid: the ID of the SLC project in local storage database to delete results
  :return: error message if ID does not exist, success message if inputs are deleted
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return "SLC project with that ID does not exist."
  if slc_project.inputs_financial is None:
    return "SLC project with that ID does not have financial inputs yet."
  else:
    db.session.delete(slc_project.inputs_financial)
    db.session.commit()
    return "Inputs deleted"

def delete_inputs_ace(slcid):
  """
  Delete SLC ACE inputs from the local storage database.

  If project with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  :param str slcid: the ID of the SLC project in local storage database to delete results
  :return: error message if ID does not exist, success message if inputs are deleted
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return "SLC project with that ID does not exist."
  if slc_project.inputs_ace is None:
    return "SLC project with that ID does not have ACE inputs yet."
  else:
    db.session.delete(slc_project.inputs_ace)
    db.session.commit()
    return "Inputs deleted"

def delete_inputs_external(slcid):
  """
  Delete a External inputs from the local storage database.

  Args:
    lmoid (int): the ID of the LMO study in local storage database to delete cables

  Returns:
    str: Success message if inputs are deleted; Error message if no external inputs exists.
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return "SLC project with that ID does not exist."
  if slc_project.inputs_external is None:
    return 'This SLC project does not have external inputs yet.'
  else:
    db.session.delete(slc_project.inputs_external)
    db.session.commit()
    return 'Inputs deleted'
