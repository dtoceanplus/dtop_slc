# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

from flask import Blueprint, request, jsonify
from marshmallow import ValidationError
import json

from ..errors import bad_request, not_found
from ..utils import join_validation_error_messages

from ..projects.slc_projects import update_status

from .slc_inputs import (
    create_inputs_general, update_inputs_general, delete_inputs_general,
    create_inputs_financial, update_inputs_financial, delete_inputs_financial,
    create_inputs_ace, update_inputs_ace, delete_inputs_ace,
    create_inputs_external, delete_inputs_external
)

from dtop_slc.service.models import SLCProject
from dtop_slc.service.schemas import (
    InputsGeneralSchema, LoadInputsGeneralSchema, UpdateInputsGeneralSchema,
    InputsFinancialSchema, LoadInputsFinancialSchema, UpdateInputsFinancialSchema,
    InputsACESchema, LoadInputsACESchema, UpdateInputsACESchema,
    InputsExternalSchema
)

bp = Blueprint('api_inputs', __name__)
"""
  @oas [get] /api/projects/<slcid>/inputs
  description: Add inputs to a SLC projects
"""
@bp.route('/<slcid>/inputs/general', methods=['GET'], strict_slashes=False)
def get_inputs_general(slcid):
  """
  Flask blueprint route for getting general inputs of a single SLC project.

  Returns an error if this project ID does not have general inputs;
  Returns 404 error if project ID does not exist in the database.

  Args:
      slcid (str): the ID of the SLC project

  Returns:
      dict: the serialised response containing the selected SLC project general inputs or a HTTP 404 error
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.inputs_general is None:
    return "SLC project does not have general inputs."

  slc_inputs_schema = InputsGeneralSchema()
  slc_inputs = slc_inputs_schema.dump(slc_project.inputs_general)

  return jsonify(slc_inputs)

@bp.route('/<slcid>/inputs/financial', methods=['GET'], strict_slashes=False)
def get_inputs_financial(slcid):
  """
  Flask blueprint route for getting Financial inputs of a single SLC project.

  Returns an error if this project ID does not have general inputs;
  Returns 404 error if project ID does not exist in the database.

  :param str slcid: the ID of the SLC project
  :return: the serialised response containing the selected SLC project or a HTTP 404 error
  :rtype: dict
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.inputs_financial is None:
    return "SLC project does not have Financial inputs."

  slc_inputs_schema = InputsFinancialSchema()
  slc_inputs = slc_inputs_schema.dump(slc_project.inputs_financial)

  return jsonify(slc_inputs)

@bp.route('/<slcid>/inputs/ace', methods=['GET'], strict_slashes=False)
def get_inputs_ace(slcid):
  """
  Flask blueprint route for getting ACE inputs of a single SLC project.

  Returns an error if this project ID does not have ACE inputs;
  Returns 404 error if project ID does not exist in the database.

  :param str slcid: the ID of the SLC project
  :return: the serialised response containing the selected SLC project or a HTTP 404 error
  :rtype: dict
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.inputs_ace is None:
    return "SLC project does not have ACE inputs."

  slc_inputs_schema = InputsACESchema()
  slc_inputs = slc_inputs_schema.dump(slc_project.inputs_ace)

  return jsonify(slc_inputs)

@bp.route('/<slcid>/inputs/external', methods=['GET'], strict_slashes=False)
def get_inputs_external(slcid):
  """
  Flask blueprint route for getting External inputs of a single SLC project.

  Returns an error if this project ID does not have external inputs;
  Returns 404 error if project ID does not exist in the database.

  :param str slcid: the ID of the SLC project
  :return: the serialised response containing the selected SLC project or a HTTP 404 error
  :rtype: dict
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.inputs_external is None:
    return "SLC project does not have External inputs."

  slc_inputs_schema = InputsExternalSchema()
  slc_inputs = slc_inputs_schema.dump(slc_project.inputs_external)
  slc_inputs["mc_general"] = slc_project.inputs_external.mc_general
  slc_inputs["bom_et"] = slc_project.inputs_external.bom_et
  slc_inputs["et_power"] = slc_project.inputs_external.et_power
  slc_inputs["bom_ed"] = slc_project.inputs_external.bom_ed
  slc_inputs["bom_sk"] = slc_project.inputs_external.bom_sk
  slc_inputs["bom_lmo"] = slc_project.inputs_external.bom_lmo
  slc_inputs["aep"] = slc_project.inputs_external.aep
  slc_inputs["main_solution"] = slc_project.inputs_external.main_solution

  return jsonify(slc_inputs)


@bp.route('/<slcid>/inputs/general', methods=['POST'], strict_slashes=False)
def post_inputs_general(slcid):
  """
  Flask blueprint route for adding general inputs to a SLC project.

  Calls the :meth:`~dtop_slc.service.api.slc_inputs.create_inputs_general()` method.
  Returns HTTP 400 error if no request body provided;
  Returns 404 error if project ID does not exist.

  :return: the json response containing a successful 'General inputs created' message and the new inputs data, if successful
  :rtype: flask.wrappers.Response
  """
  # Look for a project with this slcid
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')

  inputs_gen = slc_project.inputs_general
  if inputs_gen:
    return bad_request('General inputs for this SLC project already present')

  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  try:
    LoadInputsGeneralSchema().load(request_body)
  except ValidationError as err:
    msg = join_validation_error_messages(err)
    return bad_request(msg)

  request_body['project_id'] = slcid
  slc_inputs = create_inputs_general(request_body)
  if type(slc_inputs) is str:
    return bad_request(slc_inputs)
  if not slc_inputs:
    return bad_request('Error')
  slc_inputs_schema = InputsGeneralSchema()

  # Update SLC project status
  update_status(slcid, +20)

  result = {
      "created_inputs": slc_inputs_schema.dump(slc_inputs),
      "message": "General inputs created"
  }
  response = jsonify(result)
  response.status_code = 201

  return response

@bp.route('/<slcid>/inputs/financial', methods=['POST'], strict_slashes=False)
def post_inputs_financial(slcid):
  """
  Flask blueprint route for adding inputs related to Financial to a SLC project.

  Calls the :meth:`~dtop_slc.service.api.slc_inputs.create_inputs_financial()` method.
  Returns HTTP 400 error if no request body provided;
  Returns 404 error if project ID does not exist.

  :return: the json response containing a successful 'Financial inputs created' message and the new inputs data, if successful
  :rtype: flask.wrappers.Response
  """
  # Look for a project with this slcid
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')

  inputs_fin = slc_project.inputs_financial
  if inputs_fin:
    return bad_request('Financial inputs for this SLC project already present')

  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  try:
    LoadInputsFinancialSchema().load(request_body)
  except ValidationError as err:
    msg = join_validation_error_messages(err)
    return bad_request(msg)

  request_body['project_id'] = slcid
  slc_inputs = create_inputs_financial(request_body)
  if type(slc_inputs) is str:
    return bad_request(slc_inputs)
  if not slc_inputs:
    return bad_request('Error')
  slc_inputs_schema = InputsFinancialSchema()

  # Update SLC project status
  update_status(slcid, +15)

  result = {
      "created_inputs": slc_inputs_schema.dump(slc_inputs),
      "message": "Financial inputs created"
  }
  response = jsonify(result)
  response.status_code = 201

  return response

@bp.route('/<slcid>/inputs/ace', methods=['POST'], strict_slashes=False)
def post_inputs_ace(slcid):
  """
  Flask blueprint route for adding inputs related to ACE to a SLC project.

  Calls the :meth:`~dtop_slc.service.api.slc_inputs.create_inputs_ace()` method.
  Returns HTTP 400 error if no request body provided;
  Returns 404 error if project ID does not exist.

  :return: the json response containing a successful 'ACE inputs created' message and the new inputs data, if successful
  :rtype: flask.wrappers.Response
  """
  # Look for a project with this slcid
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')

  inputs_ace = slc_project.inputs_ace
  if inputs_ace:
    return bad_request('ACE inputs for this SLC project already present')

  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  try:
    LoadInputsACESchema().load(request_body)
  except ValidationError as err:
    msg = join_validation_error_messages(err)
    return bad_request(msg)

  request_body['project_id'] = slcid
  slc_inputs = create_inputs_ace(request_body)
  if type(slc_inputs) is str:
    return bad_request(slc_inputs)
  if not slc_inputs:
    return bad_request('Error')
  slc_inputs_schema = InputsACESchema()

  # Update SLC project status
  if slc_project.complexity == 'low':
    update_status(slcid, +15)

  result = {
      "created_inputs": slc_inputs_schema.dump(slc_inputs),
      "message": "ACE inputs created"
  }
  response = jsonify(result)
  response.status_code = 201

  return response

@bp.route('/<slcid>/inputs/external', methods=['POST'], strict_slashes=False)
def post_inputs_external(slcid):
  """
  Flask blueprint route for adding external inputs to a SLC project.

  Calls the :meth:`~dtop_slc.service.api.slc_inputs.create_inputs_external()` method.
  Returns HTTP 400 error if those inputs were already added.
  Returns 404 error if project ID does not exist.

  :return: the json response containing a successful 'External inputs created' message and the new inputs data, if successful
  :rtype: flask.wrappers.Response
  """
  # Look for a project with this slcid
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')

  inputs_external = slc_project.inputs_external
  if inputs_external:
    return bad_request('External inputs for this SLC project already present')

  request_body = request.get_json()
  # if not request_body:
  #   return bad_request('No request body provided')
  # try:
  #   InputsExternalSchema().load(request_body)
  # except ValidationError as err:
  #   msg = join_validation_error_messages(err)
  #   return bad_request(msg)

  request_body['project_id'] = slcid
  slc_inputs = create_inputs_external(request_body)
  if type(slc_inputs) is str:
    # Error inside create_inputs_external()
    return bad_request(slc_inputs)
  slc_inputs_schema = InputsExternalSchema()

  # Update SLC project status
  update_status(slcid, +15)

  result = {
      "external_inputs": slc_inputs_schema.dump(slc_inputs),
      "message": "External inputs created"
  }
  result["external_inputs"]["bom_et"] = slc_project.inputs_external.bom_et
  result["external_inputs"]["bom_ed"] = slc_project.inputs_external.bom_ed
  result["external_inputs"]["bom_sk"] = slc_project.inputs_external.bom_sk
  result["external_inputs"]["bom_lmo"] = slc_project.inputs_external.bom_lmo
  result["external_inputs"]["aep"] = slc_project.inputs_external.aep
  result["external_inputs"]["main_solution"] = slc_project.inputs_external.main_solution

  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/<slcid>/inputs/general', methods=['PUT'], strict_slashes=False)
def put_inputs_general(slcid):
  """
  Flask blueprint route for updating general inputs of a SLC project.

  Calls the :meth:`~dtop_slc.service.api.projects.update_inputs_general()` method.
  Returns HTTP 400 error if no request body provided;
  Returns 404 error if general inputs ID does not exist.

  :return: the json response containing a successful 'General inputs updated' message and the new project data, if successful
  :rtype: flask.wrappers.Response
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')

  try:
    UpdateInputsGeneralSchema().load(request_body)
  except ValidationError as err:
    _e = [err_menssage[0] for key, err_menssage in err.messages.items()]
    return bad_request(_e[0])

  slc_inputs = update_inputs_general(request_body, slcid)
  if type(slc_inputs) is str:
    if 'does not exist' in slc_inputs:
      return not_found(slc_inputs)
    else:
      return bad_request(slc_inputs)
  slc_inputs_schema = InputsGeneralSchema()

  result = {
      'updated_inputs': slc_inputs_schema.dump(slc_inputs),
      'message': 'General Inputs updated.'
  }
  response = jsonify(result)
  response.status_code = 200

  return response

@bp.route('/<slcid>/inputs/financial', methods=['PUT'], strict_slashes=False)
def put_inputs_financial(slcid):
  """
  Flask blueprint route for updating financial inputs of a SLC project.

  Calls the :meth:`~dtop_slc.service.api.projects.update_inputs_financial()` method.
  Returns HTTP 400 error if no request body provided;
  Returns 404 error if financial inputs ID does not exist.

  :return: the json response containing a successful 'Financial inputs updated' message and the new project data, if successful
  :rtype: flask.wrappers.Response
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')

  try:
    UpdateInputsFinancialSchema().load(request_body)
  except ValidationError as err:
    _e = [err_menssage[0] for key, err_menssage in err.messages.items()]
    return bad_request(_e[0])

  slc_inputs = update_inputs_financial(request_body, slcid)
  if type(slc_inputs) is str:
    if 'does not exist' in slc_inputs:
      return not_found(slc_inputs)
    else:
      return bad_request(slc_inputs)
  slc_inputs_schema = InputsFinancialSchema()

  result = {
      'updated_inputs': slc_inputs_schema.dump(slc_inputs),
      'message': 'Financial Inputs updated.'
  }
  response = jsonify(result)
  response.status_code = 200

  return response

@bp.route('/<slcid>/inputs/ace', methods=['PUT'], strict_slashes=False)
def put_inputs_ace(slcid):
  """
  Flask blueprint route for updating ACE inputs of a SLC project.

  Calls the :meth:`~dtop_slc.service.api.projects.update_inputs_ace()` method.
  Returns HTTP 400 error if no request body provided;
  Returns 404 error if ACE inputs ID does not exist.

  :return: the json response containing a successful 'ACE inputs updated' message and the new project data, if successful
  :rtype: flask.wrappers.Response
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')

  try:
    UpdateInputsACESchema().load(request_body)
  except ValidationError as err:
    _e = [err_menssage[0] for key, err_menssage in err.messages.items()]
    return bad_request(_e[0])

  slc_inputs = update_inputs_ace(request_body, slcid)
  if type(slc_inputs) is str:
    if 'does not exist' in slc_inputs:
      return not_found(slc_inputs)
    else:
      return bad_request(slc_inputs)
  slc_inputs_schema = InputsACESchema()

  result = {
      'updated_inputs': slc_inputs_schema.dump(slc_inputs),
      'message': 'ACE Inputs updated.'
  }
  response = jsonify(result)
  response.status_code = 200

  return response

# @bp.route('/<slcid>/inputs/external', methods=['PUT'], strict_slashes=False)
# def put_inputs_external(slcid):


@bp.route('/<slcid>/inputs/general', methods=['DELETE'])
def del_inputs_general(slcid):
  """
  Flask blueprint route for deleting SLC general inputs.

  Calls the :meth:`~dtop_slc.service.api.projects.delete_inputs_general()` method.
  Returns HTTP 400 error if deletion of SLC inputs is unsuccessful
  Returns HTTP 404 error if SLC project id was not found.

  :param str slcid: the ID of the SLC project to deletet results
  :return: serialised response containing 'Results deleted' message; bad request error otherwise
  :rtype: flask.wrappers.Response
  """
  data = delete_inputs_general(slcid)
  if data == "Inputs deleted":
    response = {
        'project_id': slcid,
        'message': 'General inputs deleted.'
    }
    # Update SLC project status
    update_status(slcid, -20)

    return jsonify(response)
  else:
    return bad_request(data)

@bp.route('/<slcid>/inputs/financial', methods=['DELETE'])
def del_inputs_financial(slcid):
  """
  Flask blueprint route for deleting SLC financial inputs.

  Calls the :meth:`~dtop_slc.service.api.projects.delete_inputs_financial()` method.
  Returns HTTP 400 error if deletion of SLC inputs is unsuccessful
  Returns HTTP 404 error if SLC project id was not found.

  :param str slcid: the ID of the SLC project to deletet results
  :return: serialised response containing 'Results deleted' message; bad request error otherwise
  :rtype: flask.wrappers.Response
  """
  data = delete_inputs_financial(slcid)
  if data == "Inputs deleted":
    response = {
        'project_id': slcid,
        'message': 'Financial inputs deleted.'
    }
    # Update SLC project status
    update_status(slcid, -15)

    return jsonify(response)
  else:
    return bad_request(data)

@bp.route('/<slcid>/inputs/ace', methods=['DELETE'])
def del_inputs_ace(slcid):
  """
  Flask blueprint route for deleting SLC ACE inputs.

  Calls the :meth:`~dtop_slc.service.api.projects.delete_inputs_ace()` method.
  Returns HTTP 400 error if deletion of SLC inputs is unsuccessful
  Returns HTTP 404 error if SLC project id was not found.

  :param str slcid: the ID of the SLC project to deletet results
  :return: serialised response containing 'Results deleted' message; bad request error otherwise
  :rtype: flask.wrappers.Response
  """
  data = delete_inputs_ace(slcid)
  if data == "Inputs deleted":
    response = {
        'project_id': slcid,
        'message': 'ACE inputs deleted.'
    }
    # Update SLC project status
    slc_project = SLCProject.query.get(slcid)
    if slc_project.complexity == 'low':
      update_status(slcid, -15)

    return jsonify(response)
  else:
    return bad_request(data)

@bp.route('/<slcid>/inputs/external', methods=['DELETE'])
def del_inputs_external(slcid):
  """
  Flask blueprint route for deleting SLC External inputs.

  Calls the :meth:`~dtop_slc.service.api.projects.delete_inputs_external()` method.
  Returns HTTP 400 error if deletion of SLC inputs is unsuccessful
  Returns HTTP 404 error if SLC project id was not found.

  :param str slcid: the ID of the SLC project to deletet results
  :return: serialised response containing 'Results deleted' message; bad request error otherwise
  :rtype: flask.wrappers.Response
  """
  data = delete_inputs_external(slcid)
  if data == "Inputs deleted":
    response = {
        'project_id': slcid,
        'message': 'External inputs deleted.'
    }
    # Update SLC project status
    update_status(slcid, -15)

    return jsonify(response)
  else:
    return bad_request(data)
