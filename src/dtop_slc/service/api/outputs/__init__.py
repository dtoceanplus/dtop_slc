# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

from flask import Blueprint, request, jsonify
from marshmallow import ValidationError
import json
import numpy as np

from ..errors import bad_request, not_found
from ..utils import join_validation_error_messages

from dtop_slc.service.models import SLCProject

bp = Blueprint('api_outputs', __name__)
"""
  @oas [get] /api/projects/{slcid}
  description: Returns outputs of information stored in results
"""


@bp.route('/<slcid>/billofmaterials', methods=['GET'], strict_slashes=True)
def get_compiled_bom(slcid):
  """
  Flask blueprint route to get project compiled bill of materials from results.

  If slcid provided does not exist in the database, returns a 404 error.

  Args:
    slcid (str): the ID of the SLC project

  Returns:
    dict: the serialised response containing the selected SLC project CAPEX value or a HTTP 404 error
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.results is None:
    return not_found('SLC project with that ID does not have results yet.')

  result = slc_project.results.bom

  return jsonify(result)

@bp.route('/<slcid>/economical/capex', methods=['GET'], strict_slashes=True)
def get_economical_capex(slcid):
  """
  Flask blueprint route to get project CAPEX value from results.

  If slcid provided does not exist in the database, returns a 404 error.

  Args:
    slcid (str): the ID of the SLC project

  Returns:
    dict: the serialised response containing the selected SLC project CAPEX value or a HTTP 404 error
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.results is None:
    return not_found('SLC project with that ID does not have results yet.')

  result = {}
  result["capex"] = slc_project.results.capex_total

  return jsonify(result)

@bp.route('/<slcid>/economical/opex', methods=['GET'], strict_slashes=True)
def get_economical_opex(slcid):
  """
  Flask blueprint route to get project OPEX value from results.

  If slcid provided does not exist in the database, returns a 404 error.

  Args:
    slcid (str): the ID of the SLC project

  Returns:
    dict: the serialised response containing the selected SLC project OPEX values or a HTTP 404 error
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.results is None:
    return not_found('SLC project with that ID does not have results yet.')

  result = {}
  result["opex"] = {}
  result["opex"]["total"] = slc_project.results.opex_total
  result["opex"]["per_year"] = slc_project.results.opex_year

  return jsonify(result)

@bp.route('/<slcid>/economical/lcoe', methods=['GET'], strict_slashes=True)
def get_economical_lcoe(slcid):
  """
  Flask blueprint route to get project LCOE value from results.

  If slcid provided does not exist in the database, returns a 404 error.

  Args:
    slcid (str): the ID of the SLC project

  Returns:
    dict: the serialised response containing the selected SLC project LCOE value or a HTTP 404 error
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.results is None:
    return not_found('SLC project with that ID does not have results yet.')

  result = {}
  if slc_project.results.lcoe is None:
    result["lcoe"] = np.nan
  else:
    result["lcoe"] = slc_project.results.lcoe

  return jsonify(result)

@bp.route('/<slcid>/economical/ace', methods=['GET'], strict_slashes=True)
def get_economical_ace(slcid):
  """
  Flask blueprint route to get project ACE value from results.

  If slcid provided does not exist in the database, returns a 404 error.

  Args:
    slcid (str): the ID of the SLC project

  Returns:
    dict: the serialised response containing the selected SLC project ACE value or a HTTP 404 error
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.results is None:
    return not_found('SLC project with that ID does not have results yet.')

  result = {}
  if slc_project.results.ace is None:
    result["ace"] = np.nan
  else:
    result["ace"] = slc_project.results.ace

  return jsonify(result)

@bp.route('/<slcid>/financial/pbp', methods=['GET'], strict_slashes=True)
def get_financial_payback_period(slcid):
  """
  Flask blueprint route to get project Payback Time value from results.

  If slcid provided does not exist in the database, returns a 404 error.

  Args:
    slcid (str): the ID of the SLC project

  Returns:
    dict: the serialised response containing the selected SLC project Payback Time value or a HTTP 404 error
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.results is None:
    return not_found('SLC project with that ID does not have results yet.')

  result = {}
  if slc_project.results.pay_back_period is None:
    result["pbp"] = np.nan
  else:
    result["pbp"] = slc_project.results.pay_back_period

  return jsonify(result)

@bp.route('/<slcid>/financial/irr', methods=['GET'], strict_slashes=True)
def get_financial_internal_rate_return(slcid):
  """
  Flask blueprint route to get project Internal Rate Return value from results.

  If slcid provided does not exist in the database, returns a 404 error.

  Args:
    slcid (str): the ID of the SLC project

  Returns:
    dict: the serialised response containing the selected SLC project Internal Rate Return value or a HTTP 404 error
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.results is None:
    return not_found('SLC project with that ID does not have results yet.')

  result = {}
  if slc_project.results.internal_rate_return is None:
    result["irr"] = np.nan
  else:
    result["irr"] = slc_project.results.internal_rate_return

  return jsonify(result)

@bp.route('/<slcid>/financial/npv', methods=['GET'], strict_slashes=True)
def get_financial_net_present_value(slcid):
  """
  Flask blueprint route to get project Net Present Value value from results.

  If slcid provided does not exist in the database, returns a 404 error.

  Args:
    slcid (str): the ID of the SLC project

  Returns:
    dict: the serialised response containing the selected SLC project Net Present Value value or a HTTP 404 error
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.results is None:
    return not_found('SLC project with that ID does not have results yet.')

  result = {}
  if slc_project.results.net_present_value is None:
    result["npv"] = np.nan
  else:
    result["npv"] = slc_project.results.net_present_value

  return jsonify(result)

@bp.route('/<slcid>/benchmark/metrics', methods=['GET'], strict_slashes=True)
def get_benchmark_metrics_kw(slcid):
  """
  Flask blueprint route to get project Metrics per kW value from results.

  If slcid provided does not exist in the database, returns a 404 error.

  Args:
    slcid (str): the ID of the SLC project

  Returns:
    dict: the serialised response containing the selected SLC project Metrics per kW or a HTTP 404 error
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.results is None:
    return not_found('SLC project with that ID does not have results yet.')

  result = {}
  result["capex_kw"] = slc_project.results.capex_kw
  result["opex_kw"] = slc_project.results.opex_kw

  return jsonify(result)

@bp.route('/<slcid>/benchmark/breakdown', methods=['GET'], strict_slashes=True)
def get_benchmark_lcoe_breakdown(slcid):
  """
  Flask blueprint route to get project costs breakdown over LCOE from results.

  If slcid provided does not exist in the database, returns a 404 error.

  Args:
    slcid (str): the ID of the SLC project

  Returns:
    dict: the serialised response containing the selected SLC project costs breakdown over LCOE or a HTTP 404 error
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return not_found('SLC project with that ID could not be found')
  if slc_project.results is None:
    return not_found('SLC project with that ID does not have results yet.')

  result = {}
  result["other"] = slc_project.results.bench_other_lcoe
  result["grid"] = slc_project.results.bench_grid_lcoe
  result["device"] = slc_project.results.bench_device_lcoe
  result["moors_founds"] = slc_project.results.bench_mooring_lcoe
  result["installation"] = slc_project.results.bench_inst_lcoe
  result["opex"] = slc_project.results.bench_opex_lcoe

  return jsonify(result)
