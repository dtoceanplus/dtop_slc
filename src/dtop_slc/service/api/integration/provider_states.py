# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from contextlib import suppress

import sqlalchemy.exc
from flask import Blueprint, request
import requests
import json

from dtop_slc.service.db_utils import init_db
from dtop_slc.service import db
from dtop_slc.service.models import (
    SLCProject, InputsGeneral, InputsFinancial, InputsACE, InputsExternal
)

from .data_dummy import (
    bom_et, bom_ed, bom_sk, bom_lmo,
    maintenance_solution, aep
)

bp = Blueprint("provider_states", __name__)


@bp.route('/provider_states/setup', methods=['POST'])
def provider_states_setup():
  """
  Reset database and create data neccessary for contract testing.
  Should be available only with `FLASK_ENV=development`.
  """
  init_db()

  consumer = request.json['consumer']
  state = request.json['state']

  if state.startswith('slc 1 exists'):
    with suppress(sqlalchemy.exc.IntegrityError):
      with db.session.begin_nested():
        project = SLCProject(
            id=1,
            title='test',
            complexity='high',
            status=0
        )
        db.session.add(project)

    if state == 'slc 1 exists and has results':
      with suppress(sqlalchemy.exc.IntegrityError):
        with db.session.begin_nested():
          inputs_general = InputsGeneral(
              id=1,
              project_id=1,
              device_type='TEC',
              device_topology='fixed',
              project_life=20,
              discount_rate=7.25,
              device_power=1115.0,
              device_costs=716059,
              device_number=1,
              other_costs=23704237
          )
          db.session.add(inputs_general)

          inputs_financial = InputsFinancial(
              id=1,
              project_id=1,
              energy_price=0.06,
              fit_price=0.2,
              fit_years=20
          )
          db.session.add(inputs_financial)

          inputs_external = InputsExternal(
              id=1,
              project_id=1,
              bom_et=bom_et,
              bom_ed=bom_ed,
              bom_sk=bom_sk,
              bom_lmo=bom_lmo,
              main_solution=maintenance_solution,
              aep=aep
          )
          db.session.add(inputs_external)

      r_inputs_general = requests.get(f"{request.url_root}api/projects/1/inputs/general")
      r_inputs_financial = requests.get(f"{request.url_root}api/projects/1/inputs/financial")
      r_inputs_external = requests.get(f"{request.url_root}api/projects/1/inputs/external")
      # print(r_inputs_general.json())
      # print(r_inputs_financial.json())
      # print(r_inputs_external.json())

      r_results = requests.post(f"{request.url_root}api/projects/1/results")
      # print(r_results.json())

      r_capex = requests.get(f"{request.url_root}api/projects/1/economical/capex")
      r_opex = requests.get(f"{request.url_root}api/projects/1/economical/opex")
      r_lcoe = requests.get(f"{request.url_root}api/projects/1/economical/lcoe")
      r_irr = requests.get(f"{request.url_root}api/projects/1/financial/irr")
      r_metrics = requests.get(f"{request.url_root}api/projects/1/benchmark/metrics")
      # print(r_capex.json())
      # print(r_opex.json())
      # print(r_lcoe.json())
      # print(r_irr.json())
      # print(r_metrics.json())

  return ''
