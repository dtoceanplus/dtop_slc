# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, request, jsonify
import requests
import os

from dtop_slc.service.api import results

from ..errors import bad_request, not_found

from dtop_slc.service.models import SLCProject

bp = Blueprint('api_integration', __name__)


def get_url_and_headers(module):
  """
  Common function provided by OCC to get DTOP basic module URL and headers

  :param str module: the DTOP module nickname (e.g. 'mm')
  :return: the module API url and the headers instance
  :rtype: tuple
  """
  protocol = os.getenv("DTOP_PROTOCOL", "http")
  domain = os.environ["DTOP_DOMAIN"]
  auth = os.getenv("DTOP_AUTH", "")

  print()
  print("Authorization header value :")
  header_auth = f"{auth}"
  print(header_auth)

  print()
  print("Headers - auth :")
  headers = {"Authorization": header_auth}
  print(headers)

  print()
  print("Host header value :")
  header_host = f'{module + "." + domain}'
  print(header_host)

  server_url = f'{protocol + "://" + header_host}'

  print()
  print("Check if DTOP is deployed on DNS resolved server or on workstation :")

  try:
    response = requests.get(server_url, headers=headers)
    print("DTOP appears to be deployed on DNS resolved server")
    module_api_url = f"{server_url}"
  except requests.ConnectionError as exception:
    print("DTOP appears to be deployed on workstation")
    docker_ws_ip = "http://172.17.0.1:80"
    module_api_url = f"{docker_ws_ip}"
    print()
    print("Headers - auth and localhost :")
    headers["Host"] = header_host
    print(headers)

  print()
  print("Basic Module Open API URL :")
  print(module_api_url)
  print()

  return module_api_url, headers


def call_intermodule_service(module, uri):
  """
  Generic function for communicating directly with the BE of other modules.

  Uses the *get_url_and_headers* function.
  Leads to a 400 error if the module server is not available.
  If the request is unsuccessful, a HTTP error is returned.

  :param str module: the nickname for the module that is being communicated with
  :param str uri: the URI specifying the appropriate route to call for the metric
  :return: the request response if successful, tuple of error and status code otherwise
  :rtype: requests.Response or tuple
  """
  # TODO: docstrings
  # TODO: update print statements
  print()
  print("Test example - request from Catalog BE to Main Module BE to get users' list")

  print()
  api_url, headers = get_url_and_headers(module)
  print("MM Open API URL to Get users :")
  users_url = f"{api_url + uri}"
  print(users_url)

  print()
  print("CM BE to MM BE request and response :")
  print()

  try:
    resp = requests.get(users_url, headers=headers)
    resp.raise_for_status()
    print()
    return resp
  except requests.exceptions.ConnectionError as errc:
    print("Error Connecting:", errc)
    print("The module server is not available")
    return errc, 400
  except requests.exceptions.HTTPError as errh:
    print("HTTP Error:", errh)
    return errh, errh.response.status_code


@bp.route('/integration/bom', methods=['POST'])
def post_slc_integration_bom(slcid):
  """
  Flask blueprint route to add a BOM to a SLC project.

  Returns HTTP 400 error if no request body provided.
  Returns 404 error if project ID does not exist.

  Args:
  slcid (int): SLC project ID

  Returns:
  dict: the serialised response containing this module feched response.
  """
  try:
    # Look for a study with this slcid
    slc_project = SLCProject.query.get(slcid)
    if slc_project is None:
      return not_found('SLC project with that ID could not be found')

    request_url = request.get_json()
    if not request_url:
      return bad_request('No request body provided')

    result = call_intermodule_service(
          request_url["module"], request_url["url"]).json()

    result = call_intermodule_service(
        module=request_url["module"],
        uri=request_url["url"]
    ).json()

    response = jsonify(result)
    response.status_code = 201
    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/integration/<module>', methods=['POST'])
def post_slc_integration_module(slcid, module):
  """
  Flask blueprint route to add data from other modules to a SLC project.

  Returns HTTP 400 error if no request body provided.
  Returns 404 error if project ID does not exist.

  Args:
  slcid (int): SLC project ID

  Returns:
  dict: the serialised response containing this module feched response.
  """
  try:
    # Look for a study with this slcid
    slc_project = SLCProject.query.get(slcid)
    if slc_project is None:
      return not_found('SLC project with that ID could not be found')

    request_url = request.get_json()
    if not request_url:
      return bad_request('No request body provided')

    result = call_intermodule_service(
        module=module.lower(),
        uri=request_url["url"]
    ).json()

    response = jsonify(result)
    response.status_code = 201
    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))
