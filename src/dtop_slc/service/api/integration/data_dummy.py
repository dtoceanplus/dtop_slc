# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

bom_et = {
    "id": ['CAT_turbine', 'CAT_gen', 'CAT_b2b', 'Tot_ET'],
    "name": [
        'Air turbine',
        'Generator_x',
        'Back to back converter',
        'Total ET system'
    ],
    "qnt": ['20', '20', '20', '-'],
    "total_cost": [0, 0, 0, 1908099],
    "unit_cost": ['100000', '70000', '20000', '-'],
    "uom": ['-', '-', '-', '-']
}

bom_ed = {
    "id": [
        'CAT_Cable001',
        'CAT_Cable062',
        'CAT_colpoint',
        'CAT_con001',
       'Tot_onshoreinf',
       'Tot_transm',
       'Tot_network',
       'Tot_colpoint'
    ],
    "name": [
        'Cable xyz',
        'Cable xyz239',
        'Subsea hub',
        'Connector wet-mate',
        'Total onshore infrastructure',
        'Total Transmission network',
        'Total Array network',
        'Total Collection point'
    ],
    "qnt": ['3000', '9000', '2', '3', '-', '-', '-', '-'],
    "uom": ['m', 'm', '-', '-', '-', '-', '-', '-'],
    "unit_cost": ['2300', '1100', '1000000', '1000000', '-', '-', '-', '-'],
    "total_cost": [0, 0, 0, 0, 0, 43200, 0, 0]}

bom_sk = {
    "id": ['CAT_Anchor001', 'CAT_ML001', 'Tot_SK'],
    "name": ['Anchor ', 'Mooring line', 'Total costs of SK system'],
    "qnt": ['40', '1500', '-'],
    "uom": ['-', 'm', '-'],
    "unit_cost": ['70000', '3000', '-'],
    "total_cost": [0, 0, 558491]
}

bom_lmo = {
    "id": ["Tot_Inst_Dev", "Tot_Inst_Anc", "Tot_Inst_Cable"],
    "name": ["Total cost of installation of devices", "Total cost of installation of Anchors", "Total cost of installation of cables"],
    "qnt": ["-", "-", "-"],
    "uom": ["-", "-", "-"],
    "unit_cost": ["-", "-", "-"],
    "total_cost": [872215, 6128696, 1628674]
}

maintenance_solution = {
    "operation_id": [
        "OP13_0",
        "OP13_0",
        "OP13_0",
        "OP13_0",
        "OP13_0",
        "OP13_0",
        "OP13_0",
        "OP12_0",
        "OP13_1",
        "OP12_1",
        "OP13_2",
        "OP12_2",
        "OP13_3",
        "OP12_3",
        "OP12_4",
        "OP13_4",
        "OP12_5",
        "OP12_6",
        "OP13_5",
        "OP12_7"
    ],
    "proj_year": [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20
    ],
    "cost": [
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527,
        1599527
    ],
    "cost_label": [
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX",
        "OPEX"
    ]
}

aep = {
    "1": 2727000,
    "2": 2727000,
    "3": 2727000,
    "4": 2727000,
    "5": 2727000,
    "6": 2727000,
    "7": 2727000,
    "8": 2727000,
    "9": 2727000,
    "10": 2727000,
    "11": 2727000,
    "12": 2727000,
    "13": 2727000,
    "14": 2727000,
    "15": 2727000,
    "16": 2727000,
    "17": 2727000,
    "18": 2727000,
    "19": 2727000,
    "20": 2727000
}
