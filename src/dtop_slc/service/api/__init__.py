# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import requests
from flask import Blueprint, request, jsonify

from .errors import bad_request

bp = Blueprint('api_pact', __name__)


@bp.route('/mc-general', methods=['POST'])
def mc_general():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *general* details from MC module.

  To be used in PACT testing example.

  Example request body for MC:

  {
    "mc_general": "http://localhost:{port}/mc/{mcId}/general"
  }

  :return: the general characteristics of machine
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["mc_general"]
  try:
    floating = requests.get(url).json()["floating"]
    machine_cost = requests.get(url).json()["machine_cost"]
    print(floating)
    print(machine_cost)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to MC general failed')

  result = {
      "floating": floating,
      "machine_cost": machine_cost
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/ec-farm', methods=['POST'])
def ec_farm():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

  To be used in PACT testing example.

  Example request body for EC:

  {
    "ec_farm": "http://localhost:{port}/ec/{ecId}/farm"
  }

  :return: the farm layout
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["ec_farm"]
  try:
    number_devices = requests.get(url).json()["number_devices"]
    print(number_devices)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to EC farm failed')

  result = {
      "number_devices": number_devices
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/et-array', methods=['POST'])
def et_array():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *array* from ET module.

  To be used in PACT testing example.

  Example request body for ET:

  {
    "et_array": "http://localhost:{port}/energy_transf/{etId}/array"
  }

  :return: ET array
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["et_array"]
  try:
    BoM = requests.get(url).json()["BoM"]
    value = BoM["value"]
    id_ = value["id"]
    name = value["name"]
    qnt = value["qnt"]
    uom = value["uom"]
    unit_cost = value["unit_cost"]
    total_cost = value["total_cost"]
    print(id_)
    print(name)
    print(qnt)
    print(uom)
    print(unit_cost)
    print(total_cost)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to ET array failed')

  result = {
      "id": id_,
      "name": name,
      "qnt": qnt,
      "uom": uom,
      "unit_cost": unit_cost,
      "total_cost": total_cost
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/et-devices', methods=['POST'])
def et_devices():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *devices* from ET module.

  To be used in PACT testing example.

  Example request body for ET:

  {
    "et_devices": "http://localhost:{port}/energy_transf/{etId}/devices"
  }

  :return: ET devices
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["et_devices"]
  try:
    rated_power = requests.get(url).json()[0]["Dev_rated_power"]["value"]
    print(rated_power)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to ET devices failed')

  result = {
      "rated_power": rated_power
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/ed-results', methods=['POST'])
def ed_results():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *results* from ED module.

  To be used in PACT testing example.

  Example request body for ED:

  {
    "ed_results": "http://localhost:{port}/api/energy-deliv-studies/{edId}/results"
  }

  :return: ED results
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["ed_results"]
  try:
    bom = requests.get(url).json()["b_o_m_new"]
    bom_name = bom["name"]
    bom_qnt = bom["qnt"]
    bom_uom = bom["uom"]
    bom_unit_cost = bom["unit_cost"]
    bom_total_cost = bom["total_cost"]
    print(bom)
    print(bom_name)
    print(bom_qnt)
    print(bom_uom)
    print(bom_unit_cost)
    print(bom_total_cost)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to ED results failed')

  result = {
      "bom_name": bom_name,
      "bom_qnt": bom_qnt,
      "bom_uom": bom_uom,
      "bom_unit_cost": bom_unit_cost,
      "bom_total_cost": bom_total_cost
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/sk-bom', methods=['POST'])
def sk_bom():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *bom* from SK module.

  To be used in PACT testing example.

  Example request body for SK:

  {
    "sk_bom": "http://localhost:{port}/api/sk/{ProjectId}/bom"
  }

  :return: SK results
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["sk_bom"]
  try:
    bom = requests.get(url).json()
    bom_id = bom["catalogue_ID"]
    bom_name = bom["product_name"]
    bom_qnt = bom["quantity"]
    bom_uom = bom["unit"]
    bom_unit_cost = bom["unit_cost"]
    bom_total_cost = bom["total_cost"]
    print(bom_id)
    print(bom_name)
    print(bom_qnt)
    print(bom_uom)
    print(bom_unit_cost)
    print(bom_total_cost)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to SK results failed')

  result = {
      "bom_id": bom_id,
      "bom_name": bom_name,
      "bom_qnt": bom_qnt,
      "bom_uom": bom_uom,
      "bom_unit_cost": bom_unit_cost,
      "bom_total_cost": bom_total_cost
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/lmo-bom', methods=['POST'])
def lmo_bom():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *bom* from LMO module.

  To be used in PACT testing example.

  Example request body for LMO:

  {
    "lmo_bom": "http://localhost:{port}/api/{lmoid}/bom"
  }

  :return: LMO BoM
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["lmo_bom"]
  try:
    bom = requests.get(url).json()
    bom_id = bom["id"]
    bom_name = bom["name"]
    bom_qnt = bom["qnt"]
    bom_uom = bom["uom"]
    bom_unit_cost = bom["unit_cost"]
    bom_total_cost = bom["total_cost"]
    print(bom_id)
    print(bom_name)
    print(bom_qnt)
    print(bom_uom)
    print(bom_unit_cost)
    print(bom_total_cost)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to LMO BoM failed')

  result = {
      "bom_id": bom_id,
      "bom_name": bom_name,
      "bom_qnt": bom_qnt,
      "bom_uom": bom_uom,
      "bom_unit_cost": bom_unit_cost,
      "bom_total_cost": bom_total_cost
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/lmo-maintenance-solution', methods=['POST'])
def lmo_maintenance_solution():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *maintenance solution* from LMO module.

  To be used in PACT testing example.

  Example request body for LMO:

  {
    "lmo_maintenance_solution": "http://localhost:{port}/api/{lmoid}/phases/maintenance/plan"
  }

  :return: LMO Maintenance Solution
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["lmo_maintenance_solution"]
  try:
    maint_sol = requests.get(url).json()
    maint_sol_id = maint_sol["operation_id"]
    maint_sol_year = maint_sol["proj_year"]
    maint_sol_cost = maint_sol["cost"]
    maint_sol_label = maint_sol["cost_label"]
    print(maint_sol_id)
    print(maint_sol_year)
    print(maint_sol_cost)
    print(maint_sol_label)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to LMO Maintenance Solution failed')

  result = {
      "maint_sol_id": maint_sol_id,
      "maint_sol_year": maint_sol_year,
      "maint_sol_cost": maint_sol_cost,
      "maint_sol_label": maint_sol_label
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/spey-aep', methods=['POST'])
def spey_aep():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *aep* from SPEY module.

  To be used in PACT testing example.

  Example request body for SPEY:

  {
    "spey_aep": "http://localhost:{port}/spey/{speyId}/energy-production"
  }

  :return: AEP
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["spey_aep"]
  try:
    aep = requests.get(url).json()["Outputs_EP"]["array_annual_net_energy_pd"]["value"]
    print(aep)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to SPEY AEP failed')

  result = {
      "aep": aep
  }
  response = jsonify(result)
  response.status_code = 201

  return response
