# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from flask import Blueprint, jsonify

from ..errors import bad_request, not_found

from dtop_slc.service.models import SLCProject

headers = {"Access-Control-Allow-Headers": "Content-Type"}

bp = Blueprint("representation", __name__)


@bp.route("/representation/<slcid>", methods=["GET"])
def get_representation(slcid):
  """
  Get the DR of a Project
  """
  try:
    slc_project = SLCProject.query.get(slcid)
    if slc_project is None:
      return not_found('SLC project with that ID could not be found')
    if slc_project.results is None:
      return bad_request('SLC project does not have results yet.')

    result = {
        "bom_compiled": slc_project.results.bom,
        "economical_results": {
            "capex": slc_project.results.capex_total,
            "opex": {
                "total": slc_project.results.opex_total,
                "per_year": slc_project.results.opex_year
            },
            "lcoe": slc_project.results.lcoe,
            "ace": slc_project.results.ace
        },
        "financial_results": {
            "pbp": slc_project.results.pay_back_period,
            "irr": slc_project.results.internal_rate_return,
            "npv": slc_project.results.net_present_value
        },
        "benchmark": {
            "capex_kw": slc_project.results.pay_back_period,
            "opex_kw": slc_project.results.internal_rate_return,
            "lcoe_breakdown": {
                "other": slc_project.results.bench_other_lcoe,
                "grid": slc_project.results.bench_grid_lcoe,
                "device": slc_project.results.bench_device_lcoe,
                "moors_founds": slc_project.results.bench_mooring_lcoe,
                "installation": slc_project.results.bench_inst_lcoe,
                "opex": slc_project.results.bench_opex_lcoe
            }
        }
    }
    return jsonify(result)

  except:
    return bad_request('Unknown error')
