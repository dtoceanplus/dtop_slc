# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

from flask import Blueprint, request, jsonify
from marshmallow import ValidationError

from ..errors import bad_request, not_found
from ..utils import join_validation_error_messages

from .slc_projects import create_project, update_project, delete_project
from dtop_slc.service.models import SLCProject
from dtop_slc.service.schemas import (
    SLCProjectSchema, LoadSLCProjectSchema, UpdateSLCProjectSchema,
    InputsGeneralSchema,
    InputsFinancialSchema,
    InputsACESchema,
    InputsExternalSchema,
    ResultsSchema
)

bp = Blueprint('api_projects', __name__)
"""
  @oas [get] /api/projects
  description: Returns result of business logic functions
"""


@bp.route('', methods=['GET'], strict_slashes=False)
def get_slc_projects_list():
  """
  Flask blueprint route to get all SLC projects.
  """
  slc_projects = SLCProject.query.all()
  slc_projects_schema = SLCProjectSchema(many=True)
  result = slc_projects_schema.dump(slc_projects)

  return jsonify(result)


@bp.route('/<slcid>', methods=['GET'])
def get_slc_project_id(slcid):
  """
  Flask blueprint route for getting a single SLC project.

  If slcid provided does not exist in the database, returns a 404 error.

  :param str slcid: the ID of the SLC project
  :return: the serialised response containing the selected SLC project or a HTTP 404 error
  :rtype: dict
  """
  project = SLCProject.query.get(slcid)
  if project is None:
    return not_found('SLC project with that ID could not be found')
  project_schema = SLCProjectSchema()
  result = project_schema.dump(project)

  return jsonify(result)


@bp.route('', methods=['POST'], strict_slashes=False)
def create_slc_project():
  """
  Flask blueprint route for deleting a SLC project.
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  try:
    LoadSLCProjectSchema().load(request_body)
  except ValidationError as err:
    msg = join_validation_error_messages(err)
    return bad_request(msg)
  slc_project = create_project(request_body)
  if type(slc_project) is str:
    return bad_request(slc_project)
  slc_project_schema = SLCProjectSchema()
  result = {
      "created_project": slc_project_schema.dump(slc_project),
      "message": "Project created"
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/<slcid>', methods=['PUT'])
def update_slc_project(slcid):
  """
  Flask blueprint route for updating a SLC project.

  Calls the :meth:`~dtop_slc.service.api.slc_projects.update_study()` method.
  Returns HTTP 400 error if no request body provided;
  Returns 404 error if study ID does not exist.

  :return: the json response containing a successful 'project updated' message and the new project data, if successful
  :rtype: flask.wrappers.Response
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request("No request body provided")

  try:
    UpdateSLCProjectSchema().load(request_body)
  except ValidationError as err:
    _e = [err_menssage[0] for key, err_menssage in err.messages.items()]
    return bad_request(_e[0])
  slc_project = update_project(request_body, slcid)
  if type(slc_project) is str:
    if "does not exist" in slc_project:
      return not_found(slc_project)
    else:
      return bad_request(slc_project)
  slc_project_schema = SLCProjectSchema()

  response = {
      'updated_project': slc_project_schema.dump(slc_project),
      'message': 'Project updated.'
  }

  return jsonify(response)


@bp.route('/<slcid>', methods=['DELETE'])
def delete_slc_project(slcid):
  """
  Flask blueprint route for deleting a SLC project.

  Calls the :meth:`~dtop_slc.service.api.projects.delete_project()` method.
  Returns HTTP 400 error if deletion of SLC project is unsuccessful or
  if SLC project id was not found.

  :param str slcid: the ID of the SLC project to be deleted
  :return: serialised response containing 'Project deleted' message; bad request error otherwise
  :rtype: flask.wrappers.Response
  """
  data = delete_project(slcid)
  if data == "Project deleted":
    response = {
        'project_id': slcid,
        'message': 'Project deleted.'
    }
    return jsonify(response)
  else:
    return bad_request(data)


@bp.route('/<slcid>/export', methods=['GET'])
def get_slc_project_id_export(slcid):
  """
  Flask blueprint route for exporting a single SLC project.

  If slcid provided does not exist in the database, returns a 404 error.

  :param str slcid: the ID of the SLC project
  :return: the serialised response containing the exported SLC project or a HTTP 404 error
  :rtype: dict
  """
  project = SLCProject.query.get(slcid)
  if project is None:
    return not_found('SLC project with that ID could not be found')
  if project.results is None:
    return bad_request('There is no results for this SLC project yet')
  project_schema = SLCProjectSchema()
  inputs_general_schema = InputsGeneralSchema()
  inputs_financial_schema = InputsFinancialSchema()
  inputs_ace_schema = InputsACESchema()
  inputs_external_schema = InputsExternalSchema()
  results_schema = ResultsSchema()

  dict_project = project_schema.dump(project)
  dict_inputs_general = inputs_general_schema.dump(project.inputs_general)
  dict_inputs_financial = inputs_financial_schema.dump(project.inputs_financial)
  dict_inputs_ace = inputs_ace_schema.dump(project.inputs_ace)
  dict_inputs_external = inputs_external_schema.dump(project.inputs_external)
  dict_results = results_schema.dump(project.results)

  dict_project["inputs_general"] = dict_inputs_general
  dict_project["inputs_financial"] = dict_inputs_financial
  dict_project["inputs_ace"] = dict_inputs_ace
  dict_project["inputs_external"] = dict_inputs_external
  dict_project["results"] = dict_results

  return jsonify(dict_project)

