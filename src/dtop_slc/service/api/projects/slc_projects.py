# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from sqlalchemy.exc import IntegrityError

from ....service import db

from ...models import SLCProject
from ...schemas import SLCProjectSchema


def create_project(data):
  """
  Create a new SLC project and add it to the local storage database.

  Returns an error message and rolls back the session if the new SLC project has the same name as an existing project in the database.

  :param dict data: dictionary required to create the SLC project object.
  :return: the newly created SLC project instance
  :rtype: dtop_slc.service.models.SLCProject
  """
  slc_project_schema = SLCProjectSchema()
  slc_project = slc_project_schema.load(data)
  slc_project.status = 0
  try:
    db.session.add(slc_project)
    db.session.commit()
    return slc_project
  except IntegrityError:
    db.session.rollback()
    return "SLC project with that name already exists."


def update_project(data, slcid):
  """
  Update a SLC project entry in the local storage database.

  Returns an error message if:
  - the SLC project ID that is trying to be updated does not exist or

  :param dict data: the SLC project data required to update a project in the local database.
  :param str slcid: the ID of the SLC project in the local storage database
  :return: the updated SLC project instance
  :rtype: dtop_slc.service.models.SLCProject
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return "SLC project with that ID does not exist."

  # Update existing project (ID=slcid) with existing data from "data"
  title = data.get('title', None)
  desc = data.get('description', None)
  complexity = data.get('complexity', None)
  if title is not None:
    slc_project.title = title
  if desc is not None:
    slc_project.description = desc
  if complexity is not None:
    slc_project.complexity = complexity

  # Update database
  try:
    db.session.commit()
    return slc_project
  except IntegrityError:
    db.session.rollback()
    return "Unknown error."


def delete_project(slcid):
  """
  Delete a SLC project from the local storage database.

  If project with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  :param str slcid: the ID of the SLC project in local storage database to be deleted
  :return: error message if ID does not exist, dictionary with deleted status set to True if deleted successfully
  :rtype: dict
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return "SLC project with that ID does not exist."
  else:
    db.session.delete(slc_project)
    db.session.commit()
    return "Project deleted"


def update_status(slcid, percentage):
  """
  Update the status of an SLC project..

  Returns an error message if:
  - the SLC project ID that is trying to be updated does not exist

  :param str slcid: the ID of the SLC project in the local storage database
  """
  slc_project = SLCProject.query.get(slcid)
  if slc_project is None:
    return "SLC project with that ID does not exist."

  status = slc_project.status
  status += percentage
  if status > 100:
    status = 100
  elif status < 0:
    status = 0
  slc_project.status = status

  # Update database
  try:
    db.session.commit()
  except IntegrityError:
    db.session.rollback()
    return "Unknown error."
