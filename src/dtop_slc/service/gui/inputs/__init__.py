# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses service logic API.
# It provides GUI via HTTP API.
# It create a blueprint and define routes.

# Templates: http://flask.pocoo.org/docs/1.0/blueprints/#templates

from flask import Blueprint, render_template, request
from flask import jsonify
import json

import pandas as pd

bp = Blueprint('gui_inputs', __name__)

@bp.route('/', strict_slashes=False)#, methods=["GET", "POST"])
def index():
  # Loads a page to introduce inputs for the SLC
  return render_template("inputs.html")

@bp.route('/complexity', strict_slashes=False)#, methods=["GET", "POST"])
def complexity():
  # Loads a page to introduce project complexity for the SLC
  return render_template("gui/inputs_complexity.html")

@bp.route('/input_cpx1', strict_slashes=False)#, methods=["GET", "POST"])
def inputs_cpx1():
  # Loads a page to introduce inputs for complexity 1
  return render_template("gui/inputs_cpx1.html")

@bp.route('/input_cpx2', strict_slashes=False)#, methods=["GET", "POST"])
def inputs_cpx2():
  # Loads a page to introduce inputs for complexity 2
  return render_template("gui/inputs_cpx2.html")

@bp.route('/input_cpx3', strict_slashes=False)#, methods=["GET", "POST"])
def inputs_cpx3():
  # Loads a page to introduce inputs for complexity 3
  return render_template("gui/inputs_cpx3.html")

@bp.route("/output", strict_slashes=False)
def output():

  complexity = request.args.get('cpx')
  json_economical = request.args.get('economical')
  json_financial = request.args.get('financial')
  json_benchmark = request.args.get('benchmark')

  dict_economical = json.loads(json_economical)
  dict_financial = json.loads(json_financial)
  dict_benchmark = json.loads(json_benchmark)

  dict_economical_labels = {
    "lcoe": "LCOE",
    "ace": "ACE",
    "capex_total": "CAPEX",
    "cost_equip": "Cost of equipment",
    "cost_inst": "Cost of installation",
    "opex_total": "OPEX",
    "opex_year": "AVR OPEX"
  }
  dict_financial_labels = {
    "pbp": "Payback Period",
    "dpbp": "Discounted Payback Period",
    "irr": "Internal Rate of Return",
    "npv": "Net Present Value"
  }
  dict_benchmark_labels = {
    "capex_kw": "CAPEX per kW",
    "opex_kw": "OPEX per kW",
    "other_lcoe": "Other costs over LCOE",
    "device_lcoe": "Device costs over LCOE",
    "grid_lcoe": "Grid costs over LCOE",
    "mooring_lcoe": "Moorings and Foundations costs over LCOE",
    "inst_lcoe": "Installation costs over LCOE",
    "opex_lcoe": "OPEX costs over LCOE"
  }
  dict_economical_units = {
    "lcoe": "€/kWh",
    "ace": "m/M€",
    "capex_total": "€",
    "cost_equip": "€",
    "cost_inst": "€",
    "opex_total": "€",
    "opex_year": "€/year"
  }
  dict_financial_units = {
    "pbp": "years",
    "disc_pbp": "years",
    "irr": "%",
    "npv": "€"
  }
  dict_benchmark_units = {
    "capex_kw": "€/kW",
    "opex_kw": "€/kW",
    "other_lcoe": "%",
    "grid_lcoe": "%",
    "device_lcoe": "%",
    "mooring_lcoe": "%",
    "inst_lcoe": "%",
    "opex_lcoe": "%"
  }

  return render_template('gui/output.html',
                         cpx=complexity,
                         economical=dict_economical,
                         financial=dict_financial,
                         benchmark=dict_benchmark,
                         economical_labels=dict_economical_labels,
                         financial_labels=dict_financial_labels,
                         benchmark_labels=dict_benchmark_labels,
                         economical_units=dict_economical_units,
                         financial_units=dict_financial_units,
                         benchmark_units=dict_benchmark_units)
