# System Lifetime Costs (SLC) tool.
# SLC is a DTOceanPlus tool to assess the overall costs of wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
import json

from . import db

from sqlalchemy.orm import backref
from sqlalchemy.ext import mutable


class JsonEncodedDict(db.TypeDecorator):
  """Decode and encode the json data"""
  impl = db.Text

  def process_bind_param(self, value, dialect):
    if value is None:
      return '{}'
    else:
      return json.dumps(value, allow_nan=True)

  def process_result_value(self, value, dialect):
    if value is None:
      return {}
    else:
      return json.loads(value)

mutable.MutableDict.associate_with(JsonEncodedDict)


class SLCProject(db.Model):
  """An SQLAlchemy database model class for a SLC Project."""

  id = db.Column(db.Integer, primary_key=True)    # The ID of the SLC Project
  title = db.Column(db.String(50))
  description = db.Column(db.String(500))
  complexity = db.Column(db.String(5))
  date = db.Column(db.DateTime, default=datetime.now)
  status = db.Column(db.Integer)

  # one-to-one relationship with general inputs
  inputs_general = db.relationship('InputsGeneral',
                                   backref='slc_project',
                                   uselist=False,
                                   cascade='all, delete-orphan')
  # one-to-one relationship with Financial inputs
  inputs_financial = db.relationship('InputsFinancial',
                                     backref='slc_project',
                                     uselist=False,
                                     cascade='all, delete-orphan')
  # one-to-one relationship with ACE inputs
  inputs_ace = db.relationship('InputsACE',
                               backref='slc_project',
                               uselist=False,
                               cascade='all, delete-orphan')
  # one-to-one relationship with External inputs
  inputs_external = db.relationship('InputsExternal',
                                    backref='slc_project',
                                    uselist=False,
                                    cascade='all, delete-orphan')

  # one-to-one relationship with Economical Results
  results = db.relationship('Results',
                            backref='slc_project',
                            uselist=False,
                            cascade='all, delete-orphan')


class InputsGeneral(db.Model):
  """An SQLAlchemy database model class for general SLC inputs."""

  id = db.Column(db.Integer, primary_key=True)
  project_id = db.Column(db.Integer,
                         db.ForeignKey('slc_project.id'),
                         unique=True)
  device_type = db.Column(db.String(16))
  device_topology = db.Column(db.String(16))
  project_life = db.Column(db.Integer)
  discount_rate = db.Column(db.Float)
  device_power = db.Column(db.Float)
  device_costs = db.Column(db.Float)
  device_number = db.Column(db.Integer)
  other_costs = db.Column(db.Float)

class InputsFinancial(db.Model):
  """An SQLAlchemy database model class for inputs related with Financial calculations."""

  id = db.Column(db.Integer, primary_key=True)
  project_id = db.Column(db.Integer,
                         db.ForeignKey('slc_project.id'),
                         unique=True)
  energy_price = db.Column(db.Float)
  financial_grant = db.Column(db.Float)
  fit_price = db.Column(db.Float)
  fit_years = db.Column(db.Integer)

class InputsACE(db.Model):
  """An SQLAlchemy database model class for inputs related with ACE calculations."""

  id = db.Column(db.Integer, primary_key=True)
  project_id = db.Column(db.Integer,
                         db.ForeignKey('slc_project.id'),
                         unique=True)
  accw = db.Column(db.Float)
  surface_area = db.Column(db.Float)
  surface_thick = db.Column(db.Float)
  density = db.Column(db.Float)
  cost_manufacture = db.Column(db.Float)

class InputsExternal(db.Model):
  """An SQLAlchemy database model class for inputs comming from other modules."""

  id = db.Column(db.Integer, primary_key=True)
  project_id = db.Column(db.Integer,
                         db.ForeignKey('slc_project.id'),
                         unique=True)

  mc_general = db.Column(JsonEncodedDict, default={})
  number_devices = db.Column(db.Integer)

  bom_et = db.Column(JsonEncodedDict, default={})
  et_power = db.Column(db.JSON)

  bom_sk = db.Column(JsonEncodedDict, default={})

  bom_ed = db.Column(JsonEncodedDict, default={})

  bom_lmo = db.Column(JsonEncodedDict, default={})
  main_solution = db.Column(JsonEncodedDict, default={})

  aep = db.Column(JsonEncodedDict, default={})


class Results(db.Model):
  """An SQLAlchemy database model class for SLC Economical Results."""
  __tablename__ = 'results'

  id = db.Column(db.Integer, primary_key=True)
  project_id = db.Column(db.Integer,
                         db.ForeignKey('slc_project.id'),
                         unique=True)

  bom = db.Column(JsonEncodedDict, default={})

  lcoe = db.Column(db.Float)
  ace = db.Column(db.Float)
  capex_total = db.Column(db.Float)
  cost_equip = db.Column(db.Float)
  cost_inst = db.Column(db.Float)
  opex_total = db.Column(db.Float)
  opex_year = db.Column(db.Float)

  pay_back_period = db.Column(db.Float)
  disc_pay_back_period = db.Column(db.Float)
  internal_rate_return = db.Column(db.Float)
  net_present_value = db.Column(db.Float)

  capex_kw = db.Column(db.Float)
  opex_kw = db.Column(db.Float)
  bench_other_lcoe = db.Column(db.Float)
  bench_grid_lcoe = db.Column(db.Float)
  bench_device_lcoe = db.Column(db.Float)
  bench_mooring_lcoe = db.Column(db.Float)
  bench_inst_lcoe = db.Column(db.Float)
  bench_opex_lcoe = db.Column(db.Float)

class BOM(db.Model):
  """An SQLAlchemy database model class for the Bill of Materials."""
  results_id = db.Column(db.Integer, db.ForeignKey("results.id"))
  id = db.Column(db.String(50), primary_key=True)
  name = db.Column(db.String(100))
  qnt = db.Column(db.Float)
  uom = db.Column(db.String(10))
  unit_cost = db.Column(db.Float)
  total_cost = db.Column(db.Float)
  category = db.Column(db.String(100))

  def update(self, **kwargs):
    for key, value in kwargs.items():
      setattr(self, key, value)
