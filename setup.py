# This file is for creating Python package.

# Packaging Python Projects: https://packaging.python.org/tutorials/packaging-projects/
# Setuptools documentation: https://setuptools.readthedocs.io/en/latest/index.html

import setuptools

setuptools.setup(
    name="dtop-slc",
    version="1.0.0",
    packages=setuptools.find_packages(where='src'),
    package_dir={'':'src'},
    install_requires=[
        "dredd_hooks",
        "flask",
        "flask-babel",
        "flask_cors",
        "flask-sqlalchemy",
        "flask-marshmallow<0.12",
        "marshmallow-sqlalchemy<=0.25.0",
        "requests",
        "pandas",
        "numpy==1.19.4"
    ],
    include_package_data=True,
    zip_safe=False,
)
