# This file is intended primarily for CI.

SHELL := bash -o pipefail
.PHONY: *

## !
## Set the required module nickname
MODULE_SHORT_NAME=slc

## !
## Set the required docker image TAG
MODULE_TAG=1.0.0

## !
## Update the values of CI_REGISTRY_IMAGE to your module registry
CI_REGISTRY_IMAGE?=registry.gitlab.com/dtoceanplus/dtop_slc


login:
	echo ${CI_REGISTRY_PASSWORD} | docker login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY_IMAGE}

logout:
	docker logout ${CI_REGISTRY_IMAGE}

build-prod-be:
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} || true
	docker build --cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  --tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  --file ./${MODULE_SHORT_NAME}-prod.dockerfile \
          .
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG}
	docker images

build-prod-fe:
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} || true
	docker build --build-arg DTOP_MODULE_SHORT_NAME="${MODULE_SHORT_NAME}" \
	  --cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
	  --tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
      --file ./src/dtop_slc/gui/frontend-prod.dockerfile \
	  ./src/dtop_slc
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG}
	docker images

help:
	@echo URLs for access to properly deployed module ${MODULE_SHORT_NAME} on Staging server :
	@echo "       " - https://${MODULE_SHORT_NAME}.dto.opencascade.com
	@echo "       " - https://${MODULE_SHORT_NAME}.dto.opencascade.com/api
