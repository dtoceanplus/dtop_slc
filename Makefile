# 'build-TEST' builds a Docker image for test
# 'TEST' runs build and then the test itself
# 'TEST-nodeps' runs test, assuming the image exists already
# 'x-TEST' is what actually runs inside the image
# Force re-build all

SHELL = bash
.PHONY: *
export MODULE_SHORT_NAME = slc
export DTOP_DOMAIN = dto.opencascade.com

build-%:
	docker-compose `find ci -name '*.docker-compose' -printf ' -f %p'` build $*

run:
	docker-compose --file docker-compose.test.yml --file docker-compose.yml up nginx frontend backend mm

build-backend:
	docker-compose --file docker-compose.test.yml --file docker-compose.yml build backend


impose:
	docker-compose run --rm contracts

impose-local:
	make -f ci.makefile slc-deploy
	docker-compose run --rm contracts

verify:
	docker-compose --project-name slc -f verifiable.docker-compose -f verifier.docker-compose run verifier
	docker-compose --project-name slc -f verifiable.docker-compose stop slc
	docker cp `docker-compose --project-name slc -f verifiable.docker-compose ps -q`:/app/.coverage.make-verify

verifier:
	docker-compose --project-name slc -f verifiable.docker-compose stop slc
	docker cp `docker-compose --project-name slc -f verifiable.docker-compose ps -q`:/app/.coverage.make-verify
