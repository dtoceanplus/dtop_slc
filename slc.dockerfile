FROM python:3.7


WORKDIR /app

COPY  . .

RUN pip install -e .
RUN pip install -r requirements.txt
RUN pip install python-dotenv

EXPOSE 5000

CMD python3 -m flask run
