import SLCMain from '../pages/slc/main.page';

describe('included auth', () => {
  before(() => {
    Cypress.Cookies.defaults({ whitelist: 'accessToken' });
  });

  it('auth', () => {
    SLCMain.open()
    cy.wait(10000);
    cy.get('[data-cy-slc=authModulePlacement] iframe').then(($iframe) => {
      const iframeBody = $iframe.contents().find('body');
      cy.wrap(iframeBody).find('[data-cy-auth=loginInput]').type('admin@dtop.com');
      cy.wrap(iframeBody).find('[data-cy-auth=passwordInput]').type('j2zjf#afw21');
      cy.wrap(iframeBody).find('[data-cy-auth=formSubmitButton]').click();
    });
    cy.wait(10000);
    cy.get('[data-cy-slc=authModulePlacement] iframe').should('not.exist');
  });

  it('logout', () => {
    SLCMain.open();
    cy.wait(10000);
    cy.get('[data-cy-slc=userPanelModulePlacement] iframe').then(($iframe) => {
      const iframeBody = $iframe.contents().find('body');
      cy.wrap(iframeBody).find('[data-cy-auth=logoutButton]').click();
    });
    cy.get('[data-cy-slc=userPanelModulePlacement] iframe').should('not.exist');
    cy.wait(10000);
  });

  it('a few actions during one session', () => {
    SLCMain.open();
    cy.wait(10000);
    cy.get('[data-cy-slc=authModulePlacement] iframe').then(($iframe) => {
      const iframeBody = $iframe.contents().find('body');
      cy.wrap(iframeBody).find('[data-cy-auth=loginInput]').type('admin@dtop.com');
      cy.wrap(iframeBody).find('[data-cy-auth=passwordInput]').type('j2zjf#afw21');
      cy.wrap(iframeBody).find('[data-cy-auth=formSubmitButton]').click();
    });
    cy.wait(10000);
    cy.get('[data-cy-slc=authModulePlacement] iframe').should('not.exist');
    cy.get('[data-cy-slc=userPanelModulePlacement] iframe').then(($iframe) => {
      const iframeBody = $iframe.contents().find('body');
      cy.wrap(iframeBody).find('[data-cy-auth=logoutButton]').click();
    });
    cy.wait(10000);
    cy.get('[data-cy-slc=userPanelModulePlacement] iframe').should('not.exist');
    cy.get('[data-cy-slc=authModulePlacement] iframe').then(($iframe) => {
      const iframeBody = $iframe.contents().find('body');
      cy.wrap(iframeBody).find('[data-cy-auth=loginInput]').type('admin@dtop.com');
      cy.wrap(iframeBody).find('[data-cy-auth=passwordInput]').type('j2zjf#afw21');
      cy.wrap(iframeBody).find('[data-cy-auth=formSubmitButton]').click();
    });
    cy.wait(10000);
    cy.get('[data-cy-slc=authModulePlacement] iframe').should('not.exist');
    cy.get('[data-cy-slc=userPanelModulePlacement] iframe').then(($iframe) => {
      const iframeBody = $iframe.contents().find('body');
      cy.wrap(iframeBody).find('[data-cy-auth=logoutButton]').click();
    });
  });
});
