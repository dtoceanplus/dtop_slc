const visitDashboard = () => {
    cy.visit('/#/dashboard');
};

const checkTitle = () => {
    cy.contains('Welcome to DTOceanPlus').should('be.visible');
};

export default {
    visitDashboard,
    checkTitle,
};
