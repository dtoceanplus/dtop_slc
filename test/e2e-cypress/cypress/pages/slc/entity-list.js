export const getInput = iframe => {
    return cy.wrap(iframe).find("[data-cy-slc=input]");
  };
  export const getAvailibleId = (iframe, result, name = "PT1") => {
    return cy.wrap(iframe).find(`[data-cy-slc=availibleId${name}]`);
  };
  export const getConsumedId = (iframe, result, name = "PT1") => {
    return cy.wrap(iframe).find(`[data-cy-slc=consumedId${name}]`);
  };
  export const fillInput = iframe => {
    getInput(iframe).type("1234");
  };

  export const openSelect = iframe => {
    cy.wrap(iframe)
      .find("[data-cy-slc=select]")
      .click()
  };
  export const fillSelect = iframe => {
    cy.wrap(iframe)
    .find("[data-cy-slc=selectItem1]")
    .click()
  };
  export const submitCreateForm = iframe => {
    cy.wrap(iframe)
      .find("[data-cy-slc=submitCreateButton]")
      .click();
  };
  export const submitDeleteForm = iframe => {
    cy.wrap(iframe)
      .find("[data-cy-slc=submitDeleteButton]")
      .click();
  };

  export default {
    getInput,
    fillInput,
    openSelect,
    fillSelect,
    submitCreateForm,
    submitDeleteForm,
    getAvailibleId,
    getConsumedId
  };
