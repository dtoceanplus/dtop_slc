export const getTitle = iframe => {
    return cy.wrap(iframe).find("[data-cy-slc=title]");
  };
  export const goToForm = iframe => {
    cy.wrap(iframe).find("[data-cy-slc=performLink]").click()
  };
  export const submitForm = iframe => {
    cy.wrap(iframe).find("[data-cy-slc=submitButton]").click()
  };


  export default {
    getTitle,
    goToForm,
    submitForm
  };
