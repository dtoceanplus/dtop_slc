from dtop_slc.business import Financial
import pandas as pd

def test_positive_cashflows():
  tables_cols = ['id','cost','proj_year','cost_label','category']
  capex_list = ['id',7000000,0,'capex','Device']
  opex_list = [['op1',10000,2,'opex','OPEX'],
               ['op2',50000,8,'opex','OPEX']
              ]
  capex_Table = pd.DataFrame([capex_list],columns=tables_cols)
  opex_Table = pd.DataFrame(opex_list,columns=tables_cols)
  expenses = pd.concat([capex_Table,opex_Table])

  Financial2 = Financial.get_complexity(
                 complexity = 'med',
                 proj_life=10,
                 disc_rate=10,
                 array_avr_energy_prod={
                    "array_annual_net_energy_pd": {
                          "value": '''{
                              \"1\":100000,
                              \"2\":100000,
                              \"3\":100000,
                              \"4\":100000,
                              \"5\":100000,
                              \"6\":100000,
                              \"7\":100000,
                              \"8\":100000,
                              \"9\":100000,
                              \"10\":100000,
                          }''',
                          "Aggregation-Level": "array"
                      }
                  },
                 financial_grant=True,
                 financial_fit=True,
                 market_price=0.055,
                 grant_value=12000000,
                 feed_in_tariff_value=0.3,
                 feed_in_tariff_years=10,
                 expenses_overall=expenses
              )
  Financial2.payback_period()

  Financial3 = Financial.get_complexity(
                 complexity = 'high',
                 proj_life=10,
                 disc_rate=10,
                 array_avr_energy_prod={
                    "array_annual_net_energy_pd": {
                          "value": '''{
                              \"1\":100000,
                              \"2\":100000,
                              \"3\":100000,
                              \"4\":100000,
                              \"5\":100000,
                              \"6\":100000,
                              \"7\":100000,
                              \"8\":100000,
                              \"9\":100000,
                              \"10\":100000,
                          }''',
                          "Aggregation-Level": "array"
                      }
                  },
                 financial_grant=True,
                 financial_fit=True,
                 market_price=0.055,
                 grant_value=12000000,
                 feed_in_tariff_value=0.3,
                 feed_in_tariff_years=10,
                 expenses_overall=expenses
              )
  Financial3.payback_period()
