from dtop_slc.business.core import bom_compiler
import json

def test_core():
  # Load BoMs
  import os
  _local_path = '/src/dtop_slc/service/api/inputs'
  _inputs_path = os.getcwd() + _local_path

  _file = open(_inputs_path + '/bom_et.json')
  _file_str = _file.read()
  _file.close()
  bom_et = json.loads(_file_str)
  _file = open(_inputs_path + '/bom_ed.json')
  _file_str = _file.read()
  _file.close()
  bom_ed = json.loads(_file_str)
  _file = open(_inputs_path + '/bom_sk.json')
  _file_str = _file.read()
  _file.close()
  bom_sk = json.loads(_file_str)
  _file = open(_inputs_path + '/bom_lmo.json')
  _file_str = _file.read()
  _file.close()
  bom_lmo = json.loads(_file_str)

  bom = bom_compiler(bom_et=bom_et,
                     bom_ed=bom_ed,
                     bom_sk=bom_sk,
                     bom_lmo=bom_lmo,
                     device_topology='None')
  bom = bom_compiler(bom_et=bom_et,
                     bom_ed=bom_ed,
                     bom_sk=bom_sk,
                     bom_lmo=bom_lmo,
                     device_topology='wec_fix')
  bom = bom_compiler(bom_et=bom_et,
                     bom_ed=bom_ed,
                     bom_sk=bom_sk,
                     bom_lmo=bom_lmo,
                     device_topology='wec_float')
  bom = bom_compiler(bom_et=bom_et,
                     bom_ed=bom_ed,
                     bom_sk=bom_sk,
                     bom_lmo=bom_lmo,
                     device_topology='tec_fix')
  bom = bom_compiler(bom_et=bom_et,
                     bom_ed=bom_ed,
                     bom_sk=bom_sk,
                     bom_lmo=bom_lmo,
                     device_topology='tec_float')

  bom = bom_compiler(bom_et=bom_et,
                     bom_ed=bom_ed,
                     bom_sk=bom_sk,
                     bom_lmo=bom_lmo)
  bom = bom_compiler(device_struct_cost='float')
  bom = bom_compiler(number_of_devices='integer')
  bom = bom_compiler(other_costs='float')
