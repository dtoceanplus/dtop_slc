from dtop_slc.business import Economical
from dtop_slc.business import Financial
from dtop_slc.business import Benchmark

def test_convert_inputs_cpx1():
    # METRICS
    Economical1 = Economical.get_complexity(
                    complexity='low',
                    proj_life='integer'
                  )
    Economical1 = Economical.get_complexity(
                    complexity='low',
                    disc_rate='float'
                  )
    Economical1 = Economical.get_complexity(
                    complexity='low',
                    compute_ace=True,
                    accw='float'
                  )
    Economical1 = Economical.get_complexity(
                    complexity='low',
                    compute_ace=True
                  )
    Economical1 = Economical.get_complexity(
                    complexity='low',
                    compute_ace=True,
                    accw=20,
                    device_surface=None
                  )
    Economical1 = Economical.get_complexity(
                    complexity='low',
                    compute_ace=True,
                    accw=20,
                    device_surface='float'
                  )
    Economical1 = Economical.get_complexity(
                    complexity='low',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=None
                  )
    Economical1 = Economical.get_complexity(
                    complexity='low',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick='float'
                  )
    Economical1 = Economical.get_complexity(
                    complexity='low',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=0.1,
                    mat_dens=None
                  )
    Economical1 = Economical.get_complexity(
                    complexity='low',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=0.1,
                    mat_dens='float'
                  )
    Economical1 = Economical.get_complexity(
                    complexity='low',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=0.1,
                    mat_dens=1000,
                    mat_cost=None
                  )
    Economical1 = Economical.get_complexity(
                    complexity='low',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=0.1,
                    mat_dens=1000,
                    mat_cost='float'
                  )
    Economical1 = Economical.get_complexity(
                    complexity='low',
                    bom_compiled='other'
                  )
    Economical1.capex_compiler()
    # Economical1.ace_compiler()

    Benchmark1 = Benchmark.get_complexity(
                    complexity='low',
                    capex_total='float'
                  )
    Benchmark1 = Benchmark.get_complexity(
                    complexity='low',
                    opex_total='float'
                  )
    Benchmark1 = Benchmark.get_complexity(
                    complexity='low',
                    device_power='on'
                  )
    Benchmark1 = Benchmark.get_complexity(
                    complexity='low',
                    number_of_devices='on'
                  )
    Benchmark1 = Benchmark.get_complexity(
                    complexity='low'
                  )
    Benchmark1.metrics_per_kw()
    Benchmark1 = Benchmark.get_complexity(
                    complexity='low',
                    capex_total=10000
                  )
    Benchmark1.metrics_per_kw()
    Benchmark1 = Benchmark.get_complexity(
                    complexity='low',
                    capex_total=10000,
                    device_power=500
                  )
    Benchmark1.metrics_per_kw()
    Benchmark1 = Benchmark.get_complexity(
                    complexity='low',
                    capex_total=10000,
                    device_power=500,
                    number_of_devices=1
                  )
    Benchmark1.metrics_per_kw()


def test_convert_inputs_cpx2():
    # METRICS
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    proj_life='integer'
                  )
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    disc_rate='float'
                  )
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    array_avr_energy_prod={
                        "array_annual_net_energy_pd": {
                            "value": '''{
                              \"1\":'float',
                              \"2\":'float'
                            }'''
                        }
                    }
                  )
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    compute_ace=True,
                    accw='float'
                  )
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    compute_ace=True
                  )
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    compute_ace=True,
                    accw=20,
                    device_surface=None
                  )
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    compute_ace=True,
                    accw=20,
                    device_surface='float'
                  )
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=None
                  )
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick='float'
                  )
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=0.1,
                    mat_dens=None
                  )
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=0.1,
                    mat_dens='float'
                  )
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=0.1,
                    mat_dens=1000,
                    mat_cost=None
                  )
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=0.1,
                    mat_dens=1000,
                    mat_cost='float'
                  )
    Economical2 = Economical.get_complexity(
                    complexity='med',
                    bom_compiled='other'
                  )
    Economical2.capex_compiler()
    Economical2.ace_compiler()

    Financial2 = Financial.get_complexity(
                    complexity='med',
                    proj_life='integer'
                  )
    Financial2 = Financial.get_complexity(
                    complexity='med',
                    disc_rate='float'
                  )
    Financial2 = Financial.get_complexity(
                    complexity='med',
                    array_avr_energy_prod={
                        "array_annual_net_energy_pd": {
                            "value": '''{
                              \"1\":'float',
                              \"2\":'float'
                            }'''
                        }
                    }
                  )
    Financial2 = Financial.get_complexity(
                    complexity='med',
                    market_price='float'
                  )
    Financial2 = Financial.get_complexity(
                    complexity='med',
                    financial_grant=True
                  )
    Financial2 = Financial.get_complexity(
                    complexity='med',
                    financial_grant=True,
                    grant_value='float'
                  )
    Financial2 = Financial.get_complexity(
                    complexity='med',
                    financial_fit=True
                  )
    Financial2 = Financial.get_complexity(
                    complexity='med',
                    financial_fit=True,
                    feed_in_tariff_value='float'
                  )
    Financial2 = Financial.get_complexity(
                    complexity='med',
                    financial_fit=True,
                    feed_in_tariff_value=0.3,
                    feed_in_tariff_years='integer'
                  )
    Financial2 = Financial.get_complexity(
                    proj_life=5,
                    complexity='med',
                    financial_fit=True,
                    feed_in_tariff_value=0.3,
                    feed_in_tariff_years=7
                  )

    Benchmark2 = Benchmark.get_complexity(
                    complexity='med',
                    disc_rate='float'
                  )
    Benchmark2 = Benchmark.get_complexity(
                    complexity='med',
                    capex_total='float'
                  )
    Benchmark2 = Benchmark.get_complexity(
                    complexity='med',
                    opex_total='float'
                  )
    Benchmark2 = Benchmark.get_complexity(
                    complexity='med',
                    disc_costs='float'
                  )
    Benchmark2 = Benchmark.get_complexity(
                    complexity='med',
                    device_power='on'
                  )
    Benchmark2 = Benchmark.get_complexity(
                    complexity='med',
                    number_of_devices='on'
                  )
    Benchmark2 = Benchmark.get_complexity(
                    complexity='med'
                  )
    Benchmark2.metrics_per_kw()
    Benchmark2 = Benchmark.get_complexity(
                    complexity='med',
                    capex_total=10000
                  )
    Benchmark2.metrics_per_kw()
    Benchmark2 = Benchmark.get_complexity(
                    complexity='med',
                    capex_total=10000,
                    device_power=500
                  )
    Benchmark2.metrics_per_kw()
    Benchmark2 = Benchmark.get_complexity(
                    complexity='med',
                    capex_total=10000,
                    device_power=500,
                    number_of_devices=1
                  )
    Benchmark2.metrics_per_kw()

def test_convert_inputs_cpx3():
    # METRICS
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    proj_life='integer'
                  )
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    disc_rate='float'
                  )
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    array_avr_energy_prod={
                        "array_annual_net_energy_pd": {
                            "value": '''{
                              \"1\":'float',
                              \"2\":'float'
                            }'''
                        }
                    }
                  )
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    compute_ace=True,
                    accw='float'
                  )
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    compute_ace=True
                  )
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    compute_ace=True,
                    accw=20,
                    device_surface=None
                  )
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    compute_ace=True,
                    accw=20,
                    device_surface='float'
                  )
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=None
                  )
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick='float'
                  )
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=0.1,
                    mat_dens=None
                  )
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=0.1,
                    mat_dens='float'
                  )
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=0.1,
                    mat_dens=1000,
                    mat_cost=None
                  )
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    compute_ace=True,
                    accw=20,
                    device_surface=20,
                    device_thick=0.1,
                    mat_dens=1000,
                    mat_cost='float'
                  )
    Economical3 = Economical.get_complexity(
                    complexity='high',
                    bom_compiled='other'
                  )
    Economical3.capex_compiler()
    Economical3.ace_compiler()

    Financial3 = Financial.get_complexity(
                    complexity='high',
                    proj_life='integer'
                  )
    Financial3 = Financial.get_complexity(
                    complexity='high',
                    disc_rate='float'
                  )
    Financial3 = Financial.get_complexity(
                    complexity='high',
                    array_avr_energy_prod={
                        "array_annual_net_energy_pd": {
                            "value": '''{
                              \"1\":'float',
                              \"2\":'float'
                            }'''
                        }
                    }
                  )
    Financial3 = Financial.get_complexity(
                    complexity='high',
                    market_price='float'
                  )
    Financial3 = Financial.get_complexity(
                    complexity='high',
                    financial_grant=True
                  )
    Financial3 = Financial.get_complexity(
                    complexity='high',
                    financial_grant=True,
                    grant_value='float'
                  )
    Financial3 = Financial.get_complexity(
                    complexity='high',
                    financial_fit=True
                  )
    Financial3 = Financial.get_complexity(
                    complexity='high',
                    financial_fit=True,
                    feed_in_tariff_value='float'
                  )
    Financial3 = Financial.get_complexity(
                    complexity='high',
                    financial_fit=True,
                    feed_in_tariff_value=0.3,
                    feed_in_tariff_years='integer'
                  )
    Financial3 = Financial.get_complexity(
                    proj_life=5,
                    complexity='high',
                    financial_fit=True,
                    feed_in_tariff_value=0.3,
                    feed_in_tariff_years=7
                  )

    Benchmark3 = Benchmark.get_complexity(
                    complexity='high',
                    disc_rate='float'
                  )
    Benchmark3 = Benchmark.get_complexity(
                    complexity='high',
                    capex_total='float'
                  )
    Benchmark3 = Benchmark.get_complexity(
                    complexity='high',
                    opex_total='float'
                  )
    Benchmark3 = Benchmark.get_complexity(
                    complexity='high',
                    disc_costs='float'
                  )
    Benchmark3 = Benchmark.get_complexity(
                    complexity='high',
                    device_power='on'
                  )
    Benchmark3 = Benchmark.get_complexity(
                    complexity='high',
                    number_of_devices='on'
                  )
    Benchmark3 = Benchmark.get_complexity(
                    complexity='high'
                  )
    Benchmark3.metrics_per_kw()
    Benchmark3 = Benchmark.get_complexity(
                    complexity='high',
                    capex_total=10000
                  )
    Benchmark3.metrics_per_kw()
    Benchmark3 = Benchmark.get_complexity(
                    complexity='high',
                    capex_total=10000,
                    device_power=500
                  )
    Benchmark3.metrics_per_kw()
    Benchmark3 = Benchmark.get_complexity(
                    complexity='high',
                    capex_total=10000,
                    device_power=500,
                    number_of_devices=1
                  )
    Benchmark3.metrics_per_kw()
