from dtop_slc.business.core import bom_compiler
from dtop_slc.business.cpx1.economical1 import Economical1
from dtop_slc.business.cpx2.economical2 import Economical2
from dtop_slc.business.cpx3.economical3 import Economical3
from dtop_slc.business.cpx1.financial1 import Financial1
from dtop_slc.business.cpx2.financial2 import Financial2
from dtop_slc.business.cpx3.financial3 import Financial3
from dtop_slc.business.cpx1.benchmark1 import Benchmark1
from dtop_slc.business.cpx2.benchmark2 import Benchmark2
from dtop_slc.business.cpx3.benchmark3 import Benchmark3
import json

# Load BoMs
import os
_local_path = '/src/dtop_slc/service/api/inputs'
_inputs_path = os.getcwd() + _local_path

_file = open(_inputs_path + '/bom_et.json')
_file_str = _file.read()
_file.close()
bom_et = json.loads(_file_str)
_file = open(_inputs_path + '/bom_ed.json')
_file_str = _file.read()
_file.close()
bom_ed = json.loads(_file_str)
_file = open(_inputs_path + '/bom_sk.json')
_file_str = _file.read()
_file.close()
bom_sk = json.loads(_file_str)
_file = open(_inputs_path + '/bom_lmo.json')
_file_str = _file.read()
_file.close()
bom_lmo = json.loads(_file_str)

main_solu = {
    "operation_id": ["OP13_0", "OP13_0", "OP13_0", "OP13_0", "OP13_0"],
    "proj_year": [1, 2, 3, 4, 5],
    "cost": [75000, 75000, 75000, 75000, 75000],
    "cost_label": ["OPEX", "OPEX", "OPEX", "OPEX", "OPEX"]
}


def test_empty():
  class_Economical = Economical1()
  assert class_Economical.capex_total == None
  assert class_Economical.opex_total == None
  assert class_Economical.opex_year == None
  assert class_Economical.ace == None
  class_Economical.get_inputs()
  class_Economical.print_inputs()
  # class_Economical.capex_compiler()
  # class_Economical.opex_compiler()

  class_Economical = Economical2()
  assert class_Economical.capex_total == None
  assert class_Economical.capex_table == None
  assert class_Economical.opex_total == None
  assert class_Economical.opex_table == None
  assert class_Economical.opex_year == None
  assert class_Economical.expenses_overall == None
  assert class_Economical.lcoe == None
  assert class_Economical.ace == None
  class_Economical.get_inputs()
  class_Economical.print_inputs()
  # class_Economical.capex_compiler()

  class_Economical = Economical3()
  assert class_Economical.capex_total == None
  assert class_Economical.capex_table == None
  assert class_Economical.opex_total == None
  assert class_Economical.opex_table == None
  assert class_Economical.opex_year == None
  assert class_Economical.expenses_overall == None
  assert class_Economical.lcoe == None
  assert class_Economical.ace == None
  class_Economical.get_inputs()
  class_Economical.print_inputs()

  class_Financial = Financial2()
  assert class_Financial.pbp == None
  assert class_Financial.irr == None
  assert class_Financial.npv == None
  fin_inputs = class_Financial.get_inputs()
  class_Financial.print_inputs()

  class_Financial = Financial3()
  assert class_Financial.pbp == None
  assert class_Financial.irr == None
  assert class_Financial.npv == None
  fin_inputs = class_Financial.get_inputs()
  class_Financial.print_inputs()

  class_Benchmark = Benchmark1()
  assert class_Benchmark.capex_kw == None
  assert class_Benchmark.opex_kw == None
  bench_inputs = class_Benchmark.get_inputs()
  class_Benchmark.print_inputs()

  class_Benchmark = Benchmark2()
  assert class_Benchmark.capex_kw == None
  assert class_Benchmark.opex_kw == None
  assert class_Benchmark.other_breakdown == None
  assert class_Benchmark.grid_breakdown == None
  assert class_Benchmark.moor_found_breakdown == None
  assert class_Benchmark.installation_breakdown == None
  assert class_Benchmark.opex_breakdown == None
  bench_inputs = class_Benchmark.get_inputs()
  class_Benchmark.print_inputs()

  class_Benchmark = Benchmark3()
  assert class_Benchmark.capex_kw == None
  assert class_Benchmark.opex_kw == None
  assert class_Benchmark.other_breakdown == None
  assert class_Benchmark.grid_breakdown == None
  assert class_Benchmark.moor_found_breakdown == None
  assert class_Benchmark.installation_breakdown == None
  assert class_Benchmark.opex_breakdown == None
  bench_inputs = class_Benchmark.get_inputs()
  class_Benchmark.print_inputs()


def test_half_empty():
  Economical_metrics = Economical1(
                         proj_life = 20,
                         bom_compiled = bom_compiler(bom_et=bom_et,
                                                     bom_ed=bom_ed,
                                                     bom_sk=bom_sk,
                                                     bom_lmo=bom_lmo),
                         cost_of_equipment = 10000000,
                         cost_of_installation = 56000000,
                         maintenance_solution=main_solu
                       )
  Economical_metrics.capex_compiler()
  Economical_metrics.opex_compiler()

  Economical_metrics = Economical2(
                         proj_life = 20,
                         disc_rate = 10,
                         bom_compiled = bom_compiler(bom_et=bom_et,
                                                     bom_ed=bom_ed,
                                                     bom_sk=bom_sk,
                                                     bom_lmo=bom_lmo),
                         cost_of_equipment = 10000000,
                         cost_of_installation = 56000000,
                         maintenance_solution=main_solu
                       )
  Economical_metrics.capex_compiler()
  Economical_metrics.opex_compiler()

  Economical_metrics = Economical3(
                         proj_life = 20,
                         disc_rate = 10,
                         bom_compiled = bom_compiler(bom_et=bom_et,
                                                     bom_ed=bom_ed,
                                                     bom_sk=bom_sk,
                                                     bom_lmo=bom_lmo),
                         cost_of_equipment = 10000000,
                         cost_of_installation = 56000000,
                         maintenance_solution=main_solu
                       )
  Economical_metrics.capex_compiler()
  Economical_metrics.opex_compiler()
