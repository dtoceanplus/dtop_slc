from dtop_slc.business.core import bom_compiler
from dtop_slc.business import Economical
from dtop_slc.business import Financial
from dtop_slc.business import Benchmark

def test_case1_cpx1():
  import pandas as pd
  import json

  # INPUTS
  complexity = 'low'
  device_topology = None
  device_structural_costs = 1000000
  device_power = 100
  number_of_devices = 1
  other_costs = 1000000
  project_life = 5
  discount_rate = 10
  aep_array = [1000000,1000000,1000000,1000000,1000000]
  accw = 10
  device_surface = 100
  device_thickness = 0.1
  material_density = 1000
  material_cost = 2

  bom_et = pd.read_csv('test/test_inputs/case1/bom_et.csv',
                       low_memory=False, header=0)
  bom_ed = pd.read_csv('test/test_inputs/case1/bom_ed.csv',
                       low_memory=False, header=0)
  bom_sk = pd.read_csv('test/test_inputs/case1/bom_sk.csv',
                       low_memory=False, header=0)
  bom_lmo = pd.read_csv('test/test_inputs/case1/bom_lmo.csv',
                        low_memory=False, header=0)
  with open('test/test_inputs/case1/aep.json') as json_file:
    aep_spey = json.load(json_file)
    list_aep = aep_spey['array_annual_net_energy_pd']

  bom =  bom_compiler(bom_et=bom_et,
                      bom_ed=bom_ed,
                      bom_sk=bom_sk,
                      bom_lmo=bom_lmo,
                      device_topology=device_topology,
                      device_struct_cost=device_structural_costs,
                      number_of_devices=number_of_devices,
                      other_costs=other_costs)
  assert bom['Cost_of_equipment'] == 7500000
  assert bom['Cost_of_installation'] == 5500000
  assert bom['Other_costs'] == 1000000

  main_solu = pd.read_csv('test/test_inputs/case1/maintenance_solution_example.csv',
                          low_memory=False, header=0)

  Economical_metrics = Economical.get_complexity(
                          complexity=complexity,
                          proj_life=project_life,
                          disc_rate=discount_rate,
                          bom_compiled=bom['BoM'],
                          cost_of_installation=bom['Cost_of_installation'],
                          cost_of_equipment=bom['Cost_of_equipment'],
                          cost_other=bom['Other_costs'],
                          maintenance_solution=main_solu,
                          array_avr_energy_prod=aep_array,
                          compute_ace=True,
                          accw=accw,
                          device_surface=device_surface,
                          device_thick=device_thickness,
                          mat_dens=material_density,
                          mat_cost=material_cost
                        )
  Economical_metrics.get_inputs()
  Economical_metrics.print_inputs()
  capex = Economical_metrics.capex_compiler()
  opex = Economical_metrics.opex_compiler()

  assert capex['Total CAPEX'] == 14000000
  assert opex['Total OPEX'] == 750000
  assert opex['AVR OPEX per year'] == 150000
  assert Economical_metrics.ace_compiler() == 500

  Financial_metrics = Financial.get_complexity(
                        complexity=complexity
                      )
  Financial_metrics.get_inputs()
  Financial_metrics.print_inputs()
  assert Financial_metrics.complexity == 'low'

  Benchmark_metrics = Benchmark.get_complexity(
                        complexity=complexity,
                        proj_life=project_life,
                        capex_total=capex['Total CAPEX'],
                        opex_total=opex['Total OPEX'],
                        device_power=device_power,
                        number_of_devices=number_of_devices
                      )
  Benchmark_metrics.get_inputs()
  Benchmark_metrics.print_inputs()
  metrics_kw = Benchmark_metrics.metrics_per_kw()

  assert metrics_kw['CAPEX per kW'] == 140000
  assert metrics_kw['OPEX per kW'] == 1500

def test_case1_cpx2():
  import pandas as pd
  import json

  # INPUTS
  complexity = 'med'
  device_topology = None
  device_structural_costs = 1000000
  device_power = 100
  number_of_devices = 1
  other_costs = 1000000
  project_life = 5
  discount_rate = 10
  aep_array = {
    "array_annual_net_energy_pd": {
          "value": '''{
              \"1\":1000000,
              \"2\":1000000,
              \"3\":1000000,
              \"4\":1000000,
              \"5\":1000000
          }''',
          "Aggregation-Level": "array"
      }
  }
  accw = 10
  device_surface = 100
  device_thickness = 0.1
  material_density = 1000
  material_cost = 2
  energy_price = 0.05

  bom_et = pd.read_csv('test/test_inputs/case1/bom_et.csv',
                       low_memory=False, header=0)
  bom_ed = pd.read_csv('test/test_inputs/case1/bom_ed.csv',
                       low_memory=False, header=0)
  bom_sk = pd.read_csv('test/test_inputs/case1/bom_sk.csv',
                       low_memory=False, header=0)
  bom_lmo = pd.read_csv('test/test_inputs/case1/bom_lmo.csv',
                        low_memory=False, header=0)
  with open('test/test_inputs/case1/aep.json') as json_file:
    aep_spey = json.load(json_file)
    list_aep = aep_spey['array_annual_net_energy_pd']

  bom =  bom_compiler(bom_et=bom_et,
                      bom_ed=bom_ed,
                      bom_sk=bom_sk,
                      bom_lmo=bom_lmo,
                      device_topology=device_topology,
                      device_struct_cost=device_structural_costs,
                      number_of_devices=number_of_devices,
                      other_costs=other_costs)
  assert bom['Cost_of_equipment'] == 7500000
  assert bom['Cost_of_installation'] == 5500000
  assert bom['Other_costs'] == 1000000

  main_solu = pd.read_csv('test/test_inputs/case1/maintenance_solution_example.csv',
                          low_memory=False, header=0)

  Economical_metrics = Economical.get_complexity(
                          complexity=complexity,
                          proj_life=project_life,
                          disc_rate=discount_rate,
                          bom_compiled=bom['BoM'],
                          cost_of_installation=bom['Cost_of_installation'],
                          cost_of_equipment=bom['Cost_of_equipment'],
                          cost_other=bom['Other_costs'],
                          maintenance_solution=main_solu,
                          array_avr_energy_prod=aep_array,
                          compute_ace=True,
                          accw=accw,
                          device_surface=device_surface,
                          device_thick=device_thickness,
                          mat_dens=material_density,
                          mat_cost=material_cost
                        )
  Economical_metrics.get_inputs()
  Economical_metrics.print_inputs()

  # Try to run cost_compiler before running capex and opex compiler
  costs = Economical_metrics.cost_compiler()

  capex = Economical_metrics.capex_compiler()
  opex = Economical_metrics.opex_compiler()
  costs = Economical_metrics.cost_compiler()

  assert capex['Total CAPEX'] == 14000000
  assert opex['Total OPEX'] == 750000
  assert opex['AVR OPEX per year'] == 150000
  assert round(Economical_metrics.lcoe_compiler(),2) == 3.48
  assert Economical_metrics.ace_compiler() == 500
  assert int(costs['Discounted costs']) == 14568618

  Financial_metrics = Financial.get_complexity(
                        complexity=complexity,
                        proj_life=project_life,
                        disc_rate=discount_rate,
                        array_avr_energy_prod=aep_array,
                        financial_grant=None,
                        financial_fit=None,
                        market_price=energy_price,
                        grant_value=None,
                        feed_in_tariff_value=None,
                        feed_in_tariff_years=None,
                        expenses_overall=costs['Overall Expenses']
                      )
  Financial_metrics.get_inputs()
  Financial_metrics.print_inputs()

  # Try to run payback_period before running net_present_value
  Financial_metrics.payback_period()

  npv = Financial_metrics.net_present_value()
  irr = Financial_metrics.internal_rate_return()
  pbp = Financial_metrics.payback_period()
  dpbp = Financial_metrics.payback_period(disc_pbp=True)

  cashflows = Financial_metrics.cf
  disc_cashflows = Financial_metrics.dcf

  assert cashflows[0] == -capex['Total CAPEX']
  assert cashflows[1] == -100000
  assert int(disc_cashflows[1]) == -90909
  assert cashflows[2] == -100000
  assert int(disc_cashflows[2]) == -82644
  assert cashflows[3] == -100000
  assert int(disc_cashflows[3]) == -75131
  assert cashflows[4] == -100000
  assert int(disc_cashflows[4]) == -68301
  assert cashflows[5] == -100000
  assert int(disc_cashflows[5]) == -62092

  assert int(npv) == -14379078
  assert irr == None
  assert pbp == None
  assert dpbp == None

  Benchmark_metrics = Benchmark.get_complexity(
                        complexity=complexity,
                        proj_life=project_life,
                        disc_rate=discount_rate,
                        expenses_overall=costs['Overall Expenses'],
                        capex_total=capex['Total CAPEX'],
                        opex_total=opex['Total OPEX'],
                        disc_costs=costs['Discounted costs'],
                        device_power=device_power,
                        number_of_devices=number_of_devices
                      )
  Benchmark_metrics.get_inputs()
  Benchmark_metrics.print_inputs()
  metrics_kw = Benchmark_metrics.metrics_per_kw()
  lcoe_breakdown = Benchmark_metrics.lcoe_breakdown()

  assert metrics_kw['CAPEX per kW'] == 140000
  assert metrics_kw['OPEX per kW'] == 1500
  assert round(lcoe_breakdown['Device'],2) == 13.73
  assert round(lcoe_breakdown['Grid'],2) == 34.32
  assert round(lcoe_breakdown['Mooring and Foundation'],2) == 3.43
  assert round(lcoe_breakdown['Installation'],2) == 37.75
  assert round(lcoe_breakdown['Other'],2) == 6.86
  assert round(lcoe_breakdown['OPEX'],2) == 3.9

  Economical_metrics = Economical.get_complexity(
                          complexity=complexity,
                          proj_life=project_life,
                          disc_rate=discount_rate,
                          bom_compiled=bom['BoM'],
                          cost_of_installation=bom['Cost_of_installation'],
                          cost_of_equipment=bom['Cost_of_equipment'],
                          cost_other=bom['Other_costs'],
                          maintenance_solution=main_solu,
                          array_avr_energy_prod=aep_array,
                          compute_ace=True,
                          accw=accw,
                          device_surface=device_surface,
                          device_thick=device_thickness,
                          mat_dens=material_density,
                          mat_cost=material_cost
                        )
  # Try to run lcoe_compiler before running capex and opex compiler
  lcoe = Economical_metrics.lcoe_compiler()

  Financial_metrics = Financial.get_complexity(
                        complexity=complexity,
                        proj_life=project_life,
                        disc_rate=discount_rate,
                        array_avr_energy_prod=aep_array,
                        financial_grant=None,
                        financial_fit=None,
                        market_price=energy_price,
                        grant_value=None,
                        feed_in_tariff_value=None,
                        feed_in_tariff_years=None,
                        expenses_overall=costs['Overall Expenses']
                      )
  # Try to run internal_rate_return() before running cashflows_compiler()
  irr = Financial_metrics.internal_rate_return()

  Financial_metrics = Financial.get_complexity(
                        complexity=complexity,
                        proj_life=project_life,
                        disc_rate=discount_rate,
                        array_avr_energy_prod=aep_array,
                        financial_grant=None,
                        financial_fit=None,
                        market_price=energy_price,
                        grant_value=None,
                        feed_in_tariff_value=None,
                        feed_in_tariff_years=None,
                        expenses_overall=costs['Overall Expenses']
                      )
  # Try to run net_present_value() before running cashflows_compiler()
  npv = Financial_metrics.net_present_value()


def test_case1_cpx3():
  import pandas as pd
  import json

  # INPUTS
  complexity = 'high'
  device_topology = None
  device_structural_costs = 1000000
  device_power = 100
  number_of_devices = 1
  other_costs = 1000000
  project_life = 5
  discount_rate = 10
  aep_array = {
    "array_annual_net_energy_pd": {
          "value": '''{
              \"1\":1000000,
              \"2\":1000000,
              \"3\":1000000,
              \"4\":1000000,
              \"5\":1000000
          }''',
          "Aggregation-Level": "array"
      }
  }
  accw = 10
  device_surface = 100
  device_thickness = 0.1
  material_density = 1000
  material_cost = 2
  energy_price = 0.05

  bom_et = pd.read_csv('test/test_inputs/case1/bom_et.csv',
                       low_memory=False, header=0)
  bom_ed = pd.read_csv('test/test_inputs/case1/bom_ed.csv',
                       low_memory=False, header=0)
  bom_sk = pd.read_csv('test/test_inputs/case1/bom_sk.csv',
                       low_memory=False, header=0)
  bom_lmo = pd.read_csv('test/test_inputs/case1/bom_lmo.csv',
                        low_memory=False, header=0)
  with open('test/test_inputs/case1/aep.json') as json_file:
    aep_spey = json.load(json_file)
    list_aep = aep_spey['array_annual_net_energy_pd']

  bom =  bom_compiler(bom_et=bom_et,
                      bom_ed=bom_ed,
                      bom_sk=bom_sk,
                      bom_lmo=bom_lmo,
                      device_topology=device_topology,
                      device_struct_cost=device_structural_costs,
                      number_of_devices=number_of_devices,
                      other_costs=other_costs)
  assert bom['Cost_of_equipment'] == 7500000
  assert bom['Cost_of_installation'] == 5500000
  assert bom['Other_costs'] == 1000000

  main_solu = {
      "operation_id": ["OP13_0", "OP13_0", "OP13_0", "OP13_0", "OP13_0"],
      "proj_year": [1, 2, 3, 4, 5],
      "cost": [75000, 75000, 75000, 75000, 75000],
      "cost_label": ["OPEX", "OPEX", "OPEX", "OPEX", "OPEX"]
  }

  Economical_metrics = Economical.get_complexity(
                          complexity=complexity,
                          proj_life=project_life,
                          disc_rate=discount_rate,
                          bom_compiled=bom['BoM'],
                          cost_of_installation=bom['Cost_of_installation'],
                          cost_of_equipment=bom['Cost_of_equipment'],
                          cost_other=bom['Other_costs'],
                          maintenance_solution=main_solu,
                          array_avr_energy_prod=aep_array,
                          compute_ace=True,
                          accw=accw,
                          device_surface=device_surface,
                          device_thick=device_thickness,
                          mat_dens=material_density,
                          mat_cost=material_cost
                        )
  Economical_metrics.get_inputs()
  Economical_metrics.print_inputs()

  # Try to run cost_compiler before running capex and opex compiler
  costs = Economical_metrics.cost_compiler()
  # Try to run lcoe_compiler before running capex and opex compiler
  lcoe = Economical_metrics.lcoe_compiler()

  capex = Economical_metrics.capex_compiler()
  opex = Economical_metrics.opex_compiler()
  costs = Economical_metrics.cost_compiler()

  assert capex['Total CAPEX'] == 14000000
  assert opex['Total OPEX'] == 750000
  assert opex['AVR OPEX per year'] == 150000
  assert round(Economical_metrics.lcoe_compiler(),2) == 3.48
  assert Economical_metrics.ace_compiler() == 500
  assert int(costs['Discounted costs']) == 14568618

  Financial_metrics = Financial.get_complexity(
                        complexity=complexity,
                        proj_life=project_life,
                        disc_rate=discount_rate,
                        array_avr_energy_prod=aep_array,
                        financial_grant=None,
                        financial_fit=None,
                        market_price=energy_price,
                        grant_value=None,
                        feed_in_tariff_value=None,
                        feed_in_tariff_years=None,
                        expenses_overall=costs['Overall Expenses']
                      )

  Financial_metrics.get_inputs()
  Financial_metrics.print_inputs()

  # Try to run payback_period before running cashflows_compiler()
  Financial_metrics.payback_period()

  npv = Financial_metrics.net_present_value()
  irr = Financial_metrics.internal_rate_return()
  pbp = Financial_metrics.payback_period()
  dpbp = Financial_metrics.payback_period(disc_pbp=True)

  cashflows = Financial_metrics.cf
  disc_cashflows = Financial_metrics.dcf

  assert cashflows[0] == -capex['Total CAPEX']
  assert cashflows[1] == -100000
  assert int(disc_cashflows[1]) == -90909
  assert cashflows[2] == -100000
  assert int(disc_cashflows[2]) == -82644
  assert cashflows[3] == -100000
  assert int(disc_cashflows[3]) == -75131
  assert cashflows[4] == -100000
  assert int(disc_cashflows[4]) == -68301
  assert cashflows[5] == -100000
  assert int(disc_cashflows[5]) == -62092
  assert int(npv) == -14379078
  assert irr == None
  assert pbp == None
  assert dpbp == None

  Benchmark_metrics = Benchmark.get_complexity(
                        complexity=complexity,
                        proj_life=project_life,
                        disc_rate=discount_rate,
                        expenses_overall=costs['Overall Expenses'],
                        capex_total=capex['Total CAPEX'],
                        opex_total=opex['Total OPEX'],
                        disc_costs=costs['Discounted costs'],
                        device_power=device_power,
                        number_of_devices=number_of_devices
                      )
  Benchmark_metrics.get_inputs()
  Benchmark_metrics.print_inputs()
  metrics_kw = Benchmark_metrics.metrics_per_kw()
  lcoe_breakdown = Benchmark_metrics.lcoe_breakdown()

  assert metrics_kw['CAPEX per kW'] == 140000
  assert metrics_kw['OPEX per kW'] == 1500
  assert round(lcoe_breakdown['Device'],2) == 13.73
  assert round(lcoe_breakdown['Grid'],2) == 34.32
  assert round(lcoe_breakdown['Mooring and Foundation'],2) == 3.43
  assert round(lcoe_breakdown['Installation'],2) == 37.75
  assert round(lcoe_breakdown['Other'],2) == 6.86
  assert round(lcoe_breakdown['OPEX'],2) == 3.9

  Economical_metrics = Economical.get_complexity(
                          complexity=complexity,
                          proj_life=project_life,
                          disc_rate=discount_rate,
                          bom_compiled=bom['BoM'],
                          cost_of_installation=bom['Cost_of_installation'],
                          cost_of_equipment=bom['Cost_of_equipment'],
                          cost_other=bom['Other_costs'],
                          maintenance_solution=main_solu,
                          array_avr_energy_prod=aep_array,
                          compute_ace=True,
                          accw=accw,
                          device_surface=device_surface,
                          device_thick=device_thickness,
                          mat_dens=material_density,
                          mat_cost=material_cost
                        )
  # Try to run lcoe_compiler before running capex and opex compiler
  lcoe = Economical_metrics.lcoe_compiler()

  Financial_metrics = Financial.get_complexity(
                        complexity=complexity,
                        proj_life=project_life,
                        disc_rate=discount_rate,
                        array_avr_energy_prod=aep_array,
                        financial_grant=None,
                        financial_fit=None,
                        market_price=energy_price,
                        grant_value=None,
                        feed_in_tariff_value=None,
                        feed_in_tariff_years=None,
                        expenses_overall=costs['Overall Expenses']
                      )
  # Try to run internal_rate_return() before running cashflows_compiler()
  irr = Financial_metrics.internal_rate_return()

  Financial_metrics = Financial.get_complexity(
                        complexity=complexity,
                        proj_life=project_life,
                        disc_rate=discount_rate,
                        array_avr_energy_prod=aep_array,
                        financial_grant=None,
                        financial_fit=None,
                        market_price=energy_price,
                        grant_value=None,
                        feed_in_tariff_value=None,
                        feed_in_tariff_years=None,
                        expenses_overall=costs['Overall Expenses']
                      )
  # Try to run net_present_value() before running cashflows_compiler()
  npv = Financial_metrics.net_present_value()
