from dtop_slc.business.core import bom_compiler
from dtop_slc.business import Economical
import pandas as pd

def test_no_float_core():
  import pandas as pd
  import json

  # INPUTS
  complexity = 'low'
  device_topology = None
  device_structural_costs = 1000000
  device_power = 100
  number_of_devices = 1
  other_costs = 1000000

  bom_et = pd.read_csv('test/test_inputs/case3/bom_et.csv',
                       low_memory=False, header=0)
  bom_ed = pd.read_csv('test/test_inputs/case3/bom_ed.csv',
                       low_memory=False, header=0)
  bom_sk = pd.read_csv('test/test_inputs/case3/bom_sk.csv',
                       low_memory=False, header=0)
  bom_lmo = pd.read_csv('test/test_inputs/case3/bom_lmo.csv',
                        low_memory=False, header=0)
  with open('test/test_inputs/case3/aep.json') as json_file:
    aep_spey = json.load(json_file)
    list_aep = aep_spey['array_annual_net_energy_pd']

  bom = bom_compiler(bom_et=bom_et,
                     bom_ed=bom_ed,
                     bom_sk=bom_sk,
                     bom_lmo=bom_lmo,
                     device_topology=device_topology,
                     device_struct_cost=device_structural_costs,
                     number_of_devices=number_of_devices,
                     other_costs=other_costs)

def test_no_float_eco():
  # INPUTS
  import pandas as pd
  complexity = 'low'
  project_life = 5
  discount_rate = 10
  aep_array = {
    "array_annual_net_energy_pd": {
          "value": '''{
              \"1\":1000000,
              \"2\":1000000,
              \"3\":1000000,
              \"4\":1000000,
              \"5\":1000000
          }''',
          "Aggregation-Level": "array"
      }
  }


  bom_cols = ['id','name','qnt','uom','unit_cost','total_cost']
  bom_list = [['id1','name1',1,'-','float','float'],
              ['id2','name2',1,'-','float','float'],
              ['id2','name2',1,'-','float','float']
             ]
  bom_table = pd.DataFrame(bom_list,columns=bom_cols)

  main_table = {
      "operation_id": ["OP13_0"],
      "proj_year": [1],
      "cost": ["float_num"],
      "cost_label": ["OPEX"]
  }

  Economical_metrics = Economical.get_complexity(
                          complexity=complexity,
                          proj_life=project_life,
                          disc_rate=discount_rate,
                          bom_compiled=bom_table,
                          cost_of_installation=100000,
                          cost_of_equipment=3000000,
                          cost_other=100000,
                          maintenance_solution=main_table,
                          array_avr_energy_prod=aep_array
                       )
  Economical_metrics.capex_compiler()
  Economical_metrics.opex_compiler()

  complexity = 'med'
  Economical_metrics = Economical.get_complexity(
      complexity=complexity,
      proj_life=project_life,
      disc_rate=discount_rate,
      bom_compiled=bom_table,
      cost_of_installation=100000,
      cost_of_equipment=3000000,
      cost_other=100000,
      maintenance_solution=main_table,
      array_avr_energy_prod=aep_array
  )
  Economical_metrics.capex_compiler()
  Economical_metrics.opex_compiler()

  complexity = 'high'
  Economical_metrics = Economical.get_complexity(
                          complexity=complexity,
                          proj_life=project_life,
                          disc_rate=discount_rate,
                          bom_compiled=bom_table,
                          cost_of_installation=100000,
                          cost_of_equipment=3000000,
                          cost_other=100000,
                          maintenance_solution=main_table,
                          array_avr_energy_prod=aep_array
                       )
  Economical_metrics.capex_compiler()
  Economical_metrics.opex_compiler()
