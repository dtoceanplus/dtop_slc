from dtop_slc.business import Financial
import pandas as pd

def test_financial_fit_cpx2():
  tables_cols = ['id','cost','proj_year','cost_label','category']
  capex_list = ['id',7000000,0,'capex','Device']
  opex_list = [['op1',50000,2,'opex','OPEX'],
               ['op2',80000,4,'opex','OPEX']
              ]
  capex_Table = pd.DataFrame([capex_list],columns=tables_cols)
  opex_Table = pd.DataFrame(opex_list,columns=tables_cols)
  expenses = pd.concat([capex_Table,opex_Table])
  Financial2 = Financial.get_complexity(
                 complexity = 'med',
                 proj_life=5,
                 disc_rate=10,
                 array_avr_energy_prod={
                    "array_annual_net_energy_pd": {
                          "value": '''{
                              \"1\":1000,
                              \"2\":1000,
                              \"3\":1000,
                              \"4\":1000,
                              \"5\":1000
                          }''',
                          "Aggregation-Level": "array"
                      }
                  },
                 financial_grant=True,
                 financial_fit=True,
                 market_price=0.055,
                 grant_value=5000000,
                 feed_in_tariff_value=0.2,
                 feed_in_tariff_years=3,
                 expenses_overall=expenses
              )
  Financial2.payback_period()
  Financial2.internal_rate_return()
  Financial2.net_present_value()

  Financial2 = Financial.get_complexity(
                 complexity = 'med',
                 proj_life=5,
                 disc_rate=10,
                 array_avr_energy_prod={
                    "array_annual_net_energy_pd": {
                          "value": '''{
                              \"1\":1000,
                              \"2\":1000,
                              \"3\":1000,
                              \"4\":1000,
                              \"5\":1000
                          }''',
                          "Aggregation-Level": "array"
                      }
                  },
                 financial_grant=True,
                 financial_fit=True,
                 market_price=0.055,
                 grant_value=5000000,
                 feed_in_tariff_value=0.2,
                 expenses_overall=expenses
              )
  Financial2.cashflows_compiler()

def test_financial_fit_cpx3():
  tables_cols = ['id','cost','proj_year','cost_label','category']
  capex_list = ['id',7000000,0,'capex','Device']
  opex_list = [['op1',50000,2,'opex','OPEX'],
               ['op2',80000,4,'opex','OPEX']
              ]
  capex_Table = pd.DataFrame([capex_list],columns=tables_cols)
  opex_Table = pd.DataFrame(opex_list,columns=tables_cols)
  expenses = pd.concat([capex_Table,opex_Table])
  Financial3 = Financial.get_complexity(
                 complexity = 'high',
                 proj_life=5,
                 disc_rate=10,
                 array_avr_energy_prod={
                    "array_annual_net_energy_pd": {
                          "value": '''{
                              \"1\":1000,
                              \"2\":1000,
                              \"3\":1000,
                              \"4\":1000,
                              \"5\":1000
                          }''',
                          "Aggregation-Level": "array"
                      }
                  },
                 financial_grant=True,
                 financial_fit=True,
                 market_price=0.055,
                 grant_value=5000000,
                 feed_in_tariff_value=0.2,
                 feed_in_tariff_years=3,
                 expenses_overall=expenses
              )
  Financial3.payback_period()
  Financial3.internal_rate_return()
  Financial3.net_present_value()

  Financial3 = Financial.get_complexity(
                 complexity = 'high',
                 proj_life=5,
                 disc_rate=10,
                 array_avr_energy_prod={
                    "array_annual_net_energy_pd": {
                          "value": '''{
                              \"1\":1000,
                              \"2\":1000,
                              \"3\":1000,
                              \"4\":1000,
                              \"5\":1000
                          }''',
                          "Aggregation-Level": "array"
                      }
                  },
                 financial_grant=True,
                 financial_fit=True,
                 market_price=0.055,
                 grant_value=5000000,
                 feed_in_tariff_value=0.2,
                 expenses_overall=expenses
              )
  Financial3.cashflows_compiler()
