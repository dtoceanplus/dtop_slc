import pytest
from flask import Response

from dtop_slc.service import create_app


@pytest.fixture
def client():
    client = create_app().test_client()
    yield client


def test_core(client):
    response: Response = client.get('/')
    assert response.status_code == 200
    assert response.content_type == 'text/html; charset=utf-8'
    assert '<a href="/api/">API documentation</a>' in response.data.decode()
    assert '<a href="/gui/complexity">Introduce inputs</a>' in response.data.decode()
