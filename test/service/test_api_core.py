# Testing Flask Applications: http://flask.pocoo.org/docs/1.0/testing/

import pytest

from dtop_slc import service
from dtop_slc.service import create_app

import json

@pytest.fixture
def client():
  client = service.create_app().test_client()
  yield client

def test_core(client):
  response = client.get("/core/core/")

@pytest.fixture
def app():
  app = create_app({"TESTING": True})
  yield app

@pytest.fixture
def client(app):
  client = app.test_client()
  yield client

  