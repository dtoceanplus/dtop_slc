import atexit

import pytest
from pact import Consumer, Provider, Term, Like, EachLike

from dtop_slc.service import create_app


@pytest.fixture
def app():
  app = create_app({'TESTING': True})
  yield app


@pytest.fixture
def client(app):
  client = app.test_client()
  yield client


pact = Consumer('slc').has_pact_with(Provider('mc'), port=1231)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_mc_general(client):
  pact.given(
      "mc 1 exists and it has general"
  ).upon_receiving(
      "a request for machine general characteristics"
  ).with_request(
      "GET", Term(r"/mc/\d+/general", "/mc/1/general")
  ).will_respond_with(
      200,
      body=Like({
          "floating": Like(True),
          "machine_cost": Like(1000000.0)
      })
  )

  with pact:
    response = client.post(
        "api/mc-general",
        json={"mc_general": f"{pact.uri}/mc/1/general"}
    )
    assert response.status_code == 201
