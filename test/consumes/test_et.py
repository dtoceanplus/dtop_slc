import atexit

import pytest
from pact import Consumer, Provider, Term, Like, EachLike

from dtop_slc.service import create_app


@pytest.fixture
def app():
  app = create_app({'TESTING': True})
  yield app


@pytest.fixture
def client(app):
  client = app.test_client()
  yield client


pact = Consumer('slc').has_pact_with(Provider('et'), port=1233)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_et_array(client):
  pact.given(
      'et 1 exists and it has array'
  ).upon_receiving(
      'a request for ET array'
  ).with_request(
      'GET', Term(r'/energy_transf/\d+/array', '/energy_transf/1/array')
  ).will_respond_with(
      200,
      body=Like({
          "BoM": Like({
              "value": Like({
                  "id": EachLike('Cat_ID_0'),
                  "name": EachLike('Elect_Simplified'),
                  "qnt": EachLike(6),
                  "uom": EachLike('NA'),
                  "unit_cost": EachLike(10000.0),
                  "total_cost": EachLike(60000.0)
              })
          })
      })
  )

  with pact:
    response = client.post(
        'api/et-array',
        json={"et_array": f'{pact.uri}/energy_transf/1/array'}
    )
    assert response.status_code == 201


# @pytest.mark.skip()
def test_et_devices(client):
  pact.given(
      'et 1 exists and it has devices'
  ).upon_receiving(
      'a request for ET devices'
  ).with_request(
      'GET', Term(r'/energy_transf/\d+/devices', '/energy_transf/1/devices')
  ).will_respond_with(
      200,
      body=EachLike({
          "Dev_rated_power": Like({
              "description": Like('Rated power of the device'),
              "label": Like('Rated power'),
              "unit": Like('kW'),
              "value": Like(1115.0)
          })
      })
  )

  with pact:
    response = client.post(
        'api/et-devices',
        json={"et_devices": f'{pact.uri}/energy_transf/1/devices'}
    )
    assert response.status_code == 201
