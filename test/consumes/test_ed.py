import atexit

import pytest
from pact import Consumer, Provider, Term, Like, EachLike

from dtop_slc.service import create_app


@pytest.fixture
def app():
  app = create_app({'TESTING': True})
  yield app


@pytest.fixture
def client(app):
  client = app.test_client()
  yield client


pact = Consumer('slc').has_pact_with(Provider('ed'), port=1234)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_ed_cpx3(client):
  pact.given(
      'ed 1 exists at cpx 3'
  ).upon_receiving(
      'a request for ED results at CPX 3'
  ).with_request(
      'GET', Term(r'/api/energy-deliv-studies/\d+/results', '/api/energy-deliv-studies/1/results')
  ).will_respond_with(
      200,
      body=Like({
          "b_o_m_new": Like({
              "name": Like({
                  "Tot_Array": Like("Total array network"),
                  "Tot_Collection_Point": Like("Total collection point"),
                  "Tot_Onshore": Like("Total onshore infrastructure"),
                  "Tot_Transmission": Like("Total transmission network")
              }),
              "qnt": Like({
                  "Tot_Array": Like("NA"),
                  "Tot_Collection_Point": Like("NA"),
                  "Tot_Onshore": Like("NA"),
                  "Tot_Transmission": Like("NA")
              }),
              "uom": Like({
                  "Tot_Array": Like("NA"),
                  "Tot_Collection_Point": Like("NA"),
                  "Tot_Onshore": Like("NA"),
                  "Tot_Transmission": Like("NA")
              }),
              "unit_cost": Like({
                  "Tot_Array": Like("NA"),
                  "Tot_Collection_Point": Like("NA"),
                  "Tot_Onshore": Like("NA"),
                  "Tot_Transmission": Like("NA")
              }),
              "total_cost": Like({
                  "Tot_Array": Like(1046924),
                  "Tot_Collection_Point": Like(517414),
                  "Tot_Onshore": Like(0),
                  "Tot_Transmission": Like(192858)
              })
          })
      })
  )

  with pact:
    response = client.post(
        'api/ed-results',
        json={
            "cpx": '3',
            "ed_results": f"{pact.uri}/api/energy-deliv-studies/1/results"
        }
    )
    assert response.status_code == 201


# @pytest.mark.skip()
def test_ed_cpx1(client):
  pact.given(
      'ed 2 exists at cpx 1'
  ).upon_receiving(
      'a request for ED results at CPX 1'
  ).with_request(
      'GET', Term(r'/api/energy-deliv-studies/\d+/results', '/api/energy-deliv-studies/1/results')
  ).will_respond_with(
      200,
      body=Like({
          "b_o_m_new": Like({
              "name": Like({
                  "Tot_Array": Like("Total array network"),
                  "Tot_Collection_Point": Like("Total collection point"),
                  "Tot_Onshore": Like("Total onshore infrastructure"),
                  "Tot_Transmission": Like("Total transmission network")
              }),
              "qnt": Like({
                  "Tot_Array": Like("NA"),
                  "Tot_Collection_Point": Like("NA"),
                  "Tot_Onshore": Like("NA"),
                  "Tot_Transmission": Like("NA")
              }),
              "uom": Like({
                  "Tot_Array": Like("NA"),
                  "Tot_Collection_Point": Like("NA"),
                  "Tot_Onshore": Like("NA"),
                  "Tot_Transmission": Like("NA")
              }),
              "unit_cost": Like({
                  "Tot_Array": Like("NA"),
                  "Tot_Collection_Point": Like("NA"),
                  "Tot_Onshore": Like("NA"),
                  "Tot_Transmission": Like("NA")
              }),
              "total_cost": Like({
                  "Tot_Array": Like(1046924),
                  "Tot_Collection_Point": Like(517414),
                  "Tot_Onshore": Like(0),
                  "Tot_Transmission": Like(192858)
              })
          })
      })
  )

  with pact:
    response = client.post(
        'api/ed-results',
        json={
            "cpx": '1',
            "ed_results": f"{pact.uri}/api/energy-deliv-studies/1/results"
        }
    )
    assert response.status_code == 201
