import atexit

import pytest
from pact import Consumer, Provider, Term, Like, EachLike

from dtop_slc.service import create_app


@pytest.fixture
def app():
  app = create_app({"TESTING": True})
  yield app


@pytest.fixture
def client(app):
  client = app.test_client()
  yield client


pact = Consumer("slc").has_pact_with(Provider("ec"), port=1232)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_ec_farm(client):
  pact.given(
      "ec 1 exists and has farm"
  ).upon_receiving(
      "a request for farm"
  ).with_request(
      "GET", Term(r"/ec/\d+/farm", "/ec/1/farm")
  ).will_respond_with(
      200,
      body=Like({
          "number_devices": Like(1)
      })
  )

  with pact:
    response = client.post(
        "api/ec-farm",
        json={"ec_farm": f"{pact.uri}/ec/1/farm"}
    )

    assert response.status_code == 201
