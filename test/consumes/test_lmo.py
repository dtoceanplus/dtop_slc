import atexit

import pytest
from pact import Consumer, Provider, Term, Like, EachLike

from dtop_slc.service import create_app


@pytest.fixture
def app():
  app = create_app({'TESTING': True})
  yield app


@pytest.fixture
def client(app):
  client = app.test_client()
  yield client


pact = Consumer('slc').has_pact_with(Provider('lmo'), port=1236)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_lmo_bom(client):
  pact.given(
      'lmo 1 exists and has installation'
  ).upon_receiving(
      'a request for LMO Bill of Materials'
  ).with_request(
      'GET', Term(r'/api/\d+/bom', '/api/1/bom')
  ).will_respond_with(
      200,
      body=Like({
          "id": EachLike('Tot_Inst_Dev'),
          "name": EachLike('Total cost of installation of devices'),
          "qnt": EachLike('-'),
          "uom": EachLike('-'),
          "unit_cost": EachLike('-'),
          "total_cost": EachLike(72582.0)
      })
  )

  with pact:
    response = client.post(
        'api/lmo-bom',
        json={"lmo_bom": f"{pact.uri}/api/1/bom"}
    )
    assert response.status_code == 201


# @pytest.mark.skip()
def test_lmo_maintenance_solution(client):
  pact.given(
      'lmo 1 exists and has maintenance'
  ).upon_receiving(
      'a request for LMO Maintenance Solution'
  ).with_request(
      'GET', Term(r'/api/\d+/phases/\w+/plan', '/api/1/phases/maintenance/plan')
  ).will_respond_with(
      200,
      body=Like({
          "operation_id": EachLike('OP_12'),
          "proj_year": EachLike(5),
          "cost": EachLike(100000.0),
          "cost_label": EachLike('opex')
      })
  )

  with pact:
    response = client.post(
        'api/lmo-maintenance-solution',
        json={"lmo_maintenance_solution": f"{pact.uri}/api/1/phases/maintenance/plan"}
    )
    assert response.status_code == 201
