import atexit

import pytest
from pact import Consumer, Provider, Term, Like, EachLike

from dtop_slc.service import create_app


@pytest.fixture
def app():
  app = create_app({'TESTING': True})
  yield app


@pytest.fixture
def client(app):
  client = app.test_client()
  yield client


pact = Consumer('slc').has_pact_with(Provider('spey'), port=1237)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_spey_aep(client):
  pact.given(
      'spey 1 exists and has aep'
  ).upon_receiving(
      'a request for SPEY Annual Energy Production'
  ).with_request(
      'GET', Term(r'/spey/\d+/energy-production', '/spey/1/energy-production')
  ).will_respond_with(
      200,
      body=Like({
          "Outputs_EP": Like({
              "array_annual_net_energy_pd": Like({
                  "value": Like(
                      '''{\"1\":0.0912616929,
                          \"2\":0.021161305,
                          \"3\":0.0421514944,
                          \"4\":0.1150467716,
                          \"5\":0.0254962355,
                          \"6\":0.1018708647,
                          \"7\":0.0740930869,
                          \"8\":0.0275496235,
                          \"9\":0.0,
                          \"10\":0.2191991786}'''
                  )
              })
          })
      })
  )

  with pact:
    response = client.post(
        'api/spey-aep',
        json={"spey_aep": f"{pact.uri}/spey/1/energy-production"}
    )
    assert response.status_code == 201
