import atexit

import pytest
from pact import Consumer, Provider, Term, Like, EachLike

from dtop_slc.service import create_app


@pytest.fixture
def app():
  app = create_app({'TESTING': True})
  yield app


@pytest.fixture
def client(app):
  client = app.test_client()
  yield client


pact = Consumer('slc').has_pact_with(Provider('sk'), port=1235)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_sk_bom(client):
  pact.given(
      "sk 1 exists and it has bom"
  ).upon_receiving(
      "a request for SK Bill of Materials"
  ).with_request(
      "GET", Term(r"/sk/\d+/bom", "/sk/1/bom")
  ).will_respond_with(
      200,
      body=Like({
          "catalogue_ID": EachLike('abchor_n_2'),
          "product_name": EachLike('abchor_n_2'),
          "quantity": EachLike(3.0),
          "unit": EachLike('unit'),
          "unit_cost": EachLike(29575.508943101893),
          "total_cost": EachLike(88726.52682930569)
      })
  )

  with pact:
    response = client.post(
        "api/sk-bom",
        json={"sk_bom": f"{pact.uri}/sk/1/bom"}
    )
    assert response.status_code == 201
